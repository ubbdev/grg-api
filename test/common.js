'use strict';

// Some data and helper functions to be used for tests

const server = require('../server/server.js');
const supertest = require('supertest');
const agent = supertest(server);
const chai = require('chai');
const MailDev = require('maildev');

module.exports.server = server;
module.exports.agent = agent;
module.exports.assert = chai.assert;

module.exports.maildev = new MailDev({
  silent: true,
  disableWeb: true,
});

const Person = server.models.Person;

const settings = require('../settings.test.json');
module.exports.projects = settings.projects;
const projectId1 = settings.projects[0].id;
const projectId2 = settings.projects[1].id;

let admin = {username: 'admin', password: 'abc', name: 'A Admin', email: 'a.admin@email.com', projectId: projectId1, role: 'admin'};
let editor = {username: 'editor', password: 'abc', name: 'B Editor', email: 'b.editor@email.com', projectId: projectId1, role: 'editor'};
let contributor = {username: 'contributor', password: 'abc', name: 'C Person', email: 'c.contributor@email.com', projectId: projectId1, role: 'contributor'};
let newUser = {username: 'newuser', password: 'abc', name: 'D Newuser', email: 'd.newuser@email.com', projectId: null, role: null};
let otherContributor = {username: 'contributor2', password: 'abc', name: 'C Person2', email: 'c.contributor2@email.com', projectId: projectId1, role: 'contributor'};
let thirdContributor = {username: 'contributor3', password: 'abc', name: 'C Person3', email: 'c.contributor3@email.com', projectId: projectId1, role: 'contributor'};
let otherProjectContributor = {username: 'contributor4', password: 'abc', name: 'C Person4', email: 'c.contributor4@email.com', projectId: projectId2, role: 'contributor'};
let otherProjectEditor = {username: 'editor2', password: 'abc', name: 'B Editor2', email: 'b.editor2@email.com', projectId: projectId2, role: 'editor'};

module.exports.admin = admin;
module.exports.editor = editor;
module.exports.contributor = contributor;
module.exports.newUser = newUser;
module.exports.otherContributor = otherContributor;
module.exports.thirdContributor = thirdContributor;
module.exports.otherProjectContributor = otherProjectContributor;
module.exports.otherProjectEditor = otherProjectEditor;

module.exports.people = [admin, editor, contributor, newUser, otherContributor, thirdContributor, otherProjectContributor, otherProjectEditor];

module.exports.createSampleArticle = function(person, article, projectId, callback) {
  agent.post('/api/projects/' + projectId + '/articles')
  .set('Content-Type', 'application/json')
  .set('Authorization', person.token)
  .send(article)
  .expect(200)
  .end((err, res) => callback(err, res.body && res.body.id));
};

module.exports.createSampleUser = function(user, projectId, roleName, callback) {
  // make a copy to avoid setting id
  let newuser = {
    username: user.username,
    password: user.password,
    name: user.name,
    email: user.email,
    emailVerified: true,
  };
  server.models.Person.create(newuser, (err, person) => {
    if (err) return callback(err);
    //store id for convenience
    user.id = person.id;
    user.emailVerified = person.emailVerified;
    if (roleName && projectId) {
      server.models.Project.addMemberWithRole(projectId, person.id, roleName,
        (err, map) => callback(err, person));
    } else callback(null, person);
  });
};
