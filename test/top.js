'use strict';

// This script does some top-level setup

const common = require('./common');
const async = require('async');
const nock = require('nock');
const mongo = require('MongoDB');
const dbConfig = require('../server/datasources.test.json').db;

function clearDatabase(done) {
  console.log('Clearing test database');
  if (dbConfig.connector !== 'mongodb') {
    console.log('Warning, tests are meant to run with MongoDB');
    return done();
  }
  const server = new mongo.Server(dbConfig.host, dbConfig.port);
  const db = new mongo.Db(dbConfig.database, server);
  db.open((err, db) => {
    if (err) return done(err);
    db.collections((err, collections) => {
      if (err) return done(err);
      async.each(collections, (collection, cb) => {
        collection.drop(cb);
      }, done);
    });
  });
}

function startServer(done) {
  // server start verifies that zotero settings are legit, in tests we mock this
  nock('https://api.zotero.org')
  .get('/groups/123/items')
  .query(q => { return q.limit === '1'; })
  .reply(200, {});

  // second test project
  nock('https://api.zotero.org')
  .get('/groups/456/items')
  .query(q => { return q.limit === '1'; })
  .reply(200, {});

  common.server.init(done);
}

// All elasticsearch indices are cleared before start
function reindex(done) {
  const client = common.server.elasticClient;
  if (!client) return done();
  common.server.models.Project.recreateElasticsearchIndices(done);
}

// Setup a bunch of example users to be used in tests
function initPeople(done) {
  async.each(common.people, (person, cb) => {
    common.createSampleUser(person, person.projectId, person.role, (err, personObj) => {
      if (err) return cb(err);
      common.server.models.Person.login(person, (err, token) => {
        if (err) return cb(err);
        person.token = token.id;
        return cb();
      });
    });
  }, done);
}

before(function(done) {
  this.timeout(10000);
  async.series([
    clearDatabase,
    startServer,
    reindex,
    initPeople,
  ], err => { console.log('Test server initialized'); done(err); });
});

before(done => {
  console.log('Starting MailDev');

  // hack to catch errors, these errors are not passed to the callback in listen
  common.maildev.smtp.on('error', err => {
    console.error('Error starting MailDev:');
    console.error(err);
  });

  common.maildev.listen(err => {
    done(err);
  });
});

after(common.maildev.close);
