'use strict';

const async = require('async');
const _ = require('lodash');

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

const projectId = common.projects[0].id;
const projectId2 = common.projects[1].id;

let Article = null;
let Reference = null;
let Project = null;

describe('Article', function() {
  let sampleArticle = null;
  let draftArticle = null;
  let publicDraft =  null;

  let ref1Id = null;
  let ref2Id = null;
  let ref3Id = null;

  before(done => {
    Article = server.models.Article;
    Reference = server.models.Reference;
    Project = server.models.Project;
    done();
  });

  before(done => {
    server.models.Reference.create({title: 'Foo', creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId},
      (err, ref) => {
        ref1Id = ref.id;
        sampleArticle = {
          title: 'Sample published',
          body: 'foo',
          contributorId: [common.contributor.id, common.otherContributor.id],
          status: Article.STATUS_PUBLISHED,
          referenceId: [ref1Id],
        };
        draftArticle = {
          title: 'Sample draft',
          body: 'bar',
          contributorId: [common.contributor.id, common.otherContributor.id],
          status: Article.STATUS_DRAFT,
          referenceId: [ref1Id],
        };
        publicDraft = {
          title: 'Sample public draft',
          body: 'baz',
          contributorId: [common.contributor.id, common.otherContributor.id],
          status: Article.STATUS_PUBLIC_DRAFT,
          referenceId: [ref1Id],
        };
        done(err);
      });
  });

  before(done => {
    Reference.create({title: 'Foo2', creators: ['Bar2 Baz2'], date: '1999', key: '2', projectId: projectId2},
      (err, ref) => {
        ref2Id = ref.id;
        done(err);
      });
  });

  before(done => {
    Reference.create({title: 'Foo3', creators: ['Bar3 Baz3'], date: '1999', key: '3', projectId: projectId},
      (err, ref) => {
        ref3Id = ref.id;
        done(err);
      });
  });

  after(done => { Reference.destroyAll(done); });
  after(done => { Article.destroyAll(done); });

  describe('Security', function() {
    let sampleArticleId = null;
    let draftId = null;
    let publicDraftId = null;

    before(done => common.createSampleArticle(common.contributor, sampleArticle, projectId, (err, id) => {
      if (err) return done(err);
      sampleArticleId = id;
      Article.findById(id, (err, article) => article.contributors.add(common.otherContributor.id).then(() => article.save(done), done));
    }));

    before(done => {
      common.createSampleArticle(common.contributor, draftArticle, projectId, (err, id) => {
        if (err) return done(err);
        draftId = id;
        done();
      });
    });

    before(done => {
      common.createSampleArticle(common.contributor, publicDraft, projectId, (err, id) => {
        if (err) return done(err);
        publicDraftId = id;
        done();
      });
    });

    after(done => { Article.destroyAll(done); });

    describe('404s', function() {
      it('GET/POST/DELETE hidden', function() {
        return Promise.all([
          agent.get('/api/articles/').expect(404),
          agent.post('/api/articles/').send(sampleArticle).expect(404),
          agent.delete('/api/articles/').expect(404),
        ]);
      });

      it('Person instances can not be changed through contributor relation', function() {
        return Promise.all([
          agent.post('/api/articles/' + sampleArticleId + '/contributors').expect(404),
          agent.delete('/api/articles/' + sampleArticleId + '/contributors').expect(404),
          agent.patch('/api/articles/' + sampleArticleId + '/contributors').expect(404),
          agent.put('/api/articles/' + sampleArticleId + '/contributors/' + common.contributor.id).expect(404),
          agent.delete('/api/articles/' + sampleArticleId + '/contributors/' + common.contributor.id).expect(404),
          agent.delete('/api/articles/' + sampleArticleId + '/contributors/rel/' + common.contributor.id).expect(404),
          agent.post('/api/articles/' + sampleArticleId + '/contributors/rel/' + common.contributor.id).expect(404),
        ]);
      });

      it('Reference relation is read only', function() {
        return Promise.all([
          agent.post('/api/articles/' + sampleArticleId + '/references').expect(404),
          agent.delete('/api/articles/' + sampleArticleId + '/references').expect(404),
          agent.patch('/api/articles/' + sampleArticleId + '/references').expect(404),
          agent.put('/api/articles/' + sampleArticleId + '/references/' + ref1Id).expect(404),
          agent.delete('/api/articles/' + sampleArticleId + '/references/' + ref1Id).expect(404),
          agent.delete('/api/articles/' + sampleArticleId + '/references/rel/' + ref1Id).expect(404),
          agent.post('/api/articles/' + sampleArticleId + '/references/rel/' + ref1Id).expect(404),
        ]);
      });
    });

    describe('Authorization', function() {
      describe('Unauthenticated', function() {
        it('can only get limited contributor info', function() {
          return agent
          .get('/api/articles/' + sampleArticleId + '/contributors')
          .expect(200)
          .expect(res => {
            const contribs = res.body;
            assert(contribs.length === 2, 'correct number');
            assert(contribs.some(c => { return c.name === common.contributor.name; }), 'correct name');
            assert(contribs.some(c => { return c.id === common.contributor.id; }), 'correct id');
            contribs.forEach(c => assert(c.email === undefined, 'no email'));
          });
        });

        it('can see published article', function() {
          return agent
          .get('/api/articles/' + sampleArticleId)
          .expect(200);
        });

        it('can see public draft article', function() {
          return agent
          .get('/api/articles/' + publicDraftId)
          .expect(200);
        });

        it('can not access drafts', function() {
          return Promise.all([
            agent.get('/api/articles/' + draftId).expect(401),
            agent.get('/api/articles/' + draftId + '/contributors').expect(401),
            agent.get('/api/articles/' + draftId + '/edits').expect(401),
            agent.get('/api/articles/' + draftId + '/references').expect(401),
            agent.get('/api/articles/' + draftId + '/subjects').expect(401),
            agent.get('/api/articles/' + draftId + '/relatedWorks').expect(401),
          ]);
        });

        // The relations above can be included through include filters i.e.
        // /api/reference/:id?filter={include: {relation: article, scope: {include: contributors}}}
        // we can live with that for now, most important is to hide body and title of drafts.
        it('can not access draft details through include filter', function() {
          return agent
          .get('/api/references/' + ref1Id + '?filter=' + JSON.stringify({include: 'articles'}))
          .expect(200)
          .expect(res => {
            const ass = res.body.articles;
            assert(ass.length === 3, 'correct number articles');
            assert(ass.some(a => { return a.status === Article.STATUS_DRAFT; }), 'draft present');
            ass.forEach(a => {
              if (a.status === Article.STATUS_DRAFT) {
                assert(a.status === Article.STATUS_DRAFT, 'Correct status');
                assert(a.title === undefined, 'no title');
                assert(a.body === undefined, 'no body');
              }
            });
          });
        });
      });

      describe('Project contributor', function() {
        let token = null;

        before(done => { token = common.contributor.token; done(); });

        it('can only get limited contributor info', function() {
          return agent
          .get('/api/articles/' + sampleArticleId + '/contributors')
          .set('Authorization', token)
          .expect(200)
          .expect(res => {
            const contribs = res.body;
            assert(res.body.length === 2, 'correct length');
            assert(res.body.some(c => { return c.id === common.contributor.id; }), 'correct user');
            assert(res.body.some(c => { return c.id === common.otherContributor.id; }), 'correct user');
            contribs.forEach(c => assert(c.email === undefined, 'no email'));
          });
        });

        it('can not update article', function() {
          return agent
          .put('/api/articles/' + sampleArticleId)
          .set('Authorization', token)
          .expect(404);
        });

        it('can read own draft through Article API', function() {
          return agent
          .get('/api/articles/' + draftId)
          .set('Authorization', token)
          .expect(200)
          .expect(res => {
            assert(res.body.title === draftArticle.title, 'correct title');
            assert(res.body.status === 'draft', 'correct status');
          });
        });

        it('can read own draft through Project API', function() {
          return agent
          .get('/api/projects/' + projectId + '/articles/' + draftId)
          .set('Authorization', token)
          .expect(200)
          .expect(res => {
            assert(res.body.title === draftArticle.title, 'correct title');
            assert(res.body.status === 'draft', 'correct status');
          });
        });

        it('can list own draft through Project API', function() {
          return agent
          .get('/api/projects/' + projectId + '/articles')
          .set('Authorization', token)
          .expect(200)
          .expect(res => {
            assert(res.body.some(ass => ass.id === draftId && ass.title === draftArticle.title), 'has draft');
          });
        });

        it('can not read drafts from other projects', function() {
          return agent
          .get('/api/projects/' + projectId + '/articles')
          .set('Authorization', common.otherProjectContributor.token)
          .expect(200)
          .expect(res => {
            assert(res.body.some(a => a.id === draftId), 'draft is in list');

            res.body.forEach(article => {
              if (article.id === draftId) {
                assert(!article.title, 'no title');
                assert(!article.body, 'no body');
                assert(article.status === 'draft', 'correct status');
              }
            });
          });
        });
      });
    });
  });

  describe('Validate', function() {
    let token = null;

    before(done => { token = common.contributor.token; done(); });

    after(done => Article.destroyAll(done));

    describe('Persons', function() {
      it('must exist', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: [ref1Id], contributorId: ['abc']})
        .expect(422);
      });

      it('must be passed as array', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: [ref1Id], contributorId: common.contributor.id})
        .expect(422);
      });

      it('must match project', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: [ref1Id], contributorId: [common.otherProjectContributor.id]})
        .expect(422);
      });

      it('are stored', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: [ref1Id], contributorId: [common.contributor.id]})
        .expect(200)
        .then(res => {
          return agent
          .get('/api/projects/' + projectId + '/articles/' + res.body.id)
          .set('Authorization', token)
          .expect(200)
          .expect(res => {
            assert(res.body.contributorId.length === 1, 'right number of contributors');
            assert(res.body.contributorId[0] = common.contributor.id, 'right contributor');
          });
        });
      });
    });

    describe('Reference', function() {
      it('not compulsory', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar'})
        .expect(200);
      });

      it('can be empty array', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: []})
        .expect(200);
      });

      it('must exist', function() {
        return agent
        .post('/api/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: [9999]})
        .expect(404);
      });

      it('must match project', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: [ref2Id]})
        .expect(422);
      });

      it('can not be single string', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: ref1Id})
        .expect(422);
      });

      it('can have several', function(done) {
        agent
        .post('/api/projects/' + projectId + '/articles')
        .set('Authorization', token)
        .set('Content-Type', 'application/json')
        .send({title: 'Foo', body: 'bar', referenceId: [ref1Id, ref3Id], status: Article.STATUS_PUBLISHED})
        .expect(200)
        .end((err, res) => {
          if (err) done(err);
          agent
          .get('/api/articles/' + res.body.id + '/references/count')
          .expect({count: 2})
          .end(done);
        });
      });
    });
  });

  describe('Elastic search indexing', function() {
    before(done => {
      if (!server.elasticClient) {
        return done('Need elastic search client to test indexing');
      }
      done();
    });

    before(done => {
      Project.searchArticlesES(projectId, null, null, (err, res) => {
        if (err) return done(err);
        assert(res.hits, 'get results from elastic search');
        assert(res.total === 0, 'start with empty index');
        done();
      });
    });

    let sampleArticleId = null;
    let draftId = null;

    it('New article is indexed', function(done) {
      common.createSampleArticle(common.contributor, sampleArticle, projectId, (err, id) => {
        if (err) return done(err);
        sampleArticleId = id;
        Project.searchArticlesES(projectId, null, null, (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'has results');
          assert(res.total === 1, 'right number hits');
          assert(res.hits[0].id === sampleArticleId, 'right id for hit');
          assert(res.hits[0].title === sampleArticle.title, 'right title for hit');
          done();
        });
      });
    });

    it('Reference of new article is also updated in ES index', function(done) {
      Project.searchReferencesES(projectId, {query: {terms: {_id: [ref1Id]}}}, null, (err, res) => {
        if (err) return done(err);
        assert(res.hits && res.total === 1, 'has results');
        const ref = res.hits[0];
        assert(ref.id === ref1Id, 'correct hit');
        assert(ref.articles && ref.articles.length === 1, 'one article');
        assert(ref.articles[0].id === sampleArticleId, 'correct article');
        done(err);
      });
    });

    it('New draft is not indexed', function(done) {
      common.createSampleArticle(common.contributor, draftArticle, projectId, (err, id) => {
        draftId = id;
        Project.searchArticlesES(projectId, null, 'title:"' + draftArticle.title + '"', (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'has results');
          assert(res.total === 0, 'no hits');
          done();
        });
      });
    });

    it('Reference of draft mentions draft in ES index', function(done) {
      Project.searchReferencesES(projectId, {query: {terms: {_id: [ref1Id]}}}, null, (err, res) => {
        if (err) return done(err);
        assert(res.hits && res.total === 1, 'has results');
        const ref = res.hits[0];
        assert(ref.articles && ref.articles.length === 2, 'two articles');
        const draft = ref.articles.find(article => article.id === draftId);
        assert(draft.status === Article.STATUS_DRAFT, 'draft is draft');
        assert(!draft.title, 'no title for draft');
        done(err);
      });
    });

    it('Draft updated to published is indexed', function(done) {
      let newArticle = _.cloneDeep(draftArticle);
      newArticle.status = Article.STATUS_PUBLISHED;
      newArticle.projectId = projectId;
      Article.replaceById(draftId, newArticle, (err, res) => {
        if (err) return done(err);
        Project.searchArticlesES(projectId, null, 'title:"' + draftArticle.title + '"', (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'has results');
          assert(res.total === 1, '1 hit');
          assert(res.hits[0].title === draftArticle.title);
          done();
        });
      });
    });

    it('Reference of publised draft is more complete in ES index', function(done) {
      Project.searchReferencesES(projectId, {query: {terms: {_id: [ref1Id]}}}, null, (err, res) => {
        if (err) return done(err);
        assert(res.hits && res.total === 1, 'has results');
        const ref = res.hits[0];
        assert(ref.articles && ref.articles.length === 2, 'two articles');
        const draft = ref.articles.find(article => article.id === draftId);
        assert(draft.status === Article.STATUS_PUBLISHED, 'draft has been published');
        assert(draft.title === draftArticle.title, 'correct title');
        done(err);
      });
    });

    it('Changing back to draft removes from index', function(done) {
      let newArticle = _.cloneDeep(draftArticle);
      newArticle.projectId = projectId;
      Article.replaceById(draftId, newArticle, (err, res) => {
        if (err) return done(err);
        Project.searchArticlesES(projectId, null, 'title:"' + draftArticle.title + '"', (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'has results');
          assert(res.total === 0, 'no hits');
          done();
        });
      });
    });

    it('Deleted article is removed from index', function(done) {
      server.models.Article.destroyById(sampleArticleId, err => {
        if (err) return done(err);
        Project.searchArticlesES(projectId, null, null, (err, res) => {
          if (err) return done(err);
          assert(res.total === 0, 'no articles left');
          done();
        });
      });
    });
  });
});
