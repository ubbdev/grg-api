'use strict';

module.exports = function(app) {
  const ZoteroConnector = app.datasources.zotero.connector;

  // writes some of the Zotero headers to response body, if present
  ZoteroConnector.observe('after execute', function(ctx, next) {
    if (ctx.req.method === 'GET') {
      ctx.res.body = {hits: ctx.res.body};
      let total, version, link;
      if ((total = ctx.res.headers['total-results']) !== 'undefined') ctx.res.body.total = total;
      if ((version = ctx.res.headers['last-modified-version']) !== 'undefined') ctx.res.body.version = version;
      // if Zotero supplies a 'next' link, store the start value for that link
      if ((link = ctx.res.headers['link'])) {
        let tmp = link.split(', ');
        for (let i = 0; i < tmp.length; ++i) {
          if (tmp[i].match(/rel="next"/)) {
            let start = tmp[i].match(/start=(\d+)/);
            ctx.res.body.next = start[1];
          }
        }
      }
      return ctx.end(null, ctx, ctx.res.body);
    }
    return next('Only accept GET requests for Zotero');
  });
};
