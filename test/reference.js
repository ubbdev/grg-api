'use strict';

const async = require('async');
const nock = require('nock');

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

const projectId = common.projects[0].id;
let token = null;

let zoteroSyncBody = require('./mock-data/zotero-sync-body.json');
let zoteroSyncBody2 = require('./mock-data/zotero-sync-body-p2.json'); // second page
let zoteroCollections = require('./mock-data/zotero-collections.json');
let zoteroCollections2 = require('./mock-data/zotero-collections-p2.json');

function zoteroReferencesPresent(projectId, zrefArray, callback) {
  async.each(zrefArray,
    (zref, done) => server.models.Reference.find({where: {and: [{key: zref.key}, {projectId: projectId}]}}, (err, obj) => {
      if (err) return done(err);

      assert(obj.length === 1, `Zotero reference ${zref.key} registered ${obj.length} times`);
      assert(obj[0].title === zref.data.title);

      done();
    }), callback);
};

function zoteroCollectionsPresent(projectId, callback) {
  const ReferenceCollection = common.server.models.ReferenceCollection;

  ReferenceCollection.find({where: {and: [{projectId: projectId}, {name: 'GGA'}]}}, (err, collections) => {
    if (err) return callback(err);
    assert(collections.length === 1, 'only one copy of collection');

    ReferenceCollection.getTree(collections[0].id, true, (err, tree) => {
      if (err) return callback(err);

      assert(tree.children.length === 32, 'correct number children');
      assert(tree.children.some(child => child.name === 'Briefe an Grieg', 'has correct child'));

      callback();
    });
  });
}

function checkSyncSuccess(nUpdated, totalAfterUpdate, versionAfterUpdate, callback) {
  server.models.Reference.syncStatus(projectId, (err, status) => {
    if (err) return callback(err);

    assert(status.success, 'success');
    assert(status.total === nUpdated, `right number updated, expected ${nUpdated}, got ${status.total}`);
    assert(!status.error, 'no error');
    assert(status.progress === nUpdated, 'finished');
    assert(!status.active, 'not active');

    server.models.Reference.count({projectId: projectId}, (err, count) => {
      if (err) return callback(err);

      assert(count === totalAfterUpdate, 'right total number references');

      server.models.ZoteroReference.getZoteroBibVersion(projectId, (err, version) => {
        if (err) return callback(err);

        assert(!err, 'should not have error: ' + err);
        assert(version === versionAfterUpdate, `Expected version '${versionAfterUpdate}', got '${version}'`);

        callback();
      });
    });
  });
};

function setupMockCollections() {
  nock('https://api.zotero.org')
  .get('/groups/123/collections')
  .query({start: 0})
  .reply(200, JSON.stringify(zoteroCollections), {'total-results': 47, link: '<https://api.zotero.org/groups/123/collections?start=25>; rel="next", <https://api.zotero.org/groups/123/collections?start=25>; rel="last", <https://www.zotero.org/groups/123/collections>; rel="alternate"'})
  .get('/groups/123/collections')
  .query({start: 25})
  .reply(200, JSON.stringify(zoteroCollections2), {'total-results': 47, link: '<https://api.zotero.org/groups/123/collections>; rel="first", <https://api.zotero.org/groups/123/collections>; rel="prev", <https://www.zotero.org/groups/123/collections>; rel="alternate"'});
}

describe('Reference', function() {
  let currentVersion = 0;

  before(done => { token = common.admin.token; done(); });

  it('Is initialized with 0 references', function() {
    return agent
    .get('/api/References/count').expect(200, {count: 0});
  });

  describe('Authorization', function() {
    let article = null;
    let articleId = null;
    let refId = null;

    before(done => {
      server.models.Reference.create({title: 'Foo', creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId},
        (err, ref) => {
          refId = ref.id;
          article = {title: 'Foo', body: 'bar', referenceId: [refId], status: 'draft'};
          done(err);
        }
        );
    });

    before(done => common.createSampleArticle(common.contributor, article, projectId, (err, id) => {
      if (err) return done(err);
      articleId = id;
      done();
    }));

    after(done => server.models.Article.destroyAll(done));
    after(done => server.models.Reference.destroyAll(done));

    describe('Anonymous users', function() {
      // don't need to check everything, just a few interfaces to make sure it's closed
      it('can not post reference through REST API', function() {
        return agent
        .post('/api/References')
        .expect(404);
      });

      it('can not post article through reference API', function() {
        return agent
        .post('/api/References/' + refId + '/articles')
        .expect(404);
      });

      it('only get filtered information about drafts through reference API', function() {
        return agent
        .get('/api/References/' + refId + '/articles')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 1, 'correct number');
          assert(res.body[0].title === undefined, 'no title');
          assert(res.body[0].body === undefined, 'no body');
          assert(res.body[0].contributorId === undefined, 'no contributors');
        });
      });

      it('can not access article directly', function() {
        return agent
        .get('/api/References/' + refId + '/articles/' + articleId)
        .expect(404);
      });

      it('can not sync', function() {
        return agent
        .get('/api/References/sync?projectId=' + projectId)
        .expect(401);
      });

      it('can not get SyncStatus', function() {
        return agent
        .get('/api/References/syncStatus?projectId=' + projectId)
        .expect(401);
      });
    });

    it('Can not post reference as contributor through REST API', function() {
      return agent
      .post('/api/References')
      .set('Authorization', token)
      .expect(404);
    });

    it('Can not post article as contributor through reference API', function() {
      return agent
      .post('/api/References/' + refId + '/articles')
      .set('Authorization', token)
      .expect(404);
    });
  });

  describe('Zotero synchronization', function() {
    afterEach(done => setTimeout(done, 50)); // to allow changes to propagate

    after(done => {
      server.models.Reference.destroyAll(done);
    });

    it('SyncStatus gives 200 and null status when logged in', function() {
      return agent
      .get('/api/References/syncStatus?projectId=' + projectId)
      .set('Authorization', token)
      .expect(200)
      .expect(res => {
        const status = res.body;
        assert(!status.active && status.success && status.total === 0 && status.progress === 0 && !status.error, 'correct status');
      });
    });

    it('Gives 404 when called with invalid projectId', function() {
      return agent
      .get('/api/References/sync?projectId=abc')
      .set('Authorization', token)
      .expect(404);
    });

    it('Gives 422 when called without projectId', function() {
      return agent
      .get('/api/References/sync')
      .set('Authorization', token)
      .expect(422);
    });

    it('SyncStatus gives 404 when called with invalid projectId', function() {
      return agent
      .get('/api/References/syncStatus?projectId=abc')
      .set('Authorization', token)
      .expect(404);
    });

    it('SyncStatus gives 422 when called without projectId', function() {
      return agent
      .get('/api/References/syncStatus')
      .set('Authorization', token)
      .expect(422);
    });

    describe('A first sync with actual data through mock API', function() {
      before(function() {
        currentVersion = 1;
        nock('https://api.zotero.org')
        .get('/groups/123/items')
        .query(q => { return q.start === '0'; })
        .reply(200, JSON.stringify(zoteroSyncBody), {'total-results': 5, 'last-modified-version': currentVersion, 'link': '<https://api.zotero.org/groups/123/items?itemType=-note&start=5>; rel="next", <https://api.zotero.org/groups/123/items?itemType=-note&start=5>; rel="last", <https://www.zotero.org/groups/123/items>; rel="alternate"'})
        .get('/groups/123/items')
        .query(q => { return q.start === '5'; })
        .reply(200, JSON.stringify(zoteroSyncBody2), {'total-results': 5, 'last-modified-version': currentVersion, 'link': '<https://api.zotero.org/groups/123/items?itemType=-note&start=5>; rel="last", <https://www.zotero.org/groups/123/items>; rel="alternate"'})
        .get('/groups/123/deleted')
        .query(true)
        .reply(200, {items: []}, {'last-modified-version': currentVersion});

        setupMockCollections();
      });

      it('Can initiate sync', function() {
        return agent
        .get('/api/References/sync?projectId=' + projectId)
        .set('Authorization', token)
        .expect(200);
      });

      it('Has correct Status', function(done) {
        checkSyncSuccess(5, 5, currentVersion, done);
      });

      it('Has stored Zotero references', function(done) {
        async.parallel([
          next => zoteroReferencesPresent(projectId, zoteroSyncBody, next),
          next => zoteroReferencesPresent(projectId, zoteroSyncBody2, next),
        ], done);
      });

      it('Has stored collection tree', function(done) {
        zoteroCollectionsPresent(projectId, done);
      });

      it('Has made access to reference tree through project available', function(done) {
        server.models.Project.getReferenceCollectionTrees(projectId, (err, trees) => {
          if (err) return done(err);
          assert(trees, 'has result');
          assert(trees.length > 1, 'several trees');
          assert(trees.some(tree => tree.name === 'GGA'));
          assert(trees.some(tree => tree.children && tree.children.length > 0), 'some trees have children');
          done();
        });
      });
    });

    describe('A second sync request without changes at the remote', function() {
      before(function() {
        nock('https://api.zotero.org')
        .get('/groups/123/items')
        .query(q => { return q.start === '0'; })
        .reply(200, JSON.stringify([]), {'total-results': 0, 'last-modified-version': currentVersion})
        .get('/groups/123/deleted')
        .query(true)
        .reply(200, {items: []}, {'last-modified-version': currentVersion})
        .get('/groups/123/collections');

        setupMockCollections();
      });

      it('Can initiate sync', function() {
        return agent
        .get('/api/References/sync?projectId=' + projectId)
        .set('Authorization', token)
        .expect(200);
      });

      it('Correct status', function(done) {
        checkSyncSuccess(0, 5, currentVersion, done);
      });

      it('Still has same Zotero references', function(done) {
        zoteroReferencesPresent(projectId, zoteroSyncBody, done);
      });

      it('Still has same collection tree', function(done) {
        zoteroCollectionsPresent(projectId, done);
      });
    });

    describe('Change title of one reference through sync', function() {
      let modifiedBody = [zoteroSyncBody[0]];
      modifiedBody[0].data.title = 'A new title';

      before(function() {
        currentVersion++;
        nock('https://api.zotero.org')
        .get('/groups/123/items')
        .query(q => { return q.start === '0'; })
        .reply(200, JSON.stringify(modifiedBody), {'total-results': 1, 'last-modified-version': currentVersion})
        .get('/groups/123/deleted')
        .query(true)
        .reply(200, {items: []}, {'last-modified-version': currentVersion});

        setupMockCollections();
      });

      it('Can initiate sync', function() {
        return agent
        .get('/api/References/sync?projectId=' + projectId)
        .set('Authorization', token)
        .expect(200);
      });

      it('Correct status', function(done) {
        checkSyncSuccess(1, 5, currentVersion, done);
      });

      it('Title has been changed', function(done) {
        zoteroReferencesPresent(projectId, modifiedBody, done);
      });
    });

    describe('Delete a reference through sync', function() {
      before(function() {
        currentVersion++;
        nock('https://api.zotero.org')
        .get('/groups/123/items')
        .query(true)
        .reply(200, [], {'total-results': 0, 'last-modified-version': currentVersion})
        .get('/groups/123/deleted')
        .query(true)
        .reply(200, {items: [zoteroSyncBody[0].key]}, {'last-modified-version': currentVersion});

        setupMockCollections();
      });

      it('Can initiate sync', function() {
        return agent
        .get('/api/References/sync?projectId=' + projectId)
        .set('Authorization', token)
        .expect(200);
      });

      it('Correct status', function(done) {
        checkSyncSuccess(1, 4, currentVersion, done);
      });

      it('Reference actually deleted', function() {
        return agent
        .get('/api/References')
        .set('Authorization', token)
        .query({filter: JSON.stringify({where: {key: zoteroSyncBody[0].key}})})
        .expect(200, []);
      });
    });

    describe('Second delete of same reference does nothing', function() {
      before(function() {
        nock('https://api.zotero.org')
        .get('/groups/123/items')
        .query(true)
        .reply(200, [], {'total-results': 0, 'last-modified-version': currentVersion})
        .get('/groups/123/deleted')
        .query(true)
        .reply(200, {items: [zoteroSyncBody[0].key]}, {'last-modified-version': currentVersion});

        setupMockCollections();
      });

      it('Can initiate', function() {
        return agent
        .get('/api/References/sync?projectId=' + projectId)
        .set('Authorization', token)
        .expect(200);
      });

      it('Correct status', function(done) {
        checkSyncSuccess(0, 4, currentVersion, done);
      });
    });

    describe('Delete reference connected to article not possible through sync', function() {
      before(done => {
        server.models.Reference.findOne({where: {key: zoteroSyncBody[1].key}})
        .then(ref => {
          if (!ref) return done('Reference ' + zoteroSyncBody[1].key + 'not found?');
          common.createSampleArticle(common.contributor, {title: 'Foo', body: 'bar', referenceId: [ref.id]}, projectId, done);
        });
      });

      after(done => server.models.Article.destroyAll(done));

      before(function() {
        currentVersion++;
        nock('https://api.zotero.org')
        .get('/groups/123/items')
        .query(true)
        .reply(200, [], {'total-results': 0, 'last-modified-version': currentVersion})
        .get('/groups/123/deleted')
        .query(true)
        .reply(200, {items: [zoteroSyncBody[1].key]}, {'last-modified-version': currentVersion});

        setupMockCollections();
      });

      it('Can initiate', function() {
        return agent
        .get('/api/References/sync?projectId=' + projectId)
        .set('Authorization', token)
        .expect(200);
      });

      it('Correct status', function(done) {
        server.models.Reference.syncStatus(projectId, (err, status) => {
          if (err) return done(err);
          assert(status.success === false, 'sync reported error');

          server.models.Reference.count({projectId: projectId}, (err, count) => {
            if (err) return done(err);

            assert(count === 4, 'right total number references');

            done();
          });
        });
      });
    });
  });

  describe('Elastic search indexing', function() {
    let refId = null;

    before(done => {
      if (!server.elasticClient) {
        return done('Need elastic search client to test indexing');
      }
      done();
    });

    before(done => {
      server.models.Project.searchReferencesES(projectId, null, null, (err, res) => {
        if (err) return done(err);
        assert(res.hits, 'get results from elastic search');
        assert(res.total === 0, 'start with empty index');
        done();
      });
    });

    after(done => server.models.Reference.destroyAll(done));

    it('New reference is indexed', function(done) {
      let title = 'Foo';
      server.models.Reference.create({title: title, creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId}, (err, reference) => {
        if (err) return done(err);
        refId = reference.id;
        server.models.Project.searchReferencesES(projectId, null, null, (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'get results from elastic search');
          assert(res.total === 1, 'get 1 hit');
          assert(res.hits[0].id === refId, 'correct result');
          done();
        });
      });
    });

    it('Updated reference is updated in index', function(done) {
      let title = 'Bar';
      server.models.Reference.replaceById(refId, {title: title, creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId}, (err, reference) => {
        if (err) return done(err);
        server.models.Project.searchReferencesES(projectId, null, null, (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'get results from elastic search');
          assert(res.total === 1, 'get 1 hit');
          assert(res.hits[0].id === refId, 'correct result');
          assert(res.hits[0].title === title, 'correct title');
          done();
        });
      });
    });

    it('Deleted reference is removed from index', function(done) {
      server.models.Reference.destroyById(refId, err => {
        if (err) return done(err);
        server.models.Project.searchReferencesES(projectId, null, null, (err, res) => {
          assert(res.hits, 'get results from elastic search');
          assert(res.total === 0, 'empty index');
          done();
        });
      });
    });
  });
});
