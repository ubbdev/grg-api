'use strict';

const async = require('async');

module.exports = function(Concept) {
  // Do this through project API instead
  Concept.disableRemoteMethodByName('create');
  Concept.disableRemoteMethodByName('find');
  Concept.disableRemoteMethodByName('findOne');
  Concept.disableRemoteMethodByName('upsert');
  Concept.disableRemoteMethodByName('updateAll');
  Concept.disableRemoteMethodByName('replaceOrCreate');
  Concept.disableRemoteMethodByName('upsertWithWhere');
  Concept.disableRemoteMethodByName('createChangeStream');

  Concept.disableRemoteMethodByName('prototype.__create__articles');
  Concept.disableRemoteMethodByName('prototype.__upsert__articles');
  Concept.disableRemoteMethodByName('prototype.__delete__articles');
  Concept.disableRemoteMethodByName('prototype.__findById__articles');
  Concept.disableRemoteMethodByName('prototype.__updateById__articles');
  Concept.disableRemoteMethodByName('prototype.__destroyById__articles');
  Concept.disableRemoteMethodByName('prototype.__link__articles');
  Concept.disableRemoteMethodByName('prototype.__unlink__articles');

  Concept.disableRemoteMethodByName('prototype.__create__references');
  Concept.disableRemoteMethodByName('prototype.__upsert__references');
  Concept.disableRemoteMethodByName('prototype.__delete__references');
  Concept.disableRemoteMethodByName('prototype.__findById__references');
  Concept.disableRemoteMethodByName('prototype.__updateById__references');
  Concept.disableRemoteMethodByName('prototype.__destroyById__references');

  // make sure project membership is inherited
  Concept.beforeRemote('addChild', (ctx, currentInstance, next) => {
    Concept.findById(ctx.args.id, (err, parent) => {
      if (err) return next(err);
      if (!parent.projectId) return next('Can not determine parent project');
      if (ctx.req.body) ctx.req.body.projectId = parent.projectId;
      next();
    });
  });

  // for sorting
  Concept.compareNodes = function(concept1, concept2) {
    return concept1.prefLabel[0].name.localeCompare(concept2.prefLabel[0].name);
  };

  // Can not do uniqueness validation for nested properties (?)
  Concept.observe('before save', (ctx, next) => {
    const Label = Concept.app.models.Label;

    // we have ctx.instance in create() and cxt.currentInstance in updateAttribute(s)
    const instance = ctx.instance || ctx.currentInstance;
    const instancePrefLabels = instance && instance.prefLabel || [];
    const instanceAltLabels = instance && instance.altLabel || [];

    // ctx.data is defined in updateAttribute(s)
    const dataPrefLabels = ctx.data && ctx.data.prefLabel || [];
    const dataAltLabels = ctx.data && ctx.data.altLabel || [];

    // check for empty labels in input
    if (instancePrefLabels.some(label => !label.name) ||
        instanceAltLabels.some(label => !label.name) ||
        dataPrefLabels.some(label => !label.name) ||
        dataAltLabels.some(label => !label.name)) {
      return next({statusCode: 422, message: 'Labels can not be empty'});
    }

    // if we are updating one of these fields, we only need to validate the new data
    const prefLabels = dataPrefLabels.length > 0 && dataPrefLabels || instancePrefLabels;
    const altLabels = dataAltLabels.length > 0 && dataAltLabels || instanceAltLabels;

    const prefSet = new Set(prefLabels.map(Label.serialize));
    const altSet = new Set(altLabels.map(Label.serialize));

    if (prefSet.size === 0) {
      return next({statusCode: 422, message: 'Concept must have prefLabel'});
    }

    if (altLabels.length > altSet.size) {
      return next({statusCode: 422, message: 'altlabels can not be repeated'});
    }

    if (prefLabels.some(l => altSet.has(Label.serialize(l)))) {
      return next({statusCode: 422, message: 'altLabels can not be same as prefLabels'});
    }

    if (prefLabels.length) {
      let tmpSet = new Set(prefLabels.map(l => l.lang));
      if (prefLabels.length > tmpSet.size) return next({statusCode: 422, message: 'Only one prefLabel per language'});
    }
    // model definition makes sure we have prefLabels, and that prefLabel lang/name pairs are unique
    next();
  });

  // if there are references, add the reverse relation to the ES index
  Concept.observe('after save', (ctx, next) => {
    ctx.instance.references((err, references) => {
      if (err) return next(err);
      async.each(references, (reference, done) => reference.save(done), next);
    });
  });

  function saveReference(refId, callback) {
    Concept.app.models.Reference.findById(refId, (err, reference) => {
      if (err) return callback(err);
      reference.save(callback);
    });
  }

  // save the reverse to trigger ES reindexing
  Concept.afterRemote('prototype.__link__references', (ctx, data, next) => {
    saveReference(ctx.args.fk, next);
  });

  // save the reverse to trigger ES indexing
  Concept.afterRemote('prototype.__unlink__references', (ctx, data, next) => {
    saveReference(ctx.args.fk, next);
  });
};
