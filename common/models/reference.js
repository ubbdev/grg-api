'use strict';

const async = require('async');

const logger = require('loopback-component-logger')('Reference');

module.exports = function(Reference) {
  // References are only changed through sync
  Reference.disableRemoteMethodByName('create');
  Reference.disableRemoteMethodByName('replaceById');
  Reference.disableRemoteMethodByName('destroyById');
  Reference.disableRemoteMethodByName('upsert');
  Reference.disableRemoteMethodByName('upsertWithWhere');
  Reference.disableRemoteMethodByName('updateAll');
  Reference.disableRemoteMethodByName('prototype.updateAttributes');
  Reference.disableRemoteMethodByName('prototype.replace');
  Reference.disableRemoteMethodByName('replaceOrCreate');
  Reference.disableRemoteMethodByName('createChangeStream');

  // Articles are read only
  Reference.disableRemoteMethodByName('prototype.__create__articles');
  Reference.disableRemoteMethodByName('prototype.__delete__articles');
  Reference.disableRemoteMethodByName('prototype.__updateById__articles');
  Reference.disableRemoteMethodByName('prototype.__destroyById__articles');
  Reference.disableRemoteMethodByName('prototype.__findById__articles');
  Reference.disableRemoteMethodByName('prototype.__link__articles');
  Reference.disableRemoteMethodByName('prototype.__unlink__articles');

  // Collections are changed through sync
  Reference.disableRemoteMethodByName('prototype.__create__collections');
  Reference.disableRemoteMethodByName('prototype.__delete__collections');
  Reference.disableRemoteMethodByName('prototype.__updateById__collections');
  Reference.disableRemoteMethodByName('prototype.__destroyById__collections');
  Reference.disableRemoteMethodByName('prototype.__findById__collections');
  Reference.disableRemoteMethodByName('prototype.__link__collections');
  Reference.disableRemoteMethodByName('prototype.__unlink__collections');

  // Subject linking done via subject interface
  Reference.disableRemoteMethodByName('prototype.__create__subjects');
  Reference.disableRemoteMethodByName('prototype.__delete__subjects');
  Reference.disableRemoteMethodByName('prototype.__updateById__subjects');
  Reference.disableRemoteMethodByName('prototype.__destroyById__subjects');
  Reference.disableRemoteMethodByName('prototype.__findById__subjects');
  Reference.disableRemoteMethodByName('prototype.__link__subjects');
  Reference.disableRemoteMethodByName('prototype.__unlink__subjects');

  const currentSync = {};
  const syncStatusKey = 'BibliographySyncStatus';

  // Maps Zotero reference to Reference object
  function zoteroToReferenceObj(projectId, zref) {
    let concatCreators = [];
    const creators = zref.data.creators;
    if (creators) {
      for (let i = 0; i < creators.length; ++i) {
        if (creators[i].creatorType === 'author' && creators[i].firstName) {
          concatCreators.push(creators[i].lastName + ', ' + creators[i].firstName);
        }
        if (creators[i].creatorType === 'author' && creators[i].name) {
          concatCreators.push(creators[i].name);
        }
        if (creators[i].creatorType === 'editor' && creators[i].firstName) {
          concatCreators.push(creators[i].lastName + ', ' + creators[i].firstName + ' (ed.)');
        }
      }
    }
    let type = '';
    let icon = '';
    const zrefType = zref.data.itemType;
    if (zrefType) {
      if (zrefType === 'book') {
        type = 'Book';
        icon = 'book';
      }
      if (zrefType === 'journalArticle') {
        type = 'Journal article';
        icon = 'file-text-o';
      }
      if (zrefType === 'artwork') {
        type = 'Artwork';
        icon = 'book';
      }
      if (zrefType === 'audioRecording') {
        type = 'Audio Recording';
        icon = 'book';
      }
      if (zrefType === 'bill') {
        type = 'Bill';
        icon = 'book';
      }
      if (zrefType === 'blogpost') {
        type = 'Blogpost';
        icon = 'book';
      }
      if (zrefType === 'bookSection') {
        type = 'Chapter';
        icon = 'book';
      }
      if (zrefType === 'thesis') {
        type = 'Thesis';
        icon = 'book';
      }
      if (zrefType === 'presentation') {
        type = 'Presentation';
        icon = 'book';
      }
      if (zrefType === 'manuscript') {
        type = 'Manuscript';
        icon = 'book';
      }
      if (zrefType === 'magazineArticle') {
        type = 'Magazine Article';
        icon = 'book';
      }
      if (zrefType === 'letter') {
        type = 'Letter';
        icon = 'book';
      }
      if (zrefType === 'interview') {
        type = 'Interview';
        icon = 'book';
      }
    }

    return {
      projectId: projectId,
      key: zref.key,
      title: zref.data.title ? zref.data.title : 'Unknown',
      creators: concatCreators.length > 0 ? concatCreators : ['Unknown'],
      itemType: type ? type : '### MISSING ###',
      manuscriptType: zref.data.manuscriptType,
      thesisType: zref.data.thesisType,
      websiteType: zref.data.websiteType,
      letterType: zref.data.letterType,
      icon: icon,
      abstract: zref.data.abstractNote,
      issueDate: zref.data.issueDate,
      date: zref.data.date,
      doi: zref.data.DOI,
      publicationTitle: zref.data.publicationTitle,
      publication: zref.data.publication,
      issue: zref.data.issue,
      volume: zref.data.volume,
      pages: zref.data.pages,
      edition: zref.data.edition,
      series: zref.data.series,
      seriesTitle: zref.data.seriesTitle,
      seriesNumber: zref.data.seriesNumber,
      publisher: zref.data.publisher,
      place: zref.data.place,
      isbn: zref.data.isbn,
      issn: zref.data.issn,
      link: zref.links.alternate.href,
      url: zref.data.url,
      dateAdded: zref.data.dateAdded,
      dateModified: zref.data.dateModified,
      collections: zref.data.collections,
    };
  };

  // Looks for a reference with same project and Zotero key
  function upsertZoteroReference(projectId, zref, callback) {
    Reference.findOne({where: {projectId: projectId, key: zref.key}}, (err, reference) => {
      if (err) return callback(err);

      let newRef = zoteroToReferenceObj(projectId, zref);

      if (reference) return reference.updateAttributes(newRef, callback);

      Reference.create(newRef, callback);
    });
  };

  // Delete reference, will not delete it if it is linked to an article
  function deleteZoteroReference(projectId, key, callback) {
    Reference.findOne({where: {projectId: projectId, key: key}}, (err, reference) => {
      if (!reference) {
        return callback(`No reference '${key}', can not be deleted`);
      }

      reference.articles((err, articles) => {
        if (err) return callback(err);

        if (articles.length > 0) {
          return callback(`Reference '${key}' is linked to ${articles.length} article(s), will not be deleted`);
        }

        logger.info(`Deleting reference ${reference.key}`);

        reference.destroy(callback);
      });
    });
  };

  // Retrieve all new Zotero references, and store local copies
  function updateZoteroReferences(project, callback) {
    const ZoteroRef = Reference.app.models.ZoteroReference;
    const opt = project.bibliographyService.options;
    const status = currentSync[project.id];

    //helper function for recursion
    function iterUpdate(start, version, cb) {
      ZoteroRef.getUpdatedReferences(opt.groupId, version, start, opt.authorization, (err, data) => {
        if (err) return cb(err);

        status.total = data.total;
        status.progress = start;

        if (data.version === version || data.total === 0) return cb(null, data);

        logger.info(`Syncing updated references from version ${version} to ${data.version}: at ${start} of ${data.total}`);

        async.each(data.hits,
          (zref, done) => upsertZoteroReference(project.id, zref, done),
          err => {
            if (err) {
              status.error.push(err);
              return cb();
            }

            if (data.next) return iterUpdate(data.next, version, cb);
            return cb(null, data);
          });
      });
    };

    // no recursion here, but helper function used for symmetry
    function deleteReferences(version, cb) {
      ZoteroRef.getDeletedReferences(opt.groupId, version, opt.authorization, (err, data) => {
        if (err) return cb(err);
        if (data.version === version) return cb(null, data);
        if (data.hits && data.hits.items) {
          logger.info(`Syncing ${data.hits.items.length} deleted references`);
          status.total = parseInt(status.total) + data.hits.items.length;
          async.each(data.hits.items,
            (key, done) => deleteZoteroReference(project.id, key, err => {
              // if Reference wasn't deleted because it wasn't there, or it had an article, total is reduced.
              if (err) {
                status.total--;
                status.error.push(err);
              }

              // we don't want to stop syncing because of errors here
              done();
            }), cb);
        }
      });
    };

    ZoteroRef.getZoteroBibVersion(project.id, (err, version) => {
      if (err) return callback(err);
      let newVersion = 0;

      async.series([
        done => iterUpdate(0, version, (err, data) => {
          newVersion = data.version;
          done();
        }),
        done => deleteReferences(version, done),
        done => ZoteroRef.setZoteroBibVersion(project.id, newVersion, done),
      ], err => {
        logger.info(`Synced ${status.total} references from Zotero, at remote version ${newVersion}`);
        if (err) status.error.push(err);
        callback();
      });
    });
  };

  // Synchronize local bibliography with remote. For Zotero use version number and 'since'-option for api
  Reference.sync = function(projectId, callback) {
    if (!projectId) return callback({message: 'Project ID missing', statusCode: 422});

    Reference.app.models.Project.findById(projectId, (err, project) => {
      if (err) return callback(err);
      if (!project) return callback({message: `No project found with id '${projectId}'`, statusCode: 404});

      let status = currentSync[projectId];

      if (status && status.active) return callback({message: 'Synchronization already in progress', statusCode: 409});

      currentSync[projectId] = status = {
        active: true,
        error: [],
        success: false,
        total: 0,
        progress: 0,
        lastUpdated: null,
      };

      const bibType = project.bibliographyService.type;
      let update = null;

      if (bibType === 'Zotero') {
        update = updateZoteroReferences;
      } else {
        return callback({message: `Project '${projectId}' has invalid bibliography-type '${bibType}'`, statusCode: 409});
      }

      // leave function early, and let sync run in background (can take a while)
      Reference.app.models.ReferenceCollection.sync(projectId, callback);

      return update(project, () => {
        status.active = false;
        status.success = status.error.length === 0;
        status.lastUpdated = Date.now();

        if (status.success) {
          status.progress = status.total;
          status.error = null;
        }

        currentSync[projectId] = status;

        patchStatus(projectId, status, err => {
          if (err) logger.error(err);
          delete currentSync[projectId];
        });
      });
    });
  };

  Reference.syncStatus = function(projectId, callback) {
    if (!projectId) return callback({message: 'Project ID missing', statusCode: 422});

    Reference.app.models.Project.findById(projectId, (err, project) => {
      if (err) return callback(err);
      if (!project) return callback({message: `No project found with id '${projectId}'`, statusCode: 404});

      if (currentSync[projectId]) return callback(null, currentSync[projectId]);

      Reference.app.models.State.findOne({where: {projectId: projectId, key: syncStatusKey}}, (err, statusObj) => {
        if (err) return callback(err);
        if (!statusObj) {
          callback(null, {
            active: false,
            success: true,
            total: 0,
            progress: 0,
            error: null,
            lastUpdated: null,
          });
        } else {
          callback(null, statusObj.value);
        }
      });
    });
  };

  function patchStatus(projectId, value, callback) {
    Reference.app.models.State.upsertWithWhere(
      {projectId: projectId, key: syncStatusKey},
      {projectId: projectId, key: syncStatusKey, value: value},
      (err, obj) => {
        callback(err);
      });
  }

  Reference.remoteMethod('sync', {
    http: {verb: 'get'},
    accepts: [
      {arg: 'projectId', type: 'string'},
    ],
    returns: {arg: 'startedSync', type: 'boolean'},
    description: 'Synchronize all references with external bibliography, including ReferenceCollections',
  });

  Reference.remoteMethod('syncStatus', {
    http: {verb: 'get'},
    accepts: [
      {arg: 'projectId', type: 'string'},
    ],
    returns: [
      {arg: 'active', type: 'boolean', root: true},
      {arg: 'project', type: 'string', root: true},
      {arg: 'success', type: 'boolean', root: true},
      {arg: 'total', type: 'number', root: true},
      {arg: 'progress', type: 'number', root: true},
      {arg: 'error', type: 'string', root: true},
      {arg: 'lastUpdated', type: 'date'},
    ],
    description: 'Check if syncronization is running, and if it failed display error',
  });

  /**************************
   * Elasticsearch indexing *
   **************************/

  Reference.instance2indexName = function(instance) {
    return instance.projectId;
  };

  // This function includes the article and subject relations too. For the ES index to be kept in sync with the relation,
  // the Article and Concept models have been modified to also trigger a Reference save, which in turn calls the function below.
  // The alternative would have been to not include the relation and then query LB for the relation on each object returned from ES.
  Reference.instance2document = function(instance, cb) {
    instance.articles((err, articles) => {
      instance.subjects((err, subjects) => {
        let collections = [].concat(instance.collections);
        async.each(
          instance.collections,
          (key, done) => Reference.app.models.ReferenceCollection.getParentKeys(instance.projectId, key, (err, keys) => {
            if (err) return done(err);
            collections.push(keys);
            done();
          }),
          err => {
            if (err) return cb(err);
            cb(null, {
              title: instance.title,
              creators: instance.creators,
              itemType: instance.itemType,
              abstract: instance.abstract,
              issueDate: instance.issueDate,
              date: instance.date,
              doi: instance.doi,
              publicationTitle: instance.publicationTitle,
              publication: instance.publication,
              issue: instance.issue,
              volume: instance.volume,
              pages: instance.pages,
              edition: instance.edition,
              series: instance.series,
              seriesTitle: instance.seriesTitle,
              seriesNumber: instance.seriesNumber,
              publisher: instance.publisher,
              place: instance.place,
              isbn: instance.isbn,
              issn: instance.issn,
              link: instance.link,
              url: instance.url,
              collections: collections,
              articles: articles.map(article => {
                let ret = {};
                ret.status = article.status;
                ret.id = article.id;
                if (article.status !== Reference.app.models.Article.STATUS_DRAFT) {
                  ret.title = article.title;
                  ret.publicationDate = article.publicationDate;
                };
                return ret;
              }),
              subjects: subjects.map(subject => {
                return {prefLabel: subject.prefLabel, id: subject.id};
              }),
            });
          });
      });
    });
  };

  Reference.elasticsearchMapping = {
    properties: {
      title: {type: 'text'},
      creators: {type: 'keyword'},
      itemType: {type: 'keyword'},
      abstract: {type: 'text'},
      issueDate: {type: 'date'},
      //date: {type: 'date'}, // format unclear add back when figured it out
      doi: {type: 'keyword'},
      publicationTitle: {type: 'text'},
      publication: {type: 'text'},
      issue: {type: 'keyword'},
      volume: {type: 'keyword'},
      pages: {type: 'text'},
      edition: {type: 'keyword'},
      series: {type: 'text'},
      seriesTitle: {type: 'text'},
      seriesNumber: {type: 'keyword'},
      publisher: {type: 'keyword'},
      place: {type: 'keyword'},
      isbn: {type: 'keyword'},
      issn: {type: 'keyword'},
      link: {type: 'keyword'},
      url: {type: 'keyword'},
      collections: {type: 'keyword'},
      articles: {
        properties: {
          title: {type: 'text'},
        },
      },
      subjects: {
        properties: {
          prefLabel: {
            properties: {
              name: {type: 'text'},
              lang: {type: 'keyword'},
            },
          },
        },
      },
    },
  };
};
