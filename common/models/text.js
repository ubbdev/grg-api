'use strict';

module.exports = function(Text) {
  // Do this through project API instead
  Text.disableRemoteMethodByName('create');
  Text.disableRemoteMethodByName('find');
  Text.disableRemoteMethodByName('findOne');

  // Do all editing via project, simplifies role resolver
  Text.disableRemoteMethodByName('upsert');
  Text.disableRemoteMethodByName('upsertWithWhere');
  Text.disableRemoteMethodByName('updateAll');
  Text.disableRemoteMethodByName('replaceOrCreate');
  Text.disableRemoteMethodByName('replaceById');
  Text.disableRemoteMethodByName('prototype.updateAttributes');
  Text.disableRemoteMethodByName('prototype.replace');
  Text.disableRemoteMethodByName('createChangeStream');
  Text.disableRemoteMethodByName('deleteById');
};
