'use strict';

module.exports = function(PublicComment) {
  PublicComment.disableRemoteMethodByName('find');
  PublicComment.disableRemoteMethodByName('findOne');
  PublicComment.disableRemoteMethodByName('upsert');
  PublicComment.disableRemoteMethodByName('create');
  PublicComment.disableRemoteMethodByName('upsertWithWhere');
  PublicComment.disableRemoteMethodByName('updateAll');
  PublicComment.disableRemoteMethodByName('replaceOrCreate');
  PublicComment.disableRemoteMethodByName('replaceById');
  PublicComment.disableRemoteMethodByName('createChangeStream');
  PublicComment.disableRemoteMethodByName('moveNode');
  PublicComment.disableRemoteMethodByName('makeNodeRoot');
};
