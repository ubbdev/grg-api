'use strict';

const async = require('async');
const settings = require('../../server/getSettings');

const logger = require('loopback-component-logger')('Project');

module.exports = function(Project) {
  Project.validatesPresenceOf('description');

  Project.disableRemoteMethodByName('create');
  Project.disableRemoteMethodByName('upsert');
  Project.disableRemoteMethodByName('upsertWithWhere');
  Project.disableRemoteMethodByName('updateAll');
  Project.disableRemoteMethodByName('deleteById');
  Project.disableRemoteMethodByName('replaceById');
  Project.disableRemoteMethodByName('prototype.updateAttributes');
  Project.disableRemoteMethodByName('prototype.replace');
  Project.disableRemoteMethodByName('replaceOrCreate');
  Project.disableRemoteMethodByName('createChangeStream');

  Project.disableRemoteMethodByName('prototype.__get__roleMapping');
  Project.disableRemoteMethodByName('prototype.__count__roleMapping');
  Project.disableRemoteMethodByName('prototype.__findById__roleMapping');
  Project.disableRemoteMethodByName('prototype.__create__roleMapping');
  Project.disableRemoteMethodByName('prototype.__delete__roleMapping');
  Project.disableRemoteMethodByName('prototype.__destroyById__roleMapping');
  Project.disableRemoteMethodByName('prototype.__updateById__roleMapping');

  // This should only be done by Edits
  Project.disableRemoteMethodByName('prototype.__updateById__articles');

  Project.disableRemoteMethodByName('prototype.__findById__people');
  Project.disableRemoteMethodByName('prototype.__create__people');
  Project.disableRemoteMethodByName('prototype.__delete__people');
  Project.disableRemoteMethodByName('prototype.__destroyById__people');
  Project.disableRemoteMethodByName('prototype.__updateById__people');

  Project.disableRemoteMethodByName('prototype.__create__references');
  Project.disableRemoteMethodByName('prototype.__delete__references');
  Project.disableRemoteMethodByName('prototype.__destroyById__references');
  Project.disableRemoteMethodByName('prototype.__updateById__references');

  Project.disableRemoteMethodByName('prototype.__findById__edits');
  Project.disableRemoteMethodByName('prototype.__create__edits');
  Project.disableRemoteMethodByName('prototype.__delete__edits');
  Project.disableRemoteMethodByName('prototype.__destroyById__edits');
  Project.disableRemoteMethodByName('prototype.__updateById__edits');
  Project.disableRemoteMethodByName('prototype.__link__edits');
  Project.disableRemoteMethodByName('prototype.__unlink__edits');

  // This function should only be called at server boot.
  Project.observe('before save', (ctx, next) => {
    let bibService = null;
    let project = ctx.data || ctx.instance;
    bibService = project.bibliographyService;

    if (!bibService) {
      return next(`Project '${ctx.instance.id}' has no bibliographyService`);
    }

    if (!bibService.type) {
      return next(`The bibliographyService in project '${ctx.instance.id}' has no type`);
    }

    if (!bibService.options) {
      return next(`The bibliographyService in project '${ctx.instance.id}' has no options`);
    }

    if (bibService.type === 'Zotero') {
      Project.app.models.ZoteroReference.verifyProjectOptions(bibService.options, project.id, next);
    } else {
      next();
    }
  });

  /*************/
  /*** Roles ***/
  /*************/

  Project.observe('after save', (ctx, next) => {
    if (!ctx.isNewInstance) return next();
    async.each(
      ['admin', 'editor', 'contributor'],
      (role, done) => ctx.instance.roleMapping.create({role: role}, done),
      next
    );
  });

  function personHasOneOfRoles(projectId, personId, roleNames, callback) {
    Project.findById(projectId, (err, project) => {
      if (err) return callback(err);
      if (!project) return callback({statusCode: 404, message: `Project '${projectId}' not found`});
      Project.app.models.ProjectRoleMapping.find(
        {where: {projectId: projectId, role: {inq: roleNames}}},
        (err, roleMaps) => {
          async.some(roleMaps, (roleMap, done) => roleMap.people.exists(personId, done), callback);
        }
      );
    });
  };

  Project.personIsContributor = function(projectId, personId, callback) {
    personHasOneOfRoles(projectId, personId, ['contributor', 'editor', 'admin'], callback);
  };

  Project.personIsEditor = function(projectId, personId, callback) {
    personHasOneOfRoles(projectId, personId, ['editor', 'admin'], callback);
  };

  Project.personIsAdmin = function(projectId, personId, callback) {
    personHasOneOfRoles(projectId, personId, ['admin'], callback);
  };

  // gives person a role in project, if not member from before it also added as member
  Project.addMemberWithRole = function(projectId, personId, roleName, callback) {
    let roleMap = null;
    async.series(
      [
        // check that role mapping exists
        done => Project.app.models.ProjectRoleMapping.findOne({where: {projectId: projectId, role: roleName}}, (err, obj) => {
          if (!obj) return done({statusCode: 404, message: `Role '${roleName}' not registered for project ${projectId}`});
          roleMap = obj;
          done(err);
        }),

        // check that person exists
        done => Project.app.models.Person.exists(personId, (err, exists) => {
          if (!exists) return done({statusCode: 404, message: `Person with id '${personId}' does not exist`});
          done(err);
        }),

        // add person to rolemap
        done => roleMap.people.add(personId, done),

        // add person to project
        done => Project.findById(projectId, (err, project) => {
          if (err) return done(err);
          project.people.add(personId, done);
        }),
      ],
      err => callback(err, {personId: personId, projectId: projectId})
    );
  };

  // remove the a role from a member
  Project.unassignRole = function(projectId, personId, roleName, callback) {
    Project.app.models.ProjectRoleMapping.findOne(
      {where: {projectId: projectId, role: roleName}},
      (err, roleMap) => {
        if (err) return callback(err);
        if (!roleMap) return callback({statusCode: 404, message: `Role '${roleName}' not registered for project ${projectId}'`});
        roleMap.people.remove(personId, err => {
          callback(null, {projectId: projectId, personId: personId});
        });
      }
    );
  };

  // Make sure you can't unlink a person if the person has roles in the project
  Project.beforeRemote('prototype.__unlink__people', function(ctx, currentInstance, next) {
    const projectId = ctx.req.params.id;
    const personId = ctx.req.params.fk;

    personHasOneOfRoles(projectId, personId, ['admin', 'editor', 'contributor'],
      (err, hasRole) => {
        if (err) return next(err);
        if (hasRole) return next({statusCode: 409, message: `Can not remove person '${personId}' from project '${projectId}', because person has a role in project`});
        Project.findById(projectId, (err, project) => {
          if (err) return next(err);
          project.people.remove(personId, err => next(err, {projectId: projectId, personId: personId}));
        });
      }
    );
  });

  Project.getMembersWithRole = function(projectId, roleName, filter, callback) {
    if (roleName === null) return callback('Role name missing');
    Project.app.models.ProjectRoleMapping.findOne(
      {where: {projectId: projectId, role: roleName}},
      (err, roleMap) => {
        if (err) return callback(err);
        if (!roleMap) {
          return callback({statusCode: 404, message: `Role '${roleName}' not registered for project ${projectId}`});
        }
        roleMap.people(filter, callback);
      }
    );
  };

  Project.addContributor = function(projectId, personId, callback) {
    Project.addMemberWithRole(projectId, personId, 'contributor', callback);
  };

  Project.addEditor = function(projectId, personId, callback) {
    Project.addMemberWithRole(projectId, personId, 'editor', callback);
  };

  Project.addAdmin = function(projectId, personId, callback) {
    Project.addMemberWithRole(projectId, personId, 'admin', callback);
  };

  Project.removeContributor = function(projectId, personId, callback) {
    Project.unassignRole(projectId, personId, 'contributor', callback);
  };

  Project.removeEditor = function(projectId, personId, callback) {
    Project.unassignRole(projectId, personId, 'editor', callback);
  };

  Project.removeAdmin = function(projectId, personId, callback) {
    Project.unassignRole(projectId, personId, 'admin', callback);
  };

  Project.getContributors = function(projectId, callback) {
    Project.getMembersWithRole(projectId, 'contributor', null, callback);
  };

  Project.getEditors = function(projectId, callback) {
    Project.getMembersWithRole(projectId, 'editor', null, callback);
  };

  Project.getAdmins = function(projectId, callback) {
    Project.getMembersWithRole(projectId, 'admin', null, callback);
  };

  Project.remoteMethod('addContributor', {
    http: {verb: 'post', path: '/:id/contributors'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'personId', type: 'string', required: true},
    ],
    returns: {arg: 'contributor', type: 'object'},
    description: 'Add contributor to project',
  });

  Project.remoteMethod('addEditor', {
    http: {verb: 'post', path: '/:id/editors'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'personId', type: 'string', required: true},
    ],
    returns: {arg: 'editor', type: 'object'},
    description: 'Add editor to project',
  });

  Project.remoteMethod('addAdmin', {
    http: {verb: 'post', path: '/:id/admins'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'personId', type: 'string', required: true},
    ],
    returns: {arg: 'admin', type: 'object'},
    description: 'Add admin to project',
  });

  Project.remoteMethod('removeContributor', {
    http: {verb: 'delete', path: '/:id/contributors'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'personId', type: 'string', required: true},
    ],
    returns: {arg: 'contributor', type: 'object'},
    description: 'Remove contributor from project',
  });

  Project.remoteMethod('removeEditor', {
    http: {verb: 'delete', path: '/:id/editors'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'personId', type: 'string', required: true},
    ],
    returns: {arg: 'editor', type: 'object'},
    description: 'Remove editor from project',
  });

  Project.remoteMethod('removeAdmin', {
    http: {verb: 'delete', path: '/:id/admins'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'personId', type: 'string', required: true},
    ],
    returns: {arg: 'admin', type: 'object'},
    description: 'Remove admin to project',
  });

  Project.remoteMethod('getContributors', {
    http: {verb: 'get', path: '/:id/contributors'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
    ],
    returns: {arg: 'contributors', type: 'array', root: true},
    description: 'Get contributors for project',
  });

  Project.remoteMethod('getEditors', {
    http: {verb: 'get', path: '/:id/editors'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
    ],
    returns: {arg: 'editors', type: 'array', root: true},
    description: 'Get editors for project',
  });

  Project.remoteMethod('getAdmins', {
    http: {verb: 'get', path: '/:id/admins'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
    ],
    returns: {arg: 'admins', type: 'array', root: true},
    description: 'Get admins for project',
  });

  /*****************
   * Elasticsearch *
   *****************/
  if (settings.elasticsearch) {
    function mapESResult(esResponse) {
      let ret = {};
      ret.total = esResponse.hits.total;
      ret.aggregations = esResponse.aggregations;
      if (ret.total === 0) {
        ret.hits = [];
      } else {
        ret.hits = esResponse.hits.hits.map(hit => {
          let ref = hit._source;
          ref.id = hit._id;
          return ref;
        });
      }
      return ret;
    }

    Project.searchArticlesES = function(projectId, body, q, cb) {
      // elasticsearch will return an error if projectId is wrong
      Project.app.elasticClient.search({
        index: projectId.toLowerCase(),
        type: 'Article',
        body: body,
        q: q,
      }, (err, res) => {
        if (err) return cb(err);
        cb(null, mapESResult(res));
      });
    };

    Project.searchReferencesES = function(projectId, body, q, cb) {
      Project.app.elasticClient.search({
        index: projectId.toLowerCase(),
        type: 'Reference',
        body: body,
        q: q,
      }, (err, res) => {
        if (err) return cb(err);
        cb(null, mapESResult(res));
      });
    };

    Project.searchWorksES = function(projectId, body, q, cb) {
      Project.app.elasticClient.search({
        index: projectId.toLowerCase(),
        type: 'Work',
        body: body,
        q: q,
      }, (err, res) => {
        if (err) return cb(err);
        cb(null, mapESResult(res));
      });
    };

    Project.remoteMethod('searchArticlesES', {
      http: {verb: 'get', path: '/:id/articles/search'},
      accepts: [
        {arg: 'id', type: 'string', description: 'Project id', required: true},
        {arg: 'body', type: 'object', description: 'Should have an Elasticsearch query body'},
        {arg: 'q', type: 'string', description: 'Elasticsearch string query'},
      ],
      returns: [
        {arg: 'hits', type: 'array', root: true},
        {arg: 'total', type: 'number', root: true},
      ],
      description: 'Search project articles (using Elasticsearch).',
    });

    Project.remoteMethod('searchReferencesES', {
      http: {verb: 'get', path: '/:id/references/search'},
      accepts: [
        {arg: 'id', type: 'string', description: 'Project id', required: true},
        {arg: 'body', type: 'object', description: 'Should have an Elasticsearch query body'},
        {arg: 'q', type: 'string', description: 'Elasticsearch string query'},
      ],
      returns: [
        {arg: 'hits', type: 'array', root: true},
        {arg: 'total', type: 'number', root: true},
      ],
      description: 'Search project references (using Elasticsearch).',
    });

    Project.remoteMethod('searchWorksES', {
      http: {verb: 'get', path: '/:id/works/search'},
      accepts: [
        {arg: 'id', type: 'string', description: 'Project id', required: true},
        {arg: 'body', type: 'object', description: 'Should have an Elasticsearch query body'},
        {arg: 'q', type: 'string', description: 'Elasticsearch string query'},
      ],
      returns: [
        {arg: 'hits', type: 'array', root: true},
        {arg: 'total', type: 'number', root: true},
      ],
      description: 'Search project works (using Elasticsearch).',
    });

    // create project index with model mappings
    function createIndex(projectIndexId, cb) {
      const client = Project.app.elasticClient;

      client.indices.exists({index: projectIndexId}, (err, exists) => {
        if (err) return cb(err);
        if (exists) return cb();

        // ideally this only happens in tests and first time production app is started
        async.series([
          done => client.indices.create({index: projectIndexId}, done),
          done => async.each([Project.app.models.Article, Project.app.models.Reference, Project.app.models.Work],
            (model, nextModel) => client.indices.putMapping({
              index: projectIndexId,
              type: model.definition.name,
              body: model.getElasticSearchMapping(),
            }, nextModel),
            done),
        ], cb);
      });
    }

    // delete project index, recreate it, and populate
    Project.reindex = function(projectId, cb) {
      const client = Project.app.elasticClient;
      const projectIndexId = projectId.toLowerCase();

      async.series([
        done => client.indices.exists({index: projectIndexId}, (err, exists) => {
          if (err) return done(err);
          if (!exists) return done();
          client.indices.delete({index: projectIndexId}, done);
        }),
        done => createIndex(projectIndexId, done),
        done => async.each([Project.app.models.Article, Project.app.models.Reference, Project.app.models.Work],
          (model, nextModel) => model.find({where: {projectId: projectId}}, (err, instances) => {
            if (err) return done(err);
            model.populateElasticsearchIndex(instances, nextModel);
          }), done),
      ], cb);
    };

    // in case mappings change for example, or indices are corrupted, and useful for testing purposes
    Project.recreateElasticsearchIndices = function(cb) {
      Project.find((err, projects) => {
        if (err) return cb(err);
        async.each(projects, (project, done) => Project.reindex(project.id, done), cb);
      });
    };

    Project.remoteMethod('reindex', {
      http: {verb: 'put', path: '/:id/reindex'},
      accepts: [
        {arg: 'id', type: 'string', required: true, description: 'Project id'},
      ],
      description: 'Recreate Elasticsearch index from scratch',
    });

    Project.observe('before save', (ctx, next) => {
      if (!ctx.isNewInstance) return next();
      const projectIndexId = ctx.instance.id.toLowerCase();
      createIndex(projectIndexId, next);
    });
  }

  /*****************************/
  /*** Reference collections ***/
  /*****************************/

  Project.getReferenceCollectionTrees = function(projectId, cb) {
    Project.findById(projectId, (err, project) => {
      if (err) return cb(err);
      if (!project) return cb({statusCode: 404, message: 'Project not found'});
      project.referenceCollections({where: {parent: null}}, (err, rootCollections) => {
        if (err) return cb(err);
        let result = [];
        async.each(rootCollections, (collection, done) => {
          Project.app.models.ReferenceCollection.getTree(collection.id, true, (err, tree) => {
            if (err) return done(err);
            result.push(tree);
            done();
          });
        }, err => cb(err, result));
      });
    });
  };

  Project.remoteMethod('getReferenceCollectionTrees', {
    http: {verb: 'get', path: '/:id/referenceCollections/trees'},
    accepts: [
      {arg: 'id', type: 'string', required: true, description: 'Project id'},
    ],
    returns: {arg: 'trees', type: 'array', root: true},
    description: 'Get all ReferenceCollections as array of trees',
  });

  /*********************/
  /*** Notifications ***/
  /*********************/

  // Send email message to all admins of a project, message parameter should have format {subject: '...', text: '...'}
  Project.notifyAdmins = function(projectId, message, callback) {
    if (!message.subject) return callback('Message has no subject');
    if (!message.text) return callback('Message has no text');

    Project.getAdmins(projectId, (err, admins) => {
      if (err) return callback(err);

      const recipients = admins.map(admin => admin.email);

      if (recipients.length === 0) {
        logger.warn(`Project '${projectId}' has no admins to notify, message '${JSON.stringify(message)}'' not sent`);
        return callback();
      }

      Project.app.sendMail({
        to: recipients,
        subject: message.subject,
        text: message.text,
      }, callback);
    });
  };
};

