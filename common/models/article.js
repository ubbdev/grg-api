'use strict';

const async = require('async');
const _ = require('lodash');

module.exports = function(Article) {
  Article.STATUS_PUBLISHED = 'published';
  Article.STATUS_PUBLIC_DRAFT = 'public-draft';
  Article.STATUS_DRAFT = 'draft';

  // Do this through project API instead
  Article.disableRemoteMethodByName('create');
  Article.disableRemoteMethodByName('find');
  Article.disableRemoteMethodByName('findOne');

  // Most changes take place via edits
  Article.disableRemoteMethodByName('replaceById');
  Article.disableRemoteMethodByName('upsert');
  Article.disableRemoteMethodByName('upsertWithWhere');
  Article.disableRemoteMethodByName('updateAll');
  Article.disableRemoteMethodByName('prototype.updateAttributes');
  Article.disableRemoteMethodByName('prototype.replace');
  Article.disableRemoteMethodByName('replaceOrCreate');
  Article.disableRemoteMethodByName('createChangeStream');

  // Can only modify contributors through edits
  Article.disableRemoteMethodByName('prototype.__create__contributors');
  Article.disableRemoteMethodByName('prototype.__delete__contributors');
  Article.disableRemoteMethodByName('prototype.__updateById__contributors');
  Article.disableRemoteMethodByName('prototype.__destroyById__contributors');
  Article.disableRemoteMethodByName('prototype.__link__contributors');
  Article.disableRemoteMethodByName('prototype.__unlink__contributors');

  // Can only modify references through edits
  Article.disableRemoteMethodByName('prototype.__create__references');
  Article.disableRemoteMethodByName('prototype.__delete__references');
  Article.disableRemoteMethodByName('prototype.__updateById__references');
  Article.disableRemoteMethodByName('prototype.__destroyById__references');
  Article.disableRemoteMethodByName('prototype.__link__references');
  Article.disableRemoteMethodByName('prototype.__unlink__references');

  // Can only modify subjects through edits
  Article.disableRemoteMethodByName('prototype.__create__subjects');
  Article.disableRemoteMethodByName('prototype.__delete__subjects');
  Article.disableRemoteMethodByName('prototype.__updateById__subjects');
  Article.disableRemoteMethodByName('prototype.__destroyById__subjects');
  Article.disableRemoteMethodByName('prototype.__link__subjects');
  Article.disableRemoteMethodByName('prototype.__unlink__subjects');

  // Can only modify relatedWorks through edits
  Article.disableRemoteMethodByName('prototype.__create__relatedWorks');
  Article.disableRemoteMethodByName('prototype.__delete__relatedWorks');
  Article.disableRemoteMethodByName('prototype.__updateById__relatedWorks');
  Article.disableRemoteMethodByName('prototype.__destroyById__relatedWorks');
  Article.disableRemoteMethodByName('prototype.__link__relatedWorks');
  Article.disableRemoteMethodByName('prototype.__unlink__relatedWorks');

  // Comments are created via article API but edited via comments API
  Article.disableRemoteMethodByName('prototype.__delete__publicComments');
  Article.disableRemoteMethodByName('prototype.__upsert__publicComments');
  Article.disableRemoteMethodByName('prototype.__updateById__publicComments');
  Article.disableRemoteMethodByName('prototype.__destroyById__publicComments');
  Article.disableRemoteMethodByName('prototype.__delete__editorialComments');
  Article.disableRemoteMethodByName('prototype.__upsert__editorialComments');
  Article.disableRemoteMethodByName('prototype.__updateById__editorialComments');
  Article.disableRemoteMethodByName('prototype.__destroyById__editorialComments');

  /********************************/
  /*** Hide drafts if necessary ***/
  /********************************/
  function filterDrafts(ctx, projectIds, next) {
    const allowedStatus = [Article.STATUS_PUBLISHED, Article.STATUS_PUBLIC_DRAFT];
    ctx.data = (allowedStatus.includes(ctx.data.status) || projectIds.includes(ctx.data.projectId)) ?
      ctx.data :
      {
        id: ctx.data.id,
        status: ctx.data.status,
        projectId: ctx.data.projectId,
        referenceId: ctx.data.referenceId,
      };
    next();
  }

  Article.observe('loaded', (ctx, next) => {
    if (ctx.isNewInstance) return next();
    const personId = ctx.options && ctx.options.accessToken && ctx.options.accessToken.userId;
    const authorizedRoles = ctx.options && ctx.options.authorizedRoles;

    if (!ctx.data) return next();
    if (!personId) return filterDrafts(ctx, [], next);

    // short cut when these roles are defined (but they aren't always, for example articleReader when requesting several articles)
    if (authorizedRoles && (authorizedRoles.articleContributor || authorizedRoles.articleReader || authorizedRoles.projectEditor)) return next();

    Article.app.models.Person.findById(personId, (err, person) => {
      if (err) return next(err);
      if (!person) return filterDrafts(ctx, [], next);
      person.projects(null, (err, projects) => {
        if (err) return next(err);
        if (!projects) return filterDrafts(ctx, [], next);
        return filterDrafts(ctx, projects.map(p => { return p.id; }), next);
      });
    });
  });

  /******************/
  /*** Validation ***/
  /******************/

  Article.validatesPresenceOf('projectId');
  Article.validatesInclusionOf('status', {in: [Article.STATUS_PUBLISHED, Article.STATUS_PUBLIC_DRAFT, Article.STATUS_DRAFT]});

  Article.validateAsync('contributorId', validateContributors, {message: 'Contributor invalid'});
  Article.validateAsync('referenceId', validateReferences, {message: 'References invalid'});

  // Check that person exists and belongs to correct project.
  // Used for contributors other than the caller, since these can't be handled using the role resolver.
  // This means some people are checked twice, depending if the caller is a contributor or not.
  function validatePerson(projectId, personId, callback) {
    Article.app.models.Person.findById(personId, (err, person) => {
      if (err) return callback(err);
      if (!person) return callback({message: 'Person does not exist', statusCode: 404});

      Article.app.models.Project.personIsContributor(projectId, personId,
        (err, isMember) => {
          if (err) return callback(err);
          if (!isMember) return callback({message: `Person '${person.name}' not registered for project '${projectId }'`, statusCode: 409});

          return callback();
        }
      );
    });
  }

  // Make sure added contributors are in an array and belong to the same project
  function validateContributors(errCallback, doneCallback) {
    if (this.contributorId && !(this.contributorId instanceof Array)) {
      return process.nextTick(() => {
        errCallback();
        doneCallback();
      });
    }

    async.each(this.contributorId,
      (id, done) => validatePerson(this.projectId, id, done),
      err => {
        if (err) errCallback();
        doneCallback();
      });
  }

  // Make sure added references are in an array and belong to the same project
  function validateReferences(errCallback, doneCallback) {
    const referenceId = this.referenceId;
    const projectId = this.projectId;

    if (referenceId && !(referenceId instanceof Array)) {
      errCallback();
      return doneCallback();
    }
    if (!referenceId || !referenceId.length) return doneCallback();

    async.each(referenceId, (id, next) => {
      Article.app.models.Reference.findById(id, (err, reference) => {
        if (err) return next(err);
        if (!reference) return next(new Error());
        if (reference.projectId !== projectId) {
          return next(new Error());
        }
        return next();
      });
    }, err => {
      if (err) errCallback();
      doneCallback();
    });
  }

  /**********************************************/
  /*** Build relations if posted as id arrays ***/
  /**********************************************/

  Article.observe('after save', (ctx, next) => {
    if (!ctx.isNewInstance) return next();

    let currentInstance = ctx.instance;

    async.parallel([
      done => addRelation(currentInstance.referenceId, Article.app.models.Reference, currentInstance.references, done),
      done => addRelation(currentInstance.contributorId, Article.app.models.Person, currentInstance.contributors, done),
      done => addRelation(currentInstance.conceptId, Article.app.models.Concept, currentInstance.subjects, done),
      done => addRelation(currentInstance.workId, Article.app.models.Work, currentInstance.relatedWorks, done),
    ], next);
  });

  function addRelation(relationId, relationModel, relation, callback) {
    if (!relationId) return callback();

    async.each(relationId, (id, done) => {
      relationModel.findById(id, (err, obj) => {
        if (err) return done(err);
        relation.add(obj, (err, relation) => {
          if (err) return done(err);
          obj.save(done); // to trigger ElasticSearch indexing of relation (pushToReference() is not always called when relations have been created)
        });
      });
    }, callback);
  }

  /***************/
  /*** Editing ***/
  /***************/

  // Most of Edit validation is done in edit.js, in this file we do the checks that depend on remoting context,
  // since all remote editing happens via the Article API.

  // check if person is owner of edit
  function verifyPersonIsEditOwner(edit, personId, article, cb) {
    if (edit.personId.toString() !== personId.toString()) {
      return cb({message: `Article '${article.title}' is already being edited`, statusCode: 409});
    }
    return cb();
  };

  // Do some verification and initialization that's impossible to do on Edit model, or more natural to put here.
  Article.beforeRemote('prototype.__create__edits', function(ctx, currentInstance, next) {
    const article = ctx.instance;
    const personId = Article.app.models.Person.getIdFromRequest(ctx.req);
    const body = ctx.req.body;

    if (_.isEmpty(body) === false) {
      return next({statusCode: 422, message: 'Non-empty edit. Can only use POST article/:id/edits to establish edits (for blocking). ' +
        'Add content through PUT article/:id/edits/:fk'});
    }

    article.edits({where: {finished: false}}, (err, edits) => {
      if (err) return next(err);
      if (edits.length > 0) {
        verifyPersonIsEditOwner(edits[0], personId, article, err => {
          if (err) return next(err);
          ctx.res.send(edits[0]);
        });
      } else {
        next();
      }
    });
  });

  function verifyPersonCanChangeStatus(projectId, personId, cb) {
    Article.app.models.Project.personIsEditor(projectId, personId, (err, isEditor) => {
      if (err) return cb(err);
      if (!isEditor) return cb({statusCode: 401, message: 'Only editors can change status of an article'});
      return cb();
    });
  }

  Article.beforeRemote('prototype.__updateById__edits', function(ctx, currentInstance, next) {
    const article = ctx.instance;
    const personId = Article.app.models.Person.getIdFromRequest(ctx.req);
    const body = ctx.req.body;

    article.edits({where: {id: ctx.req.params.fk}}, (err, edits) => {
      if (err) return next(err);
      if (edits.length === 0) return next({statusCode: 404, message: 'Edit not found'});
      if (edits.length > 1) return next({statusCode: 422, message: 'Only one edit at a time'}); // should never happen
      async.series([
        done => verifyPersonIsEditOwner(edits[0], personId, article, done),
        done => {
          if (body.status && body.status !== ctx.instance.status)
            return verifyPersonCanChangeStatus(ctx.instance.projectId, personId, done);
          return done();
        },
      ], next);
    });
  });

  /**************************
   * Elasticsearch indexing *
   **************************/

  // This is necessary to trigger Elasticsearch indexing of the relation
  Article.observe('after save', function pushToReference(ctx, next) {
    ctx.instance.references((err, references) => {
      if (err) return next(err);
      async.each(references, (reference, done) => reference.save(done), next);
    });
  });

  Article.excludeFromElasticsearch = function(instance) {
    return instance.status === Article.STATUS_DRAFT;
  };

  Article.instance2document = function(instance, cb) {
    instance.contributors((err, contributors) => {
      if (err) return cb(err);
      instance.references((err, references) => {
        if (err) return cb(err);
        cb(null, {
          title: instance.title,
          body: instance.body,
          status: instance.status,
          publicationDate: instance.publicationDate,
          contributors: contributors,
          references: references,
        });
      });
    });
  };

  Article.instance2indexName = function(instance) {
    return instance.projectId;
  };

  Article.elasticsearchMapping = {
    properties: {
      title: {type: 'text'},
      body: {type: 'text'},
      status: {type: 'keyword'},
      pulicationDate: {type: 'date'},
    },
  };
};
