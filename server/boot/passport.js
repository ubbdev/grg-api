'use strict';

const PassportConfigurator = require('loopback-component-passport').PassportConfigurator;
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const settings = require('../getSettings');
const crypto = require('crypto');

const logger = require('loopback-component-logger')('Passport');

const cookieSecret = settings.cookieSecret;

function generatePassword(key) {
  let hmac = crypto.createHmac('sha1', key);
  let buf = crypto.randomBytes(32);
  hmac.update(buf);
  return hmac.digest('hex');
}

// converts profile from Dataporten to Loopback user
function dataportenProfileToUser(provider, profile, options) {
  return {
    'username': profile.username,
    'name': profile.displayName,
    'email': profile.emails[0].value,
    'password': generatePassword(cookieSecret),
  };
};

// throws error if config invalid
module.exports = function(app) {
  // Create an instance of PassportConfigurator with the app instances

  const passportConfigurator = new PassportConfigurator(app);

  let passportConfig = {};

  // For each project extract the providers and add them to passportConfig.
  // Prepend each provider name with the project ID to keep them apart.
  // Auto generates callbackPath, authPath and successRedirect to incorporate project ID,
  // to support having separate logins for separate projects.
  settings.projects.forEach(project => {
    if (!project.providers || Object.keys(project.providers).length === 0) {
      throw Error('Each project must have at least one authentication provider');
    }

    Object.keys(project.providers).forEach(key => {
      let provider = project.providers[key];

      // This works for Dataporten, and probably for other Oauth providers,
      // but might need to be more sophisticated to support the general case.
      if (provider.provider !== 'local') {
        const callbackPath = '/auth/' + project.id + '/' + key + '/callback';
        if (!provider.callbackURL.match(callbackPath)) {
          const callbackTemplate = '/auth/{projectID}/{providerName}/callback';
          throw Error(`Each provider must have callbackURL with the path '${callbackTemplate}'`);
        }
        provider.callbackPath = callbackPath;
      }

      provider.authPath = '/auth/' + project.id + '/' + key;
      provider.successRedirect = '/auth/' + project.id + '/account';

      passportConfig[project.id + '-' + key] = provider;
    });
  });

  passportConfigurator.init();

  // Set up related models
  passportConfigurator.setupModels({
    userModel: app.models.Person,
    userIdentityModel: app.models.PersonIdentity,
    userCredentialModel: app.models.PersonCredential,
  });

  // Configure passport strategies for third party auth providers
  for (let s in passportConfig) {
    let c = passportConfig[s];
    c.session = c.session !== false;
    if (c.provider === 'dataporten') {
      c.profileToUser = dataportenProfileToUser;
    }
    passportConfigurator.configureProvider(s, c);
  }

  // makes the person member of the project they logged in to
  app.get('/auth/:projectId/account', ensureLoggedIn('/login'), function(req, res, next) {
    app.models.Person.findById(req.user.id, (err, person) => {
      if (err) return next(err);

      if (!person) return next(new Error('Internal error')); // should never happen

      const projectId = req.params.projectId;

      person.projects((err, projects) => {
        if (err) return next(err);

        if (projects.some(project => project.id === projectId)) return next();

        person.projects.add(projectId, (err, relation) => {
          if (err) return next(err);

          logger.info(`Adding ${req.user.name} as member in project ${projectId}`);

          app.models.Project.notifyAdmins(projectId, {
            subject: 'new user',
            text: `User ${req.user.name} logged in to ${projectId} for the first time.`,
          }, next);
        });
      });
    });
  });

  // Redirects to FrontEnd after successful login
  app.get('/auth/:projectId/account', ensureLoggedIn('/login'), function(req, res, next) {
    // in tests we need structured data to reuse access token
    if (process.env.NODE_ENV === 'test') {
      res.type('application/json');
      return res.send(req.user);
    }

    const frontEndHome = settings.projects.find(project => project.id === req.params.projectId).frontEndHome;

    if (frontEndHome) {
      res.redirect(frontEndHome + '/passport/' + req.accessToken.id + '/' + req.accessToken.userId);
    } else {
      res.type('text/plain');
      res.send('logged in with token ' + req.accessToken.id);
    }
  });

  app.get('/login', function(req, res, next) {
    res.status('401');
    res.type('text/plain');
    let msg = 'Unauthorized.';
    if (req.session && req.session.flash && req.session.flash.error) {
      msg += '\nError(s):\n' + req.session.flash.error.join('\n');
    }
    res.send(msg);
  });

  app.get('/local', function(req, res, next) {
    res.send('Not logged in local');
  });

  app.get('/auth/logout', function(req, res, next) {
    req.logout();
    res.redirect('/');
  });
};
