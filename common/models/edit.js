'use strict';

const async = require('async');
const _ = require('lodash');

module.exports = function(Edit) {
  // Edits are meant to only be created and changed via their Article
  Edit.disableRemoteMethodByName('create');
  Edit.disableRemoteMethodByName('upsert');
  Edit.disableRemoteMethodByName('replaceOrCreate');
  Edit.disableRemoteMethodByName('upsertWithWhere');
  Edit.disableRemoteMethodByName('createChangeStream');
  Edit.disableRemoteMethodByName('updateAll');
  Edit.disableRemoteMethodByName('prototype.updateAttributes');
  Edit.disableRemoteMethodByName('prototype.patchAttributes');
  Edit.disableRemoteMethodByName('replaceById');
  // deletion allowed

  /******************/
  /*** Validation ***/
  /******************/

  Edit.validateAsync('references', referencesExist, {message: 'At least one of the references does not exist'});
  Edit.validateAsync('references', referencesBelongToProject, {message: 'Reference belongs to wrong project'});

  Edit.validateAsync('contributors', contributorsExist, {message: 'At least one of the subjects does not exist'});
  Edit.validateAsync('contributors', contributorsBelongToProject, {message: 'Person is not contributor in project'});

  Edit.validateAsync('subjects', subjectsExist, {message: 'At least one of the subjects does not exist'});
  Edit.validateAsync('subjects', subjectsBelongToProject, {message: 'Subject belongs to wrong project'});

  Edit.validateAsync('relatedWorks', worksExist, {message: 'At least one of the works does not exist'});
  Edit.validateAsync('relatedWorks', worksBelongToProject, {message: 'Work belongs to wrong project'});

  // helper function for validation
  function getEditProjectId(edit, callback) {
    if (!edit.articleId) return callback('No article Id');

    Edit.app.models.Article.findById(edit.articleId, (err, article) => {
      if (err) return callback(err);
      if (!article) return callback('Invalid edit: can not access article');
      callback(null, article.projectId);
    });
  }

  // checks if relationIds exist in Model
  function relationObjExists(relationIds, Model, errCallback, doneCallback) {
    if (!relationIds) return doneCallback();

    async.each(relationIds, (id, next) => Model.exists(id, (err, exists) => {
      if (err) return next(err);
      if (!exists) return next(new Error());
      next();
    }), err => {
      if (err) errCallback();
      return doneCallback();
    });
  }

  // checks if relationIds are in the same project as the edit
  function relationObjBelongsToProject(edit, relationIds, Model, errCallback, doneCallback) {
    if (!relationIds) return doneCallback();

    getEditProjectId(edit, (err, projectId) => {
      if (err || !projectId) {
        errCallback();
        return doneCallback();
      }
      async.each(relationIds, (id, done) => {
        Model.findById(id, (err, obj) => {
          if (err || !obj) return done(new Error());

          if (obj.projectId !== projectId) {
            return done(new Error());
          }

          return done();
        });
      }, err => {
        if (err) errCallback();
        return doneCallback();
      });
    });
  }

  /*** Reference validation ***/

  function referencesExist(errCallback, doneCallback) {
    relationObjExists(this.references, Edit.app.models.Reference, errCallback, doneCallback);
  }

  function referencesBelongToProject(errCallback, doneCallback) {
    relationObjBelongsToProject(this, this.references, Edit.app.models.Reference, errCallback, doneCallback);
  }

  /*** Contributor validation ***/

  function contributorsExist(errCallback, doneCallback) {
    relationObjExists(this.contributors, Edit.app.models.Person, errCallback, doneCallback);
  }

  // contributors need to have the project role contributor
  function contributorsBelongToProject(errCallback, doneCallback) {
    getEditProjectId(this, (err, projectId) => {
      if (err || !projectId) {
        errCallback();
        return doneCallback();
      }
      async.each(this.contributors,
        (personId, done) => Edit.app.models.Project.personIsContributor(projectId, personId, (err, isContributor) => {
          if (err || !isContributor) return done(new Error());
          done();
        }),
        err => {
          if (err) errCallback();
          return doneCallback();
        });
    });
  }

  /*** Subject validation ***/

  function subjectsExist(errCallback, doneCallback) {
    relationObjExists(this.subjects, Edit.app.models.Concept, errCallback, doneCallback);
  }

  function subjectsBelongToProject(errCallback, doneCallback) {
    relationObjBelongsToProject(this, this.subjects, Edit.app.models.Concept, errCallback, doneCallback);
  }

  /*** Work validation ***/

  function worksExist(errCallback, doneCallback) {
    relationObjExists(this.relatedWorks, Edit.app.models.Work, errCallback, doneCallback);
  }

  function worksBelongToProject(errCallback, doneCallback) {
    relationObjBelongsToProject(this, this.relatedWorks, Edit.app.models.Work, errCallback, doneCallback);
  }

  /***********************/
  /*** Operation hooks ***/
  /***********************/

  // A new Edit copies all information from its parent Article
  Edit.observe('before save', function setInitialContents(ctx, next) {
    if (!ctx.isNewInstance) return next();

    let edit = ctx.instance;

    Edit.app.models.Article.findById(ctx.instance.articleId, ctx.options, (err, article) => {
      if (err) next(err);

      edit.status = article.status;
      edit.projectId = article.projectId;
      edit.body = article.body;
      edit.title = article.title;

      async.each(['contributors', 'references', 'subjects', 'relatedWorks'],
        (relation, done) => article[relation]((err, res) => {
          edit[relation] = [];
          if (err) return done(err);
          res.forEach(item => edit[relation].push(item.id));
          return done();
        }), next);
    });
  });

  // Once an edit is marked as finished it can't be overwritten
  Edit.observe('before save', function cannotOverwriteFinishedEdit(ctx, next) {
    if (ctx.isNewInstance) return next();

    if (ctx.currentInstance && ctx.currentInstance.finished === true) {
      return next({message: 'Edit already marked as finished, start a new one', statusCode: 409});
    }

    return next();
  });

  // An edit can not change the contributors list to empty.
  // Only checked when contributors is actually specified in the input data.
  Edit.observe('before save', function canNotHaveEmptyContributors(ctx, next) {
    if (ctx.data && ctx.data.contributors && ctx.data.contributors.length === 0) {
      return next({statusCode: 422, message: 'Contributors can not be empty'});
    }
    next();
  });

  // if new relation Ids are specified, the relation is updated with its new values
  function overwriteRelation(newRelationIds, relation, cb) {
    if (!newRelationIds) return cb();

    relation({}, (err, res) => {
      if (err) return cb();

      let oldRelationIds = res.map(r => r.id);

      let relationIdsToAdd = newRelationIds.filter(newId => !oldRelationIds.some(oldId => oldId === newId));
      let relationIdsToRemove = oldRelationIds.filter(oldId => !newRelationIds.some(newId => oldId === newId));

      async.series([
        done => async.each(relationIdsToRemove, relation.remove, done),
        done => async.each(relationIdsToAdd, relation.add, done),
      ], cb);
    });
  }

  // When an Edit is finished, update Article.
  Edit.observe('after save', function patchArticle(ctx, next) {
    const edit = ctx.instance;
    const data = ctx.data || {};

    if (!edit.finished) return next();

    Edit.app.models.Article.findById(edit.articleId, (err, article) => {
      if (err) return next(err);
      if (!article) return next('No article'); // should never happen

      if (edit.status && edit.status === Edit.app.models.Article.STATUS_PUBLISHED && edit.status !== article.status) {
        article.publicationDate = Date.now();
      }

      article.title = data.title || edit.title;
      article.body = data.body || edit.body;
      article.status = data.status || edit.status;

      async.series([
        done => overwriteRelation(edit.references, article.references, done),
        done => overwriteRelation(edit.contributors, article.contributors, done),
        done => overwriteRelation(edit.subjects, article.subjects, done),
        done => overwriteRelation(edit.relatedWorks, article.relatedWorks, done),
        done => article.save(done),
      ], err => {
        if (err) {
          edit.finished = false;
          return edit.save(err2 => {
            if (err2) {
              return next([err, err2]);
            }
            return next(err);
          });
        }
        next();
      });
    });
  });
};
