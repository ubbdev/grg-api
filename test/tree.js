'use strict';

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

const async = require('async');

const projectId = 'Test';

// Will test Tree mixin through Comment base class

describe('Tree', function() {
  let commentId1 = null;
  let commentId2 = null;
  let commentId3 = null;
  let commentId4 = null;

  let Comment = null;

  // for convenience
  before(done => {
    Comment = server.models.Comment;
    done();
  });

  after(done => Comment.destroyBranch(commentId1, done));

  before(() => Comment.create({body: 'a comment'}).then(comment => { commentId1 = comment.id; }));
  before(() => Comment.create({body: 'a second comment'}).then(comment => { commentId2 = comment.id; }));

  it('new single node has null parent and children = []', function() {
    return Comment
    .findById(commentId1)
    .then(comment => {
      assert(comment.parent === null, 'null parent');
      assert(comment.children && comment.children.length === 0, 'empty children');
    });
  });

  describe('addChild() errors:', function() {
    it('can not add empty child', function(done) {
      Comment.addChild(commentId1, null, null, (err, child) => {
        assert(err && err.statusCode === 422, 'correct error');
        done();
      });
    });

    it('can not add both child object and childId', function(done) {
      Comment.addChild(commentId1, commentId2, {body: 'child'}, (err, child) => {
        assert(err && err.statusCode === 422, 'correct error');
        done();
      });
    });

    it('can not add child to non-existent parent', function(done) {
      Comment.addChild(1, commentId1, null, (err, child) => {
        assert(err && err.statusCode === 404, 'correct error');
        done();
      });
    });

    it('can not add erroneous child', function(done) {
      Comment.addChild(commentId1, null, {}, (err, child) => {
        assert(err && err.statusCode === 422, 'correct error');
        done();
      });
    });

    it('can not add non-existent child', function(done) {
      Comment.addChild(commentId1, 1, null, (err, child) => {
        assert(err && err.statusCode === 404, 'correct error');
        done();
      });
    });
  });

  describe('build tree: ', function() {
    it('can add object', function(done) {
      Comment.addChild(commentId1, null, {body: 'child'}, (err, child) => {
        if (err) return done(err);
        assert(child.body === 'child', 'correct body');
        assert(child.parent === commentId1, 'correct parent');
        commentId3 = child.id;
        Comment.findById(commentId1, (err, comment) => {
          if (err) return done(err);
          assert(comment.children && comment.children.length === 1, 'correct number children');
          assert(comment.children[0] === commentId3, 'correct child');
          done();
        });
      });
    });

    it('can add id', function(done) {
      Comment.addChild(commentId1, commentId2, null, (err, child) => {
        if (err) return done(err);
        assert(child.body === 'a second comment', 'correct body');
        assert(child.id === commentId2, 'correct id');
        assert(child.parent === commentId1, 'correct parent');
        Comment.findById(commentId1, (err, comment) => {
          if (err) return done(err);
          assert(comment.children && comment.children.length === 2, 'correct number children');
          assert(comment.children[0] === commentId3, 'correct first child');
          assert(comment.children[1] === commentId2, 'correct second child');
          done();
        });
      });
    });

    it('can not add same id twice', function(done) {
      Comment.addChild(commentId1, commentId2, null, (err, child) => {
        assert(err && err.statusCode === 409, 'correct error');
        Comment.findById(commentId1, (err, comment) => {
          if (err) return done(err);
          assert(comment.children && comment.children.length === 2, 'correct number children');
          assert(comment.children[0] === commentId3, 'correct first child');
          assert(comment.children[1] === commentId2, 'correct second child');
          done();
        });
      });
    });

    it('can add third level', function(done) {
      Comment.addChild(commentId2, null, {body: 'third level comment'}, (err, child) => {
        if (err) return done(err);
        assert(child.body === 'third level comment', 'correct body');
        assert(child.parent === commentId2, 'correct parent');
        commentId4 = child.id;

        Comment.findById(commentId2, (err, comment) => {
          if (err) return done(err);
          assert(comment.children && comment.children.length === 1, 'correct number children');
          assert(comment.children[0] === commentId4, 'correct child');
          assert(comment.parent === commentId1, 'correct parent');
          done();
        });
      });
    });

    it('can access whole tree', function(done) {
      Comment.getTree(commentId1, true, (err, tree) => {
        if (err) return done(err);
        assert(tree.id === commentId1, 'tree has correct root');
        // because these also have the article-comment mixin, they will be returned sorted by creation date
        assert(tree.children[0].id === commentId2, 'tree has correct first branch');
        assert(tree.children[1].id === commentId3, 'tree has correct second branch');
        assert(tree.children[0].children.length === 1, 'first branch has one more level');
        assert(tree.children[0].children[0].id === commentId4, 'first branch has correct leaf');
        assert(tree.children[0].children[0].body === 'third level comment', 'first branch leaf has correct body');
        assert(tree.children[1].children.length === 0, 'second branch has only one level');
        done();
      });
    });

    it('can access first level of tree', function(done) {
      Comment.getTree(commentId1, (err, tree) => {
        if (err) return done(err);
        assert(tree.id === commentId1, 'tree has correct root');
        assert(tree.children[0].id === commentId2, 'tree has correct first branch');
        assert(tree.children[1].id === commentId3, 'tree has correct second branch');
        assert(tree.children[0].children.length === 1, 'first branch has one more level');
        assert(tree.children[0].children[0] === commentId4, 'lower levels only as id');
        done();
      });
    });

    it('can not delete node with children', function(done) {
      Comment.destroyById(commentId1, err => {
        assert(err && err.statusCode === 409, 'correct error');
        done();
      });
    });

    it('can not move node if it creates circular graph', function(done) {
      Comment.moveNode(commentId2, commentId4, err => {
        assert(err, 'has error');
        assert(err.statusCode === 422, 'correct error code');
        done();
      });
    });

    it('can move node that had no parent previously', function(done) {
      Comment.create({body: 'foo'}, (err, newComment) => {
        if (err) return done(err);
        Comment.moveNode(newComment.id, commentId4, err => {
          if (err) return done(err);
          Comment.findById(newComment.id, (err, comment) => {
            if (err) return done(err);
            assert(comment.parent === commentId4, 'correct parent');
            Comment.findById(commentId4, (err, comment) => {
              if (err) return done(err);
              assert(comment.children.some(c => c === newComment.id), 'new parent has correct child');
              done();
            });
          });
        });
      });
    });

    it('can move to same parent without any effect', function(done) {
      Comment.moveNode(commentId3, commentId1, err => {
        if (err) return done(err);
        Comment.findById(commentId3, (err, comment) => {
          if (err) return done(err);
          assert(comment.parent === commentId1, 'correct parent');
          Comment.findById(commentId1, (err, comment) => {
            if (err) return done(err);
            assert(comment.children.filter(c => c === commentId3).length === 1, 'child only once');
            done();
          });
        });
      });
    });

    function buildTree(cb) {
      let commentIds = [];
      async.timesSeries(10, // do this in series so commentId indexes match body field
        (n, done) => Comment.create({body: '' + n}, (err, comment) => {
          commentIds.push(comment.id);
          done(err);
        }),
        err => {
          if (err) return cb(err);
          async.timesSeries(5, // has to be done in series, maybe becuase we are operating on each object in two of the requests
            (n, done) => { Comment.addChild(commentIds[n], commentIds[n + 1], null, done); },
            err => {
              if (err) return cb(err);
              async.timesSeries(4,
                (n, done) => Comment.addChild(commentIds[3], commentIds[n + 6], null, done),
                err => {
                  cb(err, commentIds);
                });
            });
        });
    }

    it('can not move node if it creates a complex circular graph', function(done) {
      buildTree((err, commentIds) => {
        if (err) return done(err);
        Comment.getTree(commentIds[3], false, (err, tree) => {
          if (err) return done(err);
          Comment.moveNode(commentIds[2], commentIds[9], err => {
            assert(err, 'has error');
            assert(err.statusCode === 422, 'correct error code');
            done();
          });
        });
      });
    });

    it('can move node', function(done) {
      // moving comment2 to be a child of comment3 instead of comment1
      Comment.moveNode(commentId2, commentId3, err => {
        if (err) return done(err);
        Comment.getTree(commentId3, true, (err, tree) => {
          if (err) return done(err);
          assert(tree.children[0].id === commentId2, 'correct first level child');
          assert(tree.children[0].children[0].id === commentId4, 'correct second level child');
          Comment.findById(commentId1, (err, comment) => {
            if (err) return done(err);
            assert(comment.children.length === 1, 'correct number children of old parent');
            assert(comment.children[0] === commentId3, 'old parent only has other child');
            done();
          });
        });
      });
    });

    it('can move to null parent', function(done) {
      Comment.makeNodeRoot(commentId3, err => {
        if (err) return done(err);
        Comment.findById(commentId3, (err, comment) => {
          if (err) return done(err);
          assert(comment.parent === null, 'no parent');
          Comment.findById(commentId1, (err, comment) => {
            if (err) return done(err);
            assert(!comment.children.some(c => c === commentId3), 'not child of old parent');
            done();
          });
        });
      });
    });
  });
});
