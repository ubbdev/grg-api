'use strict';

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;
const maildev = common.maildev;

const emailSettings = require('../settings.test.json').email;

describe('Email notifications', function() {
  const projectId = common.projects[0].id;

  before(done => {
    assert(emailSettings, 'email not configured, skipping tests');
    done();
  });

  beforeEach(maildev.deleteAllEmail);

  describe('Notify admin message', function() {
    it('should not be empty', function(done) {
      server.models.Project.notifyAdmins(projectId, {}, err => {
        assert(err, 'has error');
        done();
      });
    });

    it('should have subject', function(done) {
      server.models.Project.notifyAdmins(projectId, {text: 'foo'}, err => {
        assert(err, 'has error');
        done();
      });
    });

    it('should have body', function(done) {
      server.models.Project.notifyAdmins(projectId, {subject: 'foo'}, err => {
        assert(err, 'has error');
        done();
      });
    });

    it('should be sent to project admins', done => {
      const message = {text: 'foo', subject: 'bar'};
      maildev.on('new', email => {
        assert(email.text.match(message.text), 'has correct text'); // do match instead of === because nodemailer appends '\n'
        assert(email.subject === message.subject, 'has correct subject');
        assert(email.to.some(recip => recip.address === common.admin.email), 'has correct recipient');
        maildev.removeAllListeners();
        done();
      });

      maildev.on('error', err => {
        maildev.removeAllListeners();
        done(err);
      });

      server.models.Project.notifyAdmins(common.projects[0].id, message, err => {
        if (err) return done(err);
      });
    });
  });
});
