'use strict';

const settings = require('../getSettings');
const nodemailer = require('nodemailer');
const logger = require('loopback-component-logger')('Mail');

let sendMail = null;

if (settings.email) {
  let transporter = null;

  if (!settings.email.from) {
    throw new Error('Email must specify a from address');
  }

  if (settings.email.nodemailer) {
    transporter = nodemailer.createTransport(settings.email.nodemailer);
  } else {
    throw new Error('Can not specify email in settings without nodemailer settings');
  }

  sendMail = function(email, cb) {
    email.from = email.from || settings.email.from;
    transporter.sendMail(email, cb);
  };
} else {
  sendMail = function(email, cb) {
    email.from = email.from || 'no-reply@not.sent.com';
    logger.info('Did not send email: ' + JSON.stringify(email));
    cb();
  };
}

module.exports = function(app) {
  app.sendMail = sendMail;
};
