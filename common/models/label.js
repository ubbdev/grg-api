'use strict';

module.exports = function(Label) {
  Label.serialize = function(label) {
    return label.lang + '__' + label.name;
  };
};
