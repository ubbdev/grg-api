'use strict';

module.exports = function(Model, options) {
  Model.defineProperty('created', {type: 'date', defaultFn: 'now'});
  Model.defineProperty('lastUpdated', {type: 'date', defaultFn: 'now'});

  Model.observe('before save', (ctx, next) => {
    if (ctx.isNewInstance) return next();
    let instance = ctx.instance || ctx.data;
    instance.lastUpdated = Date.now();
    next();
  });
};
