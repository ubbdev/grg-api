'use strict';

const async = require('async');

/**
 * Mixin for Elasticsearch indexing
 *
 * Automatically updates an elasticsearch index using app.elasticClient whenever an instance
 * of the model is created, updated or deleted. If no Elasticsearch client is attached to the
 * app, it does nothing.
 *
 * The mixing uses two required and one optional members of the model it is applied to:
 *   - Model.instance2document(): an async member function that takes an instance of the Model and
 *     returns the document to be indexed. Allows one to exclude certain fields. (Required)
 *   - Model.instance2indexName(): a function that returns the index to use for a given
 *     instance. (Required)
 *   - Model.elasticsearchMapping: a member object that defines the Elasticsearch mapping to
 *     use. (Required)
 *   - Model.excludeFromElasticsearch(): a member function that takes an instance of the Model
 *     returns true if the instance should be excluded from the index, and false else. This
 *     allows one to exclude drafts for example. (Optional)
 */

module.exports = function(Model, options) {
  if (!Model.instance2document || typeof Model.instance2document !== 'function') {
    throw new Error("Elasticsearch mixin requires model to have a member function 'instance2document()'");
  }

  if (!Model.instance2indexName || typeof Model.instance2indexName !== 'function') {
    throw new Error("Elasticsearch mixin requires model to have a member function 'instance2indexName()'");
  }

  if (!Model.elasticsearchMapping) {
    throw new Error("Elasticsearch mixin requires model to have a member object 'elasticsearchMapping'");
  }

  const type = Model.definition.name;
  const mapToES = Model.instance2document;
  const mapping = Model.elasticsearchMapping;
  const exclude = Model.excludeFromElasticsearch || function(instance) { return false; };

  Model.getElasticSearchMapping = function() {
    return mapping;
  };

  function getIndex(instance) {
    return Model.instance2indexName(instance).toLowerCase();
  }

  function createESDocument(instance, cb) {
    const client = Model.app.elasticClient;

    if (!client) return cb();

    if (exclude(instance)) return cb();

    mapToES(instance, (err, doc) => {
      if (err) return cb(err);
      client.create({
        index: getIndex(instance),
        type: type,
        refresh: 'true',
        body: doc,
        id: instance.id,
      }, cb);
    });
  }

  // assumes the instance is already indexed
  function updateESDocument(instance, cb) {
    const client = Model.app.elasticClient;

    if (!client) return cb();

    const index = getIndex(instance);

    // if the instance changes to a state that should be excluded, we delete it
    if (exclude(instance)) {
      client.delete({
        index: index,
        type: type,
        id: instance.id,
        refresh: 'true',
      }, cb);
    } else {
      mapToES(instance, (err, doc) => {
        if (err) return cb(err);
        client.update({
          index: index,
          type: type,
          refresh: 'true',
          body: {
            doc: doc,
          },
          id: instance.id,
        }, cb);
      });
    }
  }

  // add or update instance in ES index
  function indexInstance(instance, cb) {
    const client = Model.app.elasticClient;

    if (!client) return cb();

    client.exists({
      index: getIndex(instance),
      type: type,
      id: instance.id,
    }, (err, exists) => {
      if (err) return cb(err);
      if (exists) {
        updateESDocument(instance, cb);
      } else {
        createESDocument(instance, cb);
      }
    });
  }

  Model.observe('after save', function(ctx, next) {
    const instance = ctx.instance || ctx.currentInstance;

    indexInstance(instance, next);
  });

  Model.observe('before delete', function(ctx, next) {
    const client = Model.app.elasticClient;
    if (!client) return next();

    Model.find(ctx.where, (err, instances) => {
      if (err) return next(err);

      let actions = instances
      .filter(instance => !exclude(instance))
      .map(instance => {
        return {
          'delete': {
            _index: getIndex(instance),
            _type: type,
            _id: instance.id,
          },
        };
      });

      // done after filter/map to handle case of only drafts
      if (actions.length === 0) return next();

      client.bulk({body: actions, refresh: 'true'}, next);
    });
  });

  // Assumes we have an empty index
  Model.populateElasticsearchIndex = function(instances, cb) {
    const client = Model.app.elasticClient;

    if (!client) return cb();

    if (!instances || instances.length === 0) return cb();

    let actions = [];
    async.each(
      instances.filter(instance => !exclude(instance)),
      (instance, done) => mapToES(instance, (err, doc) => {
        actions.push({create: {_index: getIndex(instance), _type: type, _id: instance.id}}, doc);
        done();
      }),
      err => {
        if (err) return cb(err);
        Model.app.elasticClient.bulk({body: actions, refresh: 'true'}, cb);
      }
    );
  };
};
