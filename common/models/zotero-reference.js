'use strict';

module.exports = function(Zoteroreference) {
  var State = null;
  const versionKey = 'ZoteroBibVersion';

  // makes a test request to the group, to check that authorization
  // works, the callback takes an error as argument
  Zoteroreference.verifyProjectOptions = function(opt, projectId, callback) {
    if (!opt.groupId) {
      return callback(`Project '${projectId}' has no Zotero-group`);
    }
    if (!opt.authorization) {
      return callback(`Project '${projectId}' has no authorization key`);
    }
    Zoteroreference.findByString(opt.groupId, '', 1, 1, opt.authorization, (err, reference) => {
      if (err) {
        callback({
          name: 'Config error',
          message: "Can't access Zotero API: '" + JSON.stringify(err) + "'",
        });
      }
      if (!reference) {
        return callback({
          name: 'Config error',
          message: 'Empty response from Zotero API',
        });
      }
    });
    callback(null);
  };

  // Get the version of the Zotero bibliography used in the last sync. Set to 0 if not available.
  // Returns promise
  Zoteroreference.getZoteroBibVersion = function(projectId, callback) {
    Zoteroreference.app.models.State.findOne({where: {projectId: projectId, key: versionKey}}, (err, versionObj) => {
      let version = versionObj && versionObj.value.version || 0;
      callback(err, version);
    });
  };

  // Store new version number after sync.
  // Returns promise
  Zoteroreference.setZoteroBibVersion = function(projectId, version, callback) {
    const State = Zoteroreference.app.models.State;
    State.findOne({where: {projectId: projectId, key: versionKey}}, (err, versionObj) => {
      if (err) return callback(err);

      if (!versionObj) return State.create({projectId: projectId, key: versionKey, value: {version: version}}, callback);

      versionObj.value = {version: version};

      State.upsert(versionObj, callback);
    });
  };
};
