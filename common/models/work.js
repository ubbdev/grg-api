'use strict';

const http = require('http');
const async = require('async');
const xml2js = require('xml2js');
const appRoot = require('app-root-path');
const fs = require('fs');

const stylesheetRoot = appRoot + '/common/stylesheets/';
const meiHistoryStyleSheet = stylesheetRoot + 'mei-history-to-html.xsl';
const meiBibliographyStyleSheet = stylesheetRoot + 'mei-bibliography-to-html.xsl';
const meiSourcesStyleSheet = stylesheetRoot + 'mei-sources-to-html.xsl';

module.exports = function(Work) {
  Work.disableRemoteMethodByName('create');
  Work.disableRemoteMethodByName('replaceById');
  Work.disableRemoteMethodByName('destroyById');
  Work.disableRemoteMethodByName('upsert');
  Work.disableRemoteMethodByName('upsertWithWhere');
  Work.disableRemoteMethodByName('updateAll');
  Work.disableRemoteMethodByName('prototype.updateAttributes');
  Work.disableRemoteMethodByName('prototype.replace');
  Work.disableRemoteMethodByName('replaceOrCreate');
  Work.disableRemoteMethodByName('createChangeStream');

  Work.disableRemoteMethodByName('prototype.__create__articles');
  Work.disableRemoteMethodByName('prototype.__upsert__articles');
  Work.disableRemoteMethodByName('prototype.__delete__articles');
  Work.disableRemoteMethodByName('prototype.__findById__articles');
  Work.disableRemoteMethodByName('prototype.__updateById__articles');
  Work.disableRemoteMethodByName('prototype.__destroyById__articles');
  Work.disableRemoteMethodByName('prototype.__link__articles');
  Work.disableRemoteMethodByName('prototype.__unlink__articles');

  function applyStyleSheet(documentString, stylesheetPath, cb) {
    fs.readFile(stylesheetPath, (err, stylesheetString) => {
      if (err) return cb(err);

      // Hack: Depening on how the app is run the stylesheet will need different import paths.
      // Can not be set as a stylesheet parameter because imports are done at compile-time.
      let modStylesheet = stylesheetString.toString().replace('"./mei-to-html.xsl"', `"${stylesheetRoot}mei-to-html.xsl"`);

      libxslt.parse(modStylesheet, (err, stylesheet) => {
        if (err) return cb(err);
        stylesheet.apply(documentString, cb);
      });
    });
  }

  function condenseMermeidDescription(documentString, cb) {
    let result = {};
    async.parallel([
      done => applyStyleSheet(documentString, meiHistoryStyleSheet, (err, history) => {
        if (err) return done(err);
        result.history = history;
        done();
      }),
      done => applyStyleSheet(documentString, meiBibliographyStyleSheet, (err, bibliography) => {
        if (err) return done(err);
        result.bibliography = bibliography;
        done();
      }),
      done => applyStyleSheet(documentString, meiSourcesStyleSheet, (err, sources) => {
        if (err) return done(err);
        result.sources = sources;
        done();
      }),
    ], err => cb(err, result));
  }

  function syncMermeidWork(work, project, cb) {
    http.get(work.uri, res => {
      const {statusCode} = res;
      const contentType = res.headers['content-type'];

      let error = null;
      if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
          `Status Code: ${statusCode}`);
      } else if (!/^application\/xml/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
          `Expected application/xml but received ${contentType}`);
      }

      if (error) {
        res.resume();
        error.statusCode = 500;
        return cb(error);
      }
      res.setEncoding('utf8');
      let rawData = '';
      res.on('data', chunk => { rawData += chunk; });
      res.on('end', (rawData, (err, document) => {
          if (err) return cb(err);
          work.document = document;
          work.type = 'MEI';
          xml2js.parseString(rawData, (err, result) => {
            if (err) return cb(err);
            let titleStmt = result.mei.meiHead[0].workDesc[0].work[0].titleStmt[0];
            let creation = result.mei.meiHead[0].workDesc[0].work[0].creation[0];
            work.altLabel = [];
            work.respStmt = [];
            work.creationDate = null;
            if (titleStmt) {
              titleStmt.title.forEach(title => {
                if (title.$.type && title.$.type === 'alternative')
                  work.altLabel.push({label: title._, lang: title.$['xml:lang']});
              });

              if (titleStmt.respStmt) {
                titleStmt.respStmt[0].persName.forEach(persName => {
                  work.respStmt.push({name: persName._, role: persName.$['role']});
                });
              }
            }

            if (creation && creation.date[0]._) {
              work.creationDate = creation.date[0]._;
            }
            Work.upsert(work, (err, obj) => {
              obj.project(project, cb);
            });
          });
        }));
      }).on('error', (err) => {
      cb(`Got error: ${err.message}`);
    });
  }

  function addMermeidWorks(project, document, cb) {
    let works = [];
    document.forEach(work => {
      works.push({
        uri: project.workService.options.url + '/storage/dcm/' + work.name,
        displayLink: project.workService.options.url + '/storage/present.xq?doc=' + work.name,
        label: work.title,
        id: work.name.replace(/\.xml/, ''), // truncate
        projectId: project.id,
      });
    });
    async.eachSeries(works, (work, done) => {
      syncMermeidWork(work, project, done);
    }, cb);
  }

  function getMermeidData(options, cb) {
    // this uses a custom xquery file for Mermeid (should eventually be pushed upstream)
    http.get(options.url + '/storage/list-files-json.xq?c=' + options.collection + '&published_only' + options.publishedOnly + '&page=1&itemsPerPage=500', (res) => {
      const {statusCode} = res;
      const contentType = res.headers['content-type'];

      let error = null;
      if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
          `Status Code: ${statusCode}`);
      } else if (!/^text\/javascript/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
          `Expected text/javascript but received ${contentType}`);
      }
      if (error) {
        res.resume();
        error.statusCode = 500;
        return cb(error);
      }

      res.setEncoding('utf8');
      let rawData = '';
      res.on('data', chunk => { rawData += chunk; });
      res.on('end', () => {
        try {
          let parsedData = JSON.parse(rawData);
          cb(null, parsedData);
        } catch (e) {
          cb(e);
        }
      });
    }).on('error', (err) => {
      cb(`Got error: ${err.message}`);
    });
  }

  function syncMermeid(project, cb) {
    const options = project.workService.options;
    getMermeidData(options, (err, data) => {
      if (err) return cb(err);
      if (!data.document) return cb({statusCode: 500, message: 'Got empty results from remote server'});
      addMermeidWorks(project, data.document, err => {
        cb(err, data.document.length);
      });
    });
  }

  Work.sync = function(projectId, cb) {
    Work.app.models.Project.findById(projectId, (err, project) => {
      if (err) return cb(err);
      if (!project) return cb({message: 'No project found with id ' + projectId, statusCode: 404});

      if (project.workService && project.workService.type === 'Mermeid') {
        return syncMermeid(project, cb);
      } else {
        return cb({message: 'No valid workService for project ' + projectId, statusCode: 422});
      }
    });
  };

  Work.remoteMethod('sync', {
    http: {verb: 'get'},
    accepts: [
      {arg: 'projectId', type: 'string'},
    ],
    returns: {arg: 'itemsFound', type: 'number'},
    description: 'Synchronize all works with external service (e.g. Mermeid)',
  });

  /**************************
   * Elasticsearch indexing *
   **************************/

  Work.instance2document = function(instance, cb) {
    cb(null, {
      label: instance.label,
      document: instance.document,
      creationDate: instance.creationDate,
    });
  };

  Work.instance2indexName = function(instance) {
    return instance.projectId;
  };

  Work.elasticsearchMapping = {
    properties: {
      label: {type: 'text'},
      document: {type: 'object'},
      creationDate: {type: 'text'},
    },
  };
};
