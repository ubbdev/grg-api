'use strict';

const async = require('async');

// This mixin handles logic that's common between PublicComment and EditorialComment that can't be handled in comment.js
module.exports = function(Model, options) {
  // make sure article relation is inherited
  Model.observe('before save', (ctx, next) => {
    if (!ctx.isNewInstance) return next();
    if (!ctx.instance.parent) return next();
    Model.findById(ctx.instance.parent, (err, parent) => {
      if (err) return next(err);
      ctx.instance.articleId = parent.articleId;
      next();
    });
  });

  Model.observe('access', (ctx, next) => {
    if (!ctx.query.include) ctx.query.include = 'author';
    next();
  });

  // make sure ownership is applied also in addChild
  Model.beforeRemote('addChild', (ctx, currentInstance, next) => {
    if (ctx.req.body) ctx.req.body.personId = ctx.req.accessToken.userId;
    next();
  });

  // notify Contributors when someone comments on an article
  Model.observe('after save', (ctx, next) => {
    if (!ctx.isNewInstance) return next();

    let comment = ctx.instance;
    let commentAuthor = null;
    let article = null;
    let articleContributors = null;
    let parentComment = null;
    let parentCommentAuthor = null;
    let project = null;
    let footer = '';

    async.series([
      done => comment.article((err, obj) => {
        if (!obj) return done('comment has no article');
        article = obj;
        done(err);
      }),
      done => article.contributors((err, obj) => {
        if (!obj) return done('article has no contributors');
        articleContributors = obj;
        return done(err);
      }),
      done => article.project((err, obj) => {
        if (!obj) return done('article has no project');
        project = obj;
        return done(err);
      }),
      done => Model.app.models.Person.findById(comment.personId, (err, person) => {
        commentAuthor = person;
        return done(err);
      }),
      done => {
        if (!comment.parent) return done();

        Model.findById(comment.parent, (err, parent) => {
          if (!parent) return done('illegal parent comment');
          parentComment = parent;
          return done(err);
        });
      },
      done => {
        if (!comment.parent) return done();

        parentComment.author((err, person) => {
          if (!person) return done('parent comment has no author');
          parentCommentAuthor = person;
          done(err);
        });
      },
      done => {
        let text = `Comment by ${commentAuthor.name}:\n\n${comment.body}\n`;

        let recipients = articleContributors
        // We don't want to get emails for our own comments
        .filter(contributor => contributor.id !== commentAuthor.id)
        // If this is a reply to a comment, don't send two notifications
        .filter(contributor => {
          if (parentComment) return contributor.id !== parentCommentAuthor.id;
          return true;
        })
        .map(contributor => contributor.email);

        if (recipients.length === 0) return done();

        // here we have unwanted coupling to the frontend. TODO: find a better strategy
        footer = `\nReply and view the discussion at ${project.frontEndHome}/articles/${article.id}\n`;

        if (parentComment) {
          text += `\nIn reply to a comment by ${parentCommentAuthor.name}:\n\n${parentComment.body}\n`;
        }

        Model.app.sendMail({
          to: recipients,
          subject: `[${project.id}] ${commentAuthor.name} commented on "${article.title}"`,
          text: text + footer,
        }, done);
      },
      done => {
        if (!parentComment) return done();

        if (parentCommentAuthor.id === commentAuthor.id) return done();

        Model.app.sendMail({
          to: parentCommentAuthor.email,
          subject: `[${project.id}] ${commentAuthor.name} replied to your comment on "${article.title}"`,
          text: `Comment:\n\n${comment.body}\n` + footer,
        }, done);
      }], next);
  });
};
