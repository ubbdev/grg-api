'use strict';

const devmode = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const prodmode = process.env.NODE_ENV === 'production';
const testmode = process.env.NODE_ENV === 'test';

let settings = null;

if (testmode) {
  settings = require('../settings.test.json');
} else {
  settings = require('../settings.json');
}

module.exports = settings;
