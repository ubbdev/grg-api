'use strict';

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

const async = require('async');

const projectId = common.projects[0].id;
const projectId2 = common.projects[1].id;

// some helper functions
function addRole(rolePlural, token, userInRoleId, userToAddId, totalInRoleAfterAdded, totalMembersAfterAdded, done) {
  agent
  .post('/api/projects/' + projectId + '/' + rolePlural)
  .send({personId: userToAddId})
  .set('Authorization', token)
  .expect(200)
  .then(() => {
    return agent
    .get('/api/projects/' + projectId + '/' + rolePlural)
    .expect(200);
  })
  .then(res => {
    let people = res.body;
    assert(people.length === totalInRoleAfterAdded, 'correct number of ' + rolePlural + ' after adding');
    assert(people.some(person => person.id === userInRoleId), 'Old user still in role');
    assert(people.some(person => person.id === userToAddId), 'New user has been added');
  })
  .then(() => {
    return agent
    .get('/api/projects/' + projectId + '/people')
    .expect(200);
  })
  .then(res => {
    assert(res.body.length === totalMembersAfterAdded, 'Right number members');
    assert(res.body.some(person => person.id === userToAddId));
  })
  .then(() => done(), done);
}

function removeRole(rolePlural, token, userToKeepId, userToRemoveId, totalInRoleAfterRemoval, done) {
  agent
  .delete('/api/projects/' + projectId + '/' + rolePlural)
  .send({personId: userToRemoveId})
  .set('Authorization', token)
  .then(() => {
    return agent
    .get('/api/projects/' + projectId + '/' + rolePlural)
    .expect(200);
  })
  .then(res => {
    let people = res.body;
    assert(people.length === totalInRoleAfterRemoval, 'correct number of ' + rolePlural + ' after removing again');
    assert(people.some(person => person.id === userToKeepId), 'Old user still in role');
  })
  .then(() => done(), done);
}

function addRoleFail(rolePlural, token, userToAddId, done) {
  agent
  .post('/api/projects/' + projectId + '/' + rolePlural)
  .send({personId: userToAddId})
  .set('Authorization', token)
  .expect(401)
  .end(done);
}

function removeRoleFail(rolePlural, token, userToRemoveId, done) {
  return agent
  .delete('/api/projects/' + projectId + '/' + rolePlural)
  .send({personId: userToRemoveId})
  .set('Authorization', token)
  .expect(401)
  .end(done);
}

function rolesInInitialState(done) {
  async.each(['admins', 'editors', 'contributors'],
    (role, cb) => {
      agent.get('/api/projects/' + projectId + '/' + role)
      .expect(200)
      .end((err, res) => {
        if (err) return cb(err);
        if (role === 'contributors') assert(res.body.length === 3, '3 contributors');
        else assert(res.body.length === 1, 'Only one editor and admin to begin with');
        cb();
      });
    }, done);
}

describe('Project', function() {
  let Project = null;
  let Reference = null;
  let Article = null;
  let ProjectRoleMapping = null;

  let adminToken = null;
  let editorToken = null;
  let contributorToken = null;
  let otherProjectContributorToken = null;

  // for convenience
  before(done => {
    Project = server.models.Project;
    Reference = server.models.Reference;
    Article = server.models.Article;
    ProjectRoleMapping = server.models.ProjectRoleMapping;
    adminToken = common.admin.token;
    editorToken = common.editor.token;
    contributorToken = common.contributor.token;
    otherProjectContributorToken = common.otherProjectContributor.token;
    done();
  });

  describe('Members', function() {
    before(done => rolesInInitialState(done));
    after(done => rolesInInitialState(done));

    it('Should get all members from projects/:id/people', function() {
      return agent
      .get('/api/projects/' + projectId + '/people')
      .expect(200)
      .then(res => {
        const members = res.body;
        assert(members.length === 5, 'Correct number of members');
        assert(members.some(m => { return m.id === common.admin.id; }), 'Admin is member');
        assert(members.some(m => { return m.id === common.editor.id; }), 'Editor is member');
        assert(members.some(m => { return m.id === common.contributor.id; }), 'Contributor is member');
        assert(members.some(m => { return m.id === common.otherContributor.id; }), 'Second contributor is member');
        assert(members.some(m => { return m.id === common.thirdContributor.id; }), 'Third contributor is member');
      });
    });

    it('Should get all members from projects/:id/people for second project', function() {
      return agent
      .get('/api/projects/' + projectId2 + '/people')
      .expect(200)
      .then(res => {
        const members = res.body;
        assert(members.length === 2, 'Correct number of members');
        assert(members.some(m => { return m.id === common.otherProjectContributor.id; }), 'Third contributor is member');
      });
    });

    describe('Contributor', function() {
      it('should not be able to add members', () => {
        return agent
        .put('/api/projects/' + projectId + '/people/rel/' + common.newUser.id)
        .set('Authorization', contributorToken)
        .expect(401);
      });

      it('should not be able to remove members', () => {
        return agent
        .delete('/api/projects/' + projectId + '/people/rel/' + common.newUser.id)
        .set('Authorization', contributorToken)
        .expect(401);
      });
    });

    describe('Editor', function() {
      it('should be able to add members', () => {
        return agent
        .put('/api/projects/' + projectId + '/people/rel/' + common.newUser.id)
        .set('Authorization', editorToken)
        .expect(200)
        .then(res => {
          return agent
          .get('/api/projects/' + projectId + '/people')
          .expect(200)
          .then(res => {
            const members = res.body;
            assert(members.some(m => { return m.id === common.newUser.id; }), 'newUser is member');
          });
        });
      });

      it('should be able to remove members', () => {
        return agent
        .delete('/api/projects/' + projectId + '/people/rel/' + common.newUser.id)
        .set('Authorization', editorToken)
        .expect(204)
        .then(res => {
          return agent
          .get('/api/projects/' + projectId + '/people')
          .expect(200)
          .then(res => {
            const members = res.body;
            assert(!members.some(m => { return m.id === common.newUser.id; }), 'newUser is no longer member');
          });
        });
      });
    });
  });

  describe('Roles', function() {
    it('should get 404 when modifying roles of non-existent project', done => {
      Project.addContributor('foo', common.contributor.id, err => {
        assert(err.statusCode === 404, 'correct error');
        done();
      });
    });

    it('should get 404 when modifying roles of non-existent person', done => {
      Project.addContributor(projectId, 'foo', err => {
        assert(err.statusCode === 404, 'correct error');
        done();
      });
    });

    describe('Admin', function() {
      before(done => rolesInInitialState(done));
      after(done => rolesInInitialState(done));

      it('Should be able to add contributor role', done => {
        addRole('contributors', adminToken, common.contributor.id, common.newUser.id, 4, 6, done);
      });

      it('Should be able to remove contributor role', done => {
        removeRole('contributors', adminToken, common.contributor.id, common.newUser.id, 3, done);
      });

      it('Should be able to add editor role', done => {
        addRole('editors', adminToken, common.editor.id, common.contributor.id, 2, 6, done);
      });

      it('Should be able to remove editor role', done => {
        removeRole('editors', adminToken, common.editor.id, common.contributor.id, 1, done);
      });

      it('Should be able to add admin role', done => {
        addRole('admins', adminToken, common.admin.id, common.editor.id, 2, 6, done);
      });

      it('Should be able to remove admin role', done => {
        removeRole('admins', adminToken, common.admin.id, common.editor.id, 1, done);
      });

      it('Should get 404 for faulty person id', function() {
        return agent
        .post('/api/project/' + projectId + '/admins')
        .send({personId: '123'})
        .set('Authorization', adminToken)
        .expect(404);
      });
    });

    describe('Editor', function() {
      before(done => rolesInInitialState(done));
      after(done => rolesInInitialState(done));

      it('should not be able to remove members with roles', () => {
        return agent
        .delete('/api/projects/' + projectId + '/people/rel/' + common.contributor.id)
        .set('Authorization', editorToken)
        .expect(409);
      });

      it('Should be able to add contributor role', done => {
        addRole('contributors', editorToken, common.contributor.id, common.newUser.id, 4, 6, done);
      });

      it('Should be able to remove contributor role', done => {
        removeRole('contributors', editorToken, common.contributor.id, common.newUser.id, 3, done);
      });

      it('Should be able to add editor role', done => {
        addRole('editors', editorToken, common.editor.id, common.contributor.id, 2, 6, done);
      });

      it('Should be able to remove editor role', done => {
        removeRole('editors', editorToken, common.editor.id, common.contributor.id, 1, done);
      });

      it('Should not be able to add admin role', done => {
        addRoleFail('admins', editorToken, common.contributor.id, done);
      });

      it('Should not be able to remove admin role', done => {
        removeRoleFail('admins', editorToken, common.contributor.id, done);
      });
    });

    describe('Contributor', function() {
      before(done => rolesInInitialState(done));
      after(done => rolesInInitialState(done));

      it('Should not be able to add contributor role', done => {
        addRoleFail('contributors', contributorToken, common.newUser.id, done);
      });

      it('Should not be able to remove editor role', done => {
        removeRoleFail('contributors', contributorToken, common.newUser.id, done);
      });

      it('Should not be able to add editor role', done => {
        addRoleFail('editors', contributorToken, common.newUser.id, done);
      });

      it('Should not be able to remove editor role', done => {
        removeRoleFail('editors', contributorToken, common.newUser.id, done);
      });

      it('Should not be able to add admin role', done => {
        addRoleFail('admins', contributorToken, common.newUser.id, done);
      });

      it('Should not be able to remove admin role', done => {
        removeRoleFail('admins', contributorToken, common.newUser.id, done);
      });
    });
  });

  describe('Articles', function() {
    let sampleArticle = null;
    let publishedId = null;
    let draftId = null;
    let ref1Id = null;

    before(done => {
      Reference.create({title: 'Foo', creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId},
        (err, ref) => {
          ref1Id = ref.id;
          sampleArticle = {
            title: 'Foo',
            body: 'bar',
            referenceId: [ref1Id],
            contributorId: [common.contributor.id, common.otherContributor.id],
            status: 'published',
          };
          done(err);
        }
        );
    });

    before(done => common.createSampleArticle(common.contributor, sampleArticle, projectId, (err, id) => {
      if (err) return done(err);
      publishedId = id;
      done();
    }));

    before(done => {
      sampleArticle.status = 'draft';
      common.createSampleArticle(common.contributor, sampleArticle, projectId, (err, id) => {
        if (err) return done(err);
        draftId = id;
        sampleArticle.status = 'published';
        done();
      });
    });

    after(done => { Article.destroyAll(done); });
    after(done => { Reference.destroyAll(done); });

    function verifyDraftFiltered(article) {
      if (article.status === 'draft') {
        assert(article.title === undefined, 'no title');
        assert(article.body === undefined, 'no body');
      } else {
        assert(article.title === sampleArticle.title, 'correct title');
      }
    }

    describe('anonymous users', function() {
      it('can not create articles', function() {
        return agent
        .post('/api/projects/' + projectId + '/articles')
        .send(sampleArticle)
        .expect(401);
      });

      it('can read published articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles/' + publishedId)
        .expect(200)
        .expect(res => {
          assert(res.body.title === sampleArticle.title, 'correct title');
          assert(res.body.status === 'published', 'correct status');
        });
      });

      it('can not inspect drafts directly', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles/' + draftId)
        .expect(401);
      });

      it('only get limited information about drafts at unfiltered GET api/projects/:id/articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'correct number');
          res.body.forEach(verifyDraftFiltered);
        });
      });

      it('only get limited information about drafts at filtered GET api/projects/:id/articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles?filter=' + JSON.stringify({where: {title: sampleArticle.title}}))
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'correct number');
          res.body.forEach(verifyDraftFiltered);
        });
      });
    });

    describe('projectContributors', function() {
      // already verified multiple times throught common.createSampleArticle that contributors can create articles, therefore explicit test skipped

      it('can read published articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles/' + publishedId)
        .set('Authorization', contributorToken)
        .expect(200)
        .expect(res => {
          assert(res.body.title === sampleArticle.title, 'correct title');
          assert(res.body.status === 'published', 'correct status');
        });
      });

      it('can read drafts', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles/' + draftId)
        .set('Authorization', contributorToken)
        .expect(200)
        .expect(res => {
          assert(res.body.title === sampleArticle.title, 'correct title');
          assert(res.body.status === 'draft', 'correct status');
        });
      });

      it('get both published and draft articles at unfiltered GET api/projects/:id/articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles')
        .set('Authorization', contributorToken)
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'correct number');
          assert(res.body.some(ass => { return ass.status === 'published'; }), 'published included');
          assert(res.body.some(ass => { return ass.status === 'draft'; }), 'draft included');
        });
      });

      it('get total count of articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles/count')
        .set('Authorization', contributorToken)
        .expect({count: 2});
      });
    });

    describe('Non-member contributors', function() {
      it('can read published articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles/' + publishedId)
        .set('Authorization', otherProjectContributorToken)
        .expect(200)
        .expect(res => {
          assert(res.body.title === sampleArticle.title, 'correct title');
          assert(res.body.status === 'published', 'correct status');
        });
      });

      it('can not read drafts', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles/' + draftId)
        .set('Authorization', otherProjectContributorToken)
        .expect(401);
      });

      it('only get limited information about drafts at unfiltered GET api/projects/:id/articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles')
        .set('Authorization', otherProjectContributorToken)
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'correct number');
          res.body.forEach(verifyDraftFiltered);
        });
      });

      it('only get limitied information about drafts at filtered GET api/projects/:id/articles', function() {
        return agent
        .get('/api/projects/' + projectId + '/articles?filter=' + JSON.stringify({where: {title: sampleArticle.title}}))
        .set('Authorization', otherProjectContributorToken)
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'correct number');
          res.body.forEach(verifyDraftFiltered);
        });
      });
    });
  });

  describe('Bibliography', function() {
    it('Has to have bibliographyService', function(done) {
      Project.create({
        'id': 'foo',
        'vocabularies': [],
        'description': 'bar',
      }, (err) => {
        assert(err, 'should be error');
        assert(err.match(/has no bibliographyService/), 'should be right error');
        done();
      });
    });

    it('BibliographyService has to have type', function(done) {
      Project.create({
        'id': 'foo',
        'vocabularies': [],
        'description': 'bar',
        'bibliographyService': {'options': {}},
      }, (err) => {
        assert(err, 'should be error');
        assert(err.match(/has no type/), 'should be right error');
        done();
      });
    });

    it('BibliographyService can\'t be empty', function(done) {
      Project.create({
        'id': 'foo',
        'vocabularies': [],
        'description': 'bar',
        'bibliographyService': {'type': 'bla'},
      }, (err) => {
        assert(err, 'should be error');
        assert(err.match(/has no options/), 'should be right error');
        done();
      });
    });

    it('Zotero BibliographyService needs authorization key', function(done) {
      Project.create({
        'id': 'foo',
        'vocabularies': [],
        'description': 'bar',
        'bibliographyService': {
          'type': 'Zotero',
          'options': {'groupId': '123'},
        },
      }, (err) => {
        assert(err, 'should be error');
        assert(err.match(/has no authorization key/), 'should be right error');
        done();
      });
    });

    it('Zotero BibliographyService needs group id', function(done) {
      Project.create({
        'id': 'foo',
        'vocabularies': [],
        'description': 'bar',
        'bibliographyService': {
          'type': 'Zotero',
          'options': {'authorization': '123'},
        },
      },
      err => {
        assert(err, 'should be error');
        assert(err.match(/has no Zotero-group/), 'Should throw error');
        done();
      });
    });
  });

  describe('Elasticsearch index', function() {
    it('can not be reindexed by anonymous', function() {
      return agent
      .put('/api/projects/' + projectId + '/reindex')
      .expect(401);
    });

    it('can not be reindexed by contributor', function() {
      return agent
      .put('/api/projects/' + projectId + '/reindex')
      .set('Authorization', contributorToken)
      .expect(401);
    });

    it('can not be reindexed by editor', function() {
      return agent
      .put('/api/projects/' + projectId + '/reindex')
      .set('Authorization', editorToken)
      .expect(401);
    });

    it('can be reindexed by admin', function() {
      return agent
      .put('/api/projects/' + projectId + '/reindex')
      .set('Authorization', adminToken)
      .expect(204);
    });
  });
});
