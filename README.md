# GRG-API

REST API for an annotated bibliography based on Loopback/NodeJS.

The application imports a bibliography from Zotero and allows users to write assessments to the references.
Users can login through external providers using Passport and can be granted contributor/editor/admin privileges.
Several bibliographies can be handled through the same instance, as separate projects.

The application was conceived for the project Grieg Research Guide and therefore also has a model to import musical
Work authorities from [Mermeid](http://www.kb.dk/en/nb/dcm/projekter/mermeid.html). This can be expanded in the future to allow import of other types of Works.

Assessments can have both public and editorial comments.
They can be saved as drafts, only visible to editors and the assessment's authors.
They can be edited, and edit history will be saved. Once an edit is opened the assessment is blocked until saved.

The application can be used headless or with the standard Angular frontend, generated using the [LoopBack SDK Builder](https://github.com/mean-expert-official/loopback-sdk-builder). See the [frontend repository](https://gitlab.com/ubbdev/grg-frontend.git).

See the file `doc/data-model.svg` for a rough sketch of the data model used.

## Dependencies

The following needs to be installed for the application to run

* NodeJS 6 (has not been tested with newer versions)
* MongoDB
* ElasticSearch 5 (can run without it, but the standard frontend uses it)

Other dependencies are documented in `package.json`.

## Build

```bash
npm install
```

## Configuration

In the app root directory, copy the file `settings.template.json` to `settings.json` and replace relevant values.
This file is used to initialize the projects model.

For a hypothetical project called `FOO`, we could have the following settings

```json
{
  "projects": [
    {
      "id": "FOO",
      "description": "Project FOO",
      "frontEndHome": "http://foo.com/dashboard",
      "bibliographyService": {
        "type": "Zotero",
        "options": {
          "groupId": "123456789",
          "authorization": "Bearer abcdefghijklmnop"
        }
      },
      "workService": {
        "type": "Mermeid",
        "options": {
          "url": "http://mermeidinstance.com",
          "collection": "collectionName"
        }
      },
      "providers": {
        ...
      },
    },
  ],
  "cookieSecret": "<something secret>",
  "adminUsers": ["userName1 from passport provider", "userName2"],
  "errorLog": "./log/error.log",
  "infoLog": "./log/info.log",
  "elasticSearch": {
    ...
  },
  "email": {
    ...
  },
}
```

The project-id cannot be changed once a project is in production since these are stored as foreign keys in other models.
The first time a project is encountered it is stored in the database,
so if the project gets a new id the old one will still exist in the DB.
If there are several projects these are simply appended to the `projects` array.

The `adminUsers` is a list of usernames that are predefined as admin users.
The first time someone logs in through the passport provider,
their username is compared to this list and the user is made admin for all current projects if there's a match.
This is for bootstrapping the app, if someone has already been logged in they will have to be upgraded to admin through the REST API/frontend.

The only supported bibliography-service at the moment is Zotero.
The server verifies upon startup that the provided settings are valid, by making a test request to the Zotero API.

Work-services are not obligatory and the only supported work-service at the moment is Mermeid.
The application makes some assumptions about how it is set up (see the code).
This is the least general part of the code and it might need some adjustments for your project.


### Authentication using Passport

Credentials for external user authentication should be supplied with each project.
Several projects can share the same provider, but should have unique callback paths.
Each project should have a `frontEndHome` specified, this is where the user will be redirected upon successful login.
If the frontend is not specified, the user will be sent to a plain-text page with the generated access-token, which can be used to test the API.

Automated tests use the local passport strategy.
Production is intended to use the provider [Dataporten](https://www.uninett.no/en/service-platform-dataporten).
If other providers than Dataporten are to be used, the corresponding passport-packages need to be installed,
and possibly the boot script `passport.js` needs to be adjusted.

For our project `FOO` the providers section of the project settings above could be

```json
{
  ...
  "providers": {
    "dataporten": {
      "provider": "dataporten",
      "module": "passport-uninett-dataporten",
      "clientID": "123abc",
      "clientSecret": "456def",
      "callbackURL": "http://foo.com/auth/FOO/dataporten/callback"
    }
  }
}
```

The callbackURL must have the pattern specified above, i.e. 'auth/{projectID}/{providerName}/callback'.
This will be checked during server startup.
Other parameters needed by passport are generated by the passport boot script.

### Database

For production mode, a database must be connected by creating the file `server/datasources.production.json`.
To use MongoDB add something like the following:

```json
{
  "db": {
    "name": "db",
    "connector": "mongodb",
    "host": "localhost",
    "port": 27017,
    "database": "...",
    "password": "...",
    "user": "..."
  }
}
```

The application has only been tested with MongoDB and probably won't work with other databases without adjustment.
Some of the unit tests fail when the LB memory model is used.

### Elasticsearch

The application can run without Elasticsearch, but the standard fronted requires it.
It has only been tested with Elasticsearch 5.6.
The application provides a search interface, so Elasticsearch doesn't need to be exposed outside localhost.

The following section needs to be added to `settings.json` for Elasticsearch to be used.
Possible parameters are those accepted by [elasticsearch.js](https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/configuration.html).

```json
{
  ...
  "elasticsearch": {
    "host": "http://localhost:9200/",
    "httpAuth": "admin:changeme",
    "apiVersion": "5.6"
  }
}
```

### Email

The application can send email notifications to admins when new users register (implemented),
and notify contributors (implementation pending) and editors of activity on an assessment (implementation pending).

To activate email add a section like to the following to the settings file.

```json
{
  ...
  "email": {
    "from": "no-reply@your.url",
    "nodemailer": {
      "port": 1025,
      "ignoreTLS": true
    }
  }
}
```

The field `from` is obligatory and defines the from-address in emails sent.
Emails are sent using nodemailer, and the options provided here are passed directly to nodemailer.

If there is no email section in the settings file, the emails that would have been sent are printed to the log instead.


## Start server

In development mode
```bash
npm start
```

In production
```bash
NODE_ENV=production npm start
```


## Run integration tests

The tests require MongoDB. Database credentials are set in `datasources.test.json`,
by default the default Mongo test database is used.

To run the tests, type

```bash
npm test
```

There are tests for Elasticsearch indexing and mail sending (using MailDev).
For all tests to pass Elasticsearch needs to be available and both elasticearch and email configured in `settings.test.json`.
The email settings in the standard test-settings are those used by MailDev, so changing these will most likely break the tests.
If you have MailDev running in the background there might be conflicts with the tests.

The tests focus on the logic of the app, and verifying that ACLs are configured correctly.
Functionality that is considered as part of standard LoopBack is generally not tested.
Some library errors are difficult to mock - so not all execution branches have been tested -
but the intention is to test all regular execution paths and all error conditions that can be caused by client errors.
Running the tests creates a folder `coverage` that contains test coverage analysis.


## Build Angular SDK for grg-frontend

Clone https://gitlab.com/ubbdev/grg-frontend.git into a separate directory, then

```bash
FRONTENDROOT=<directory you have grg-frontend in> npm run build:sdk
```

See the frontend README for information about how to get that running.


## API Documentation

Once the server is started, it serves Swagger documentation to the path `/explorer/`.
The automated tests and the frontend also document how the API is intended to be used.

To try out the parts of the API that require authentication without having a frontend to login through,
visit the path `/auth/{projectID}/{providerName}`, i.e. `/auth/FOO/dataporten` in the example above, to log in.
If no frontend is specified, the generated access-token will be visible in the browser, once you have been authenticated.
To gain admin privileges, add your name to the `adminUsers` list before you log in for the first time.
