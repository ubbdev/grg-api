'use strict';

const async = require('async');
const _ = require('lodash');

// Classes that use this mixin can define a method Model.compareNodes(node1, node2), which will be used to sort children in getChildren()

module.exports = function(Model, options) {
  Model.defineProperty('parent', {type: 'string'});
  Model.defineProperty('children', {type: ['string']});

  // add empty tree relations if nothing provided
  Model.observe('before save', (ctx, next) => {
    if (!ctx.isNewInstance) return next();
    if (!ctx.instance.parent) ctx.instance.parent = null;
    if (!ctx.instance.children) ctx.instance.children = [];
    return next();
  });

  // Checks that no children will be orphaned, and if not removes link from potential parent.
  Model.observe('before delete', (ctx, next) => {
    Model.find({where: ctx.where}, (err, nodes) => {
      if (err) return next(err);
      async.each(nodes, (node, done) => {
        if (node.children && node.children.length) return done({statusCode: 409, message: 'Node ' + node.id + ' has children, can not be deleted'});
        if (!node.parent) return done();
        Model.findById(node.parent, (err, parent) => {
          if (err) return done(err);
          parent.children = parent.children.filter(child => { return child !== node.id; });
          parent.save(done);
        });
      }, next);
    });
  });

  Model.addChild = function(parentId, childId, child, cb) {
    if (childId && child) return cb({statusCode: 422, message: 'Supply either childId or child object, not both'});
    if (!childId && !child) return cb({statusCode: 422, message: 'Supply either childId or child object'});
    Model.findById(parentId, (err, parent) => {
      if (err) return cb(err);
      if (!parent) return cb({statusCode: 404, message: 'Could not find parent ' + parentId});
      if (childId) {
        Model.findById(childId, (err, childObj) => {
          if (err) return cb(err);
          if (!childObj) return cb({statusCode: 404, message: 'Node with id ' + childId + ' not found'});
          if (childObj.parent) return cb({statusCode: 409, message: 'Node ' + childId + ' already has parent ' + childObj.parent});
          parent.children.push(childId);
          childObj.parent = parentId;
          parent.save((err, obj) => {
            if (err) return cb(err);
            childObj.save(cb);
          });
        });
      } else {
        child.parent = parentId;
        Model.create(child, (err, childObj) => {
          if (err) return cb(err);
          parent.children.push(childObj.id);
          parent.save((err, obj) => {
            if (err) return cb(err);
            cb(null, childObj);
          });
        });
      }
    });
  };

  Model.remoteMethod('addChild', {
    http: {verb: 'post', path: '/:id/children'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'childId', type: 'string'},
      {arg: 'child', type: 'object', http: {source: 'body'}},
    ],
    returns: {arg: 'child', type: 'object', root: true},
    description: 'Add child to node, supply either childId or child object, not both',
  });

  Model.getTree = function(parentId, recursive, cb) {
    if (arguments.length === 2 && typeof arguments[1] === 'function') {
      cb = arguments[1];
      recursive = false;
    }
    Model.findById(parentId, (err, parent) => {
      if (err) return cb(err);
      if (!parent) return cb({statusCode: 404, message: 'Parent node \'' + parentId + '\' not found'});
      if (parent.children.length === 0) return cb(null, parent);
      let parentClone = _.cloneDeepWith(parent, (value, key) => {
        if (key === 'children') return []; // we want to replace this
        if (value instanceof Array) { // datasource-juggler has class List that inherits from Array, this confuses lodash
          return value.toObject();
        }
      });
      async.each(parent.children,
        (childId, done) => Model.findById(childId, (err, child) => {
          if (err) return done(err);
          if (!child) return cb({statusCode: 404, message: 'Node \'' + parentId + '\' has invalid child \'' + childId + '\''});
          if (!recursive) {
            parentClone.children.push(child);
            return done();
          }
          Model.getTree(child.id, true, (err, childTree) => {
            if (err) return done(err);
            parentClone.children.push(childTree);
            return done();
          });
        }), err => {
          if (err) return cb(err);
          if (Model.compareNodes) parentClone.children.sort(Model.compareNodes);
          cb(null, parentClone);
        }
      );
    });
  };

  Model.remoteMethod('getTree', {
    http: {verb: 'get', path: '/:id/tree'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'recursive', type: 'boolean', default: false},
    ],
    returns: {arg: 'tree', type: 'object', root: true},
    description: 'Get tree where specified node is root. If recursive=false, the result will only contain the first level of the tree.',
  });

  Model.destroyBranch = function(rootNodeId, cb) {
    let numberProcessed = 0;
    let errors = [];

    function complete(err) {
      --numberProcessed;
      if (err) errors.push(err);
      if (numberProcessed === 0) {
        if (errors.length > 0) return cb(errors);
        return cb();
      }
    }

    function destroyBranchInnner(tree, cb) {
      ++numberProcessed;
      if (!tree.children) return cb('Node ' + nodeId + ' was not been set up correctly');
      if (tree.children.length === 0) return Model.destroyById(tree.id, cb);
      async.each(tree.children, (child, done) => {
        destroyBranchInnner(child, err => {
          complete(err);
          done();
        });
      }, err => {
        tree.children = [];
        Model.upsert(tree, err => {
          if (err) return cb(err);
          Model.destroyById(tree.id, err => cb(err));
        });
      });
    }

    Model.getTree(rootNodeId, true, (err, tree) => {
      if (err) return cb(err);
      destroyBranchInnner(tree, complete);
    });
  };

  Model.remoteMethod('destroyBranch', {
    http: {verb: 'delete', path: '/:id/tree'},
    accepts: [{arg: 'id', type: 'string', required: true}],
    description: 'Delete node and its children, recursively',
  });

  // check if a tree contains a given child
  function treeHasChild(tree, childId) {
    if (tree.id === childId) return true;
    return tree.children.some(child => treeHasChild(child, childId));
  }

  function addParent(node, newParent, cb) {
    node.parent = newParent.id;
    newParent.children.push(node.id);
    async.parallel([
      done => { node.save(done); },
      done => { newParent.save(done); },
    ], cb);
  }

  function removeFromOldParent(nodeId, oldParentId, cb) {
    return Model.findById(oldParentId, (err, oldParent) => {
      if (err) return cb(err);
      oldParent.children = oldParent.children.filter(child => child !== nodeId);
      oldParent.save(cb);
    });
  }

  // assumes nodeId is ok to move
  function moveCheckedNode(nodeId, targetNode, cb) {
    Model.findById(nodeId, (err, node) => {
      if (err) return cb(err);
      let oldParentId = node.parent;

      addParent(node, targetNode, err => {
        if (err) return cb(err);
        if (oldParentId === null) return cb();

        removeFromOldParent(nodeId, oldParentId, cb);
      });
    });
  }

  Model.moveNode = function(nodeId, targetNodeId, cb) {
    if (nodeId === null) return cb({statusCode: 422, message: 'Must provide node id'});
    let node = null;
    let oldParent = null;

    if (targetNodeId === null) {
      Model.findById(nodeId, (err, node) => {
        if (err) return cb(err);
        return removeFromOldParent(nodeId, node.parent, err => {
          if (err) return cb(err);
          node.parent = null;
          node.save(cb);
        });
      });
    } else {
      Model.getTree(nodeId, true, (err, tree) => {
        if (err) return cb(err);

        if (tree.parent === targetNodeId) return cb(err);

        Model.findById(targetNodeId, (err, targetNode) => {
          if (err) return cb(err);
          if (!targetNode) return cb({statusCode: 404, message: 'Can not find node ' + targetNodeId});

          if (!treeHasChild(tree, targetNodeId)) {
            return moveCheckedNode(nodeId, targetNode, cb);
          }
          return cb({statusCode: 422, message: 'Circular graph not allowed'});
        });
      });
    }
  };

  Model.makeNodeRoot = function(nodeId, cb) {
    return Model.moveNode(nodeId, null, cb);
  };

  Model.remoteMethod('moveNode', {
    http: {verb: 'put', path: '/:id/parent/'},
    accepts: [{arg: 'id', type: 'string', required: true},
              {arg: 'newParentId', type: 'string', required: true}],
    description: 'Change parent node',
  });

  Model.remoteMethod('makeNodeRoot', {
    http: {verb: 'delete', path: '/:id/parent'},
    accepts: [{arg: 'id', type: 'string', required: true}],
    description: 'Make this node a root node',
  });
};
