'use strict';

const settings = require('../../server/getSettings');

const async = require('async');

const logger = require('loopback-component-logger')('Person');

module.exports = function(Person) {
  // Almost all remote methods are hidden in model.config

  // returns PersonId based on request (needed because we don't use passport in tests)
  Person.getIdFromRequest = function(req) {
    if (req.session && req.session.passport) {
      return req.session.passport.user;
    } else if (req.accessToken) {
      return req.accessToken.userId;
    }
    return null;
  };

  // If user is admin or editor, or requesting their own Person, email will be included as emailAdress in result, otherwise hidden.
  Person.afterRemote('findById', function showEmail(ctx, currentInstance, next) {
    const callerId = Person.getIdFromRequest(ctx.req);
    if (!callerId) return next(); // anonymous request

    // can always get your own email
    if (ctx.result && ctx.result.id === callerId) {
      ctx.result.emailAddress = ctx.result.email;
      return next();
    }

    // else check if caller is admin or editor in a project currentInstance is a member of
    currentInstance.projectRoleMappings(null, (err, roleMaps) => {
      if (err) return next(err);
      let projects = [];
      roleMaps.forEach(roleMap => {
        if (!projects.some(el => { return el === roleMap.projectId; }))
          projects.push(roleMap.projectId);
      });
      async.some(projects, (projectId, done) => {
        Person.app.models.Project.personIsEditor(projectId, callerId,
          (err, canAccess) => {
            if (err) return next(err);
            if (canAccess) ctx.result.emailAddress = ctx.result.email;
            return next();
          }
        );
      });
    });
  });

  Person.getRoles = function(personId, callback) {
    Person.findById(personId, (err, person) => {
      if (err) return callback(err);
      if (!person) return callback({message: 'Unknown user', statusCode: 404});
      person.projectRoleMappings(null, callback);
    });
  };

  Person.remoteMethod('getRoles', {
    http: {verb: 'get', path: '/:id/roles'},
    accepts: [
      {arg: 'id', type: 'string', required: true},
    ],
    returns: {arg: 'roles', type: 'object', root: true},
    description: 'Get roles for given person',
  });

  Person.myRoles = function(req, callback) {
    const id = Person.getIdFromRequest(req);
    if (!id) return callback(null, []);
    Person.getRoles(id, callback);
  };

  Person.remoteMethod('myRoles', {
    http: {verb: 'get', path: '/myRoles'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
    ],
    returns: {arg: 'roles', type: 'array', root: true},
    description: 'Get roles for current session',
  });

  Person.myOpenEdits = function(req, callback) {
    const id = Person.getIdFromRequest(req);

    if (!id) return callback(null, []);

    Person.findById(id, (err, person) => {
      if (err) return callback(err);
      if (!person) return callback({message: 'Unknown user', statusCode: 404});

      person.edits({where: {finished: false}, include: 'author'}, callback);
    });
  };

  Person.remoteMethod('myOpenEdits', {
    http: {verb: 'get', path: '/myOpenEdits'},
    accepts: [
      {arg: 'req', type: 'object', http: {source: 'req'}},
    ],
    returns: {arg: 'edits', type: 'array', root: true},
    description: 'Get edits for caller',
  });

  // Make predefined users admin
  Person.observe('after save', function(ctx, next) {
    if (ctx.isNewInstance) {
      logger.info('Creating new user ' + ctx.instance.name + ', username: ' + ctx.instance.username);
    }

    if (settings.adminUsers.indexOf(ctx.instance.username) !== -1) {
      logger.info('Making user ' + ctx.instance.username + ' admin of all projects');
      async.each(settings.projects,
        (project, done) => Person.app.models.Project.addAdmin(project.id, ctx.instance.id, done),
        next
      );
    } else {
      return next();
    }
  });
};
