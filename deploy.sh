#!/bin/bash

# Example deploy script for production, to be used by systemd services

appRoot=/var/www/grg-api

if [ $# -eq 0 ]; then
    echo "usage: $0 (start|stop)" >&2
    exit 1
fi

if [ "$1" == "start" ]; then 
    NODE_ENV=production forever start $appRoot/server/server.js -l $appRoot/log/forever.log
    exit $?
fi

if [ "$1" == "stop" ]; then
    NODE_ENV=production forever stop $appRoot/server/server.js
    exit $?
fi

echo "argument '$1' not recognized" >&2
exit 1


