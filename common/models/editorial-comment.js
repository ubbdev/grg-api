'use strict';

module.exports = function(EditorialComment) {
  EditorialComment.disableRemoteMethodByName('find');
  EditorialComment.disableRemoteMethodByName('findOne'); // this does not show up in swagger, but is still available, had to be hacked in role-resolver. Loopback bug?
  EditorialComment.disableRemoteMethodByName('upsert');
  EditorialComment.disableRemoteMethodByName('create');
  EditorialComment.disableRemoteMethodByName('upsertWithWhere');
  EditorialComment.disableRemoteMethodByName('updateAll');
  EditorialComment.disableRemoteMethodByName('replaceOrCreate');
  EditorialComment.disableRemoteMethodByName('replaceById');
  EditorialComment.disableRemoteMethodByName('createChangeStream');
  EditorialComment.disableRemoteMethodByName('moveNode');
  EditorialComment.disableRemoteMethodByName('makeNodeRoot');
};
