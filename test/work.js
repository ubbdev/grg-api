'use strict';

const common = require('./common.js');
const server = common.server;
const assert = common.assert;
const nock = require('nock');
const fs = require('fs');
const appRoot = require('app-root-path');

const projectId = common.projects[0].id;

describe('Work', function() {
  let Project = null;
  let Work = null;

  before(done => {
    Project = server.models.Project;
    Work = server.models.Work;
    done();
  });

  describe('Elastic search indexing', function() {
    let emptyWorkQuery = {
      index: projectId.toLowerCase(),
      type: 'Work',
    };

    let workId = null;

    before(done => {
      if (!server.elasticClient) {
        return done('Need elastic search client to test indexing');
      }
      done();
    });

    before(done => {
      server.elasticClient.search(emptyWorkQuery, (err, res) => {
        if (err) return done(err);
        assert(res.hits, 'get results from elastic search');
        assert(res.hits.total === 0, 'start with empty index');
        done();
      });
    });

    after(done => server.models.Work.destroyAll(done));

    it('New work is indexed', function(done) {
      let title = 'Foo';
      Work.create({label: title, uri: '123', displayLink: '123', type: 'baz', document: {}, projectId: projectId}, (err, work) => {
        if (err) return done(err);
        workId = work.id;
        Project.searchWorksES(projectId, null, null, (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'get results from elastic search');
          assert(res.total === 1, 'get 1 hit');
          assert(res.hits[0].id === workId, 'correct result');
          done();
        });
      });
    });

    it('Updated work is updated in index', function(done) {
      let title = 'Bar';
      Work.replaceById(workId, {label: title, uri: '123', displayLink: '123', type: 'baz', document: {}, projectId: projectId}, (err, work) => {
        if (err) return done(err);
        Project.searchWorksES(projectId, null, null, (err, res) => {
          if (err) return done(err);
          assert(res.hits, 'get results from elastic search');
          assert(res.total === 1, 'get 1 hit');
          assert(res.hits[0].id === workId, 'correct result');
          assert(res.hits[0].label === title, 'correct title');
          done();
        });
      });
    });

    it('Deleted work is removed from indexed', function(done) {
      Work.destroyById(workId, err => {
        if (err) return done(err);
        Project.searchWorksES(projectId, null, null, (err, res) => {
          assert(res.hits, 'get results from elastic search');
          assert(res.total === 0, 'empty index');
          done();
        });
      });
    });
  });

  // will only test that syncing doesn't crash, will not test correctness of XSLT stylesheets and is specific to a particular Mermeid setup, for now.
  describe('Mermeid syncing', function() {
    const workList = require('./mock-data/list-works.json');
    const options = common.projects[0].workService.options;
    let work0 = null;

    before(done => {
      fs.readFile(appRoot + '/test/mock-data/work-0.xml', 'utf8', (err, file) => {
        if (err) return done(err);
        work0 = file.toString();
        done();
      });
    });

    // checks that regular execution path works and all stylesheets are found, etc
    it('regular request does complete', function(done) {
      nock(options.url)
      .get('/storage/list-files-json.xq')
      .query({c: options.collection, page: 1, itemsPerPage: 500})
      .reply(200, JSON.stringify(workList), {'content-type': 'text/javascript'})
      .get('/storage/dcm/' + workList.document[0].name)
      .reply(200, work0, {'content-type': 'application/xml'});

      server.models.Work.sync(projectId, done);
    });

    it('invalid projectId gives 404', function(done) {
      Work.sync('foobar', err => {
        assert(err, 'has error');
        assert(err.statusCode === 404, 'right error');
        done();
      });
    });

    it('project without workService gives 422', function(done) {
      Work.sync(common.projects[1].id, err => {
        assert(err, 'has error');
        assert(err.statusCode === 422, 'right error');
        done();
      });
    });

    // Faking these errors allow us to cover most execution branches. Not sure how relevant the faked remote errors are.
    describe('Remote errors', function() {
      it('sync with empty list-files response gives 500', function(done) {
        nock(options.url)
        .get('/storage/list-files-json.xq')
        .query({c: options.collection, page: 1, itemsPerPage: 500})
        .reply(200, JSON.stringify({}), {'content-type': 'text/javascript'});

        Work.sync(projectId, err => {
          assert(err, 'has error');
          done();
        });
      });

      it('sync with wrong content-type from mermeid gives 500', function(done) {
        nock(options.url)
        .get('/storage/list-files-json.xq')
        .query({c: options.collection, page: 1, itemsPerPage: 500})
        .reply(200, JSON.stringify(workList), {'content-type': 'application/xml'});

        server.models.Work.sync(projectId, err => {
          assert(err, 'has error');
          done();
        });
      });

      it('sync with wrong response code from mermeid gives 500', function(done) {
        nock(options.url)
        .get('/storage/list-files-json.xq')
        .query({c: options.collection, page: 1, itemsPerPage: 500})
        .reply(404, JSON.stringify(workList), {'content-type': 'text/javascript'});

        Work.sync(projectId, err => {
          assert(err, 'has error');
          done();
        });
      });

      it('syncing with invalid JSON from mermeid gives 500', function(done) {
        nock(options.url)
        .get('/storage/list-files-json.xq')
        .query({c: options.collection, page: 1, itemsPerPage: 500})
        .reply(200, '{]', {'content-type': 'text/javascript'});

        Work.sync(projectId, err => {
          assert(err, 'has error');
          done();
        });
      });

      it('sync with wrong content-type for xml document gives 500', function(done) {
        nock(options.url)
        .get('/storage/list-files-json.xq')
        .query({c: options.collection, page: 1, itemsPerPage: 500})
        .reply(200, JSON.stringify(workList), {'content-type': 'text/javascript'})
        .get('/storage/dcm/' + workList.document[0].name)
        .reply(200, work0, {'content-type': 'text/javascript'});

        Work.sync(projectId, err => {
          assert(err, 'has error');
          done();
        });
      });

      it('sync with wrong response code for xml document gives 500', function(done) {
        nock(options.url)
        .get('/storage/list-files-json.xq')
        .query({c: options.collection, page: 1, itemsPerPage: 500})
        .reply(200, JSON.stringify(workList), {'content-type': 'text/javascript'})
        .get('/storage/dcm/' + workList.document[0].name)
        .reply(404, work0, {'content-type': 'application/xml'});

        Work.sync(projectId, err => {
          assert(err, 'has error');
          done();
        });
      });

      it('sync with invalid xml document gives 500', function(done) {
        nock(options.url)
        .get('/storage/list-files-json.xq')
        .query({c: options.collection, page: 1, itemsPerPage: 500})
        .reply(200, JSON.stringify(workList), {'content-type': 'text/javascript'})
        .get('/storage/dcm/' + workList.document[0].name)
        .reply(200, 'abc', {'content-type': 'application/xml'});

        Work.sync(projectId, err => {
          assert(err, 'has error');
          done();
        });
      });
    });
  });
});
