'use strict';

const async = require('async');

module.exports = function(Referencecollection) {
  // Referencecollections are only changed through sync
  Referencecollection.disableRemoteMethodByName('create');
  Referencecollection.disableRemoteMethodByName('replaceById');
  Referencecollection.disableRemoteMethodByName('destroyById');
  Referencecollection.disableRemoteMethodByName('upsert');
  Referencecollection.disableRemoteMethodByName('upsertWithWhere');
  Referencecollection.disableRemoteMethodByName('updateAll');
  Referencecollection.disableRemoteMethodByName('prototype.updateAttributes');
  Referencecollection.disableRemoteMethodByName('prototype.replace');
  Referencecollection.disableRemoteMethodByName('replaceOrCreate');
  Referencecollection.disableRemoteMethodByName('createChangeStream');
  Referencecollection.disableRemoteMethodByName('addChild');
  Referencecollection.disableRemoteMethodByName('moveNode');
  Referencecollection.disableRemoteMethodByName('makeNodeRoot');
  Referencecollection.disableRemoteMethodByName('destroyBranch');

  // References are read only
  Referencecollection.disableRemoteMethodByName('prototype.__create__references');
  Referencecollection.disableRemoteMethodByName('prototype.__delete__references');
  Referencecollection.disableRemoteMethodByName('prototype.__updateById__references');
  Referencecollection.disableRemoteMethodByName('prototype.__destroyById__references');
  Referencecollection.disableRemoteMethodByName('prototype.__findById__references');
  Referencecollection.disableRemoteMethodByName('prototype.__link__references');
  Referencecollection.disableRemoteMethodByName('prototype.__unlink__references');

  function connectZoteroCollectionToParent(project, collection, done) {
    project.referenceCollections({where: {key: collection.parentKey}}, (err, parents) => {
      if (err) return done(err);
      if (!parents || parents.length == 0) return done({statusCode: 409, message: `Collection ${collection.key} has invalid parentKey ${collection.parentKey}`});
      if (parents.length > 1) {
        return done({statusCode: 409, message: `Several collections with key: ${collection.parentKey}, parents: ${parents.map(p => p.name).join(', ')}`});
      }
      Referencecollection.addChild(parents[0].id, collection.id, null, done);
    });
  }

  // create internal parent-child relations using tree mixin
  function connectZoteroCollectionTree(project, callback) {
    project.referenceCollections({where: {parentKey: {neq: null}}}, (err, collections) => {
      if (err) return callback(err);
      async.eachSeries(collections,
        (collection, done) => connectZoteroCollectionToParent(project, collection, done),
        callback);
    });
  }

  // These are small amounts of data, so we can just let it destroy all collections and add them again.
  // Important then that the reference data only refers to these with the Zotero keys and not as internal IDs.
  function updateZoteroCollections(project, callback) {
    function iterGet(start, cb) {
      Referencecollection.app.models.ZoteroReference.getCollections(
        project.bibliographyService.options.groupId,
        start,
        project.bibliographyService.options.authorization,
        (err, data) => {
          if (err) return cb(err);

          let collections = data.hits;

          if (data.next) {
            return iterGet(data.next, (err, nextCollections) => {
              if (err) return cb(err);
              return cb(null, collections.concat(nextCollections));
            });
          }

          return cb(null, collections);
        }
      );
    }

    // we are deleting all trees, if something went wrong in previous sync it's the local tree structure that matters
    async.series([
      // get all root nodes and destroy their branches recursively
      done => project.referenceCollections({where: {parent: null}}, (err, rootNodes) => {
        if (err) return done(err);
        if (!rootNodes) return done();

        async.each(rootNodes,
          (rootNode, next) => Referencecollection.destroyBranch(rootNode.id, next),
          done);
      }),
      // get remote collections and store them
      done => iterGet(0, (err, collections) => {
        if (err) return done(err);
        async.each(collections, (collection, next) => project.referenceCollections.create({
          key: collection.data.key,
          name: collection.data.name,
          link: collection.links.alternate.href,
          parentKey: collection.data.parentCollection === false ? null : collection.data.parentCollection,
        }, next), done);
      }),
      // connect parents and children in internal representation
      done => connectZoteroCollectionTree(project, done),
    ], callback);
  }

  // Sync reference collections with external resource
  Referencecollection.sync = function(projectId, callback) {
    if (!projectId) return callback({message: 'Project ID missing', statusCode: 422});

    Referencecollection.app.models.Project.findById(projectId, (err, project) => {
      if (err) return callback(err);
      if (!project) return callback({message: `No project found with id '${projectId}'`, statusCode: 404});

      const bibType = project.bibliographyService.type;

      if (bibType === 'Zotero') {
        return updateZoteroCollections(project, callback);
      }

      return callback({message: `Project '${projectId}' has invalid bibliography-type '${bibType}'`, statusCode: 409});
    });
  };

  // get parent keys of a collection recursively
  Referencecollection.getParentKeys = function(projectId, collectionKey, cb) {
    Referencecollection.findOne({where: {key: collectionKey, projectId: projectId}}, (err, collection) => {
      if (err) return cb(err);
      if (!collection) return cb(null, []);

      if (collection.parentKey) {
        let parentKeys = [collection.parentKey];
        Referencecollection.getParentKeys(projectId, collection.parentKey, (err, keys) => {
          if (err) return cb(err);
          return cb(null, parentKeys.concat(keys));
        });
      } else {
        return cb(null, []);
      }
    });
  };
};
