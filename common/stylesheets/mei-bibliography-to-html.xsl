<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m="http://www.music-encoding.org/ns/mei"
  exclude-result-prefixes="m">

  <xsl:import href="./mei-to-html.xsl" />

  <xsl:output method="xml" encoding="UTF-8" cdata-section-elements="" omit-xml-declaration="yes" indent="no" xml:space="default" />

  <xsl:template match="/">
    <div>
      <xsl:for-each select="m:mei/m:meiHead/m:workDesc/m:work/m:biblList">
        <xsl:call-template name="print_bibliography_type" />
        <xsl:apply-templates select="." mode="bibl_paragraph" />
      </xsl:for-each>
    </div>
  </xsl:template>

</xsl:stylesheet>
