'use strict';

const logger = require('loopback-component-logger')('root');

function accessSerializer(req) {
  return {
    method: req.method,
    url: req.url,
  };
}

logger.addSerializers({access: accessSerializer});

module.exports = function() {
  return function(err, req, res, next) {
    logger.info({access: req});
    if (err) logger.error({err: err, req: req});
    next(err);
  };
};
