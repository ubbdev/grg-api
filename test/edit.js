'use strict';

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

const projectId = common.projects[0].id;
const projectId2 = common.projects[1].id;

let Article = null;
let Reference = null;
let Concept = null;
let Work = null;
let Edit = null;

let sampleArticle = null;

describe('Edit', function() {
  let editedBody = 'contributor was here!';
  let editedBody2 = editedBody + ' otherContributor was here!';
  let editedTitle = 'Title edited once';
  let editedTitle2 = 'Title edited twice';
  let sampleArticleId = null;

  let ownerToken = null;

  let ref1Id = null;
  let ref2Id = null;
  let ref3Id = null;

  let concept1Id = null;
  let concept2Id = null;

  let work1Id = null;
  let work2Id = null;

  // for convenience
  before(done => {
    Article = server.models.Article;
    Reference = server.models.Reference;
    Concept = server.models.Concept;
    Work = server.models.Work;
    Edit = server.models.Edit;
    done();
  });

  before(done => {
    Reference.create({title: 'Foo', creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId},
      (err, ref) => {
        ref1Id = ref.id;
        sampleArticle = {
          title: 'Foo',
          body: 'bar',
          referenceId: [ref1Id],
          contributorId: [common.contributor.id, common.otherContributor.id],
          status: 'draft',
        };
        done(err);
      }
      );
  });

  before(done => {
    Reference.create({title: 'Foo2', creators: ['Bar2 Baz2'], date: '1999', key: '2', projectId: projectId2},
      (err, ref) => { ref2Id = ref.id; done(err); });
  });

  before(done => {
    Reference.create({title: 'Foo3', creators: ['Bar3 Baz3'], date: '1999', key: '3', projectId: projectId},
      (err, ref) => { ref3Id = ref.id; done(err); });
  });

  before(done => { Concept.create({prefLabel: [{name: 'Foo', lang: 'en'}], projectId: projectId}, (err, concept) => { concept1Id = concept.id; done(err); }); });
  before(done => { Concept.create({prefLabel: [{name: 'Bar', lang: 'en'}], projectId: projectId}, (err, concept) => { concept2Id = concept.id; done(err); }); });

  before(done => { Work.create({uri: 'FooUri', label: 'Foo', displayLink: 'FooLink', type: 'FooType', projectId: projectId}, (err, work) => { work1Id = work.id; done(err); }); });
  before(done => { Work.create({uri: 'BarUri', label: 'Bar', displayLink: 'BarLink', type: 'BarType', projectId: projectId}, (err, work) => { work2Id = work.id; done(err); }); });

  after(done => { Reference.destroyAll(done); });
  after(done => { Article.destroyAll(done); });
  after(done => { Concept.destroyAll(done); });
  after(done => { Work.destroyAll(done); });
  after(done => { Edit.destroyAll(done); });

  before(done => common.createSampleArticle(common.contributor, sampleArticle, projectId, (err, id) => {
    if (err) return done(err);
    sampleArticleId = id;
    ownerToken = common.contributor.token;
    done();
  }));

  after(done => { Article.destroyAll(done); });

  describe('Invalid requests', function() {
    it('Posting finished edit gives 422', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .send({finished: true})
      .expect(422);
    });

    it('Posting edit with content gives 422', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .send({body: 'bla'})
      .expect(422);
    });

    it('Update non-existent edit gives 404', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + '123')
      .set('Authorization', ownerToken)
      .expect(404);
    });
  });

  describe('Basics', function() {
    let editId = null;
    let editId2 = null;

    it('Posting empty edit returns edit with contents from article', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .expect((res) => {
        editId = res.body.id;
        assert(res.body.title === sampleArticle.title, 'Title correct');
        assert(res.body.body === sampleArticle.body, 'Body correct');
        assert(res.body.personId === common.contributor.id, 'Person correct');
        assert(res.body.references[0] === ref1Id, 'Reference correct');
        assert(res.body.contributors.some(c => { return c === common.contributor.id; }), 'Contributors correct');
        assert(res.body.contributors.some(c => { return c === common.otherContributor.id; }), 'Contributors correct');
        assert(res.body.subjects.length === 0, 'Should have no subjects yet');
        assert(res.body.relatedWorks.length === 0, 'Should have no related works yet');
        assert(res.body.created, 'Should have created date');
        assert(res.body.lastUpdated, 'Should have lastUpdated');
        assert(res.body.finished === false, 'Should not be finished');
      });
    });

    it('Updating edit body successful', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
      .set('Authorization', ownerToken)
      .send({body: editedBody})
      .expect(200)
      .expect(res => {
        assert(res.body.title === sampleArticle.title, 'Title unchanged');
        assert(res.body.body === editedBody, 'Body updated');
        assert(res.body.references[0] === ref1Id, 'Reference correct');
        assert(res.body.contributors.some(c => { return c === common.contributor.id; }), 'Contributors correct');
        assert(res.body.contributors.some(c => { return c === common.otherContributor.id; }), 'Contributors correct');
        assert(res.body.finished !== true, 'not finished');
      });
    });

    it('Updating edit title successful', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
      .set('Authorization', ownerToken)
      .send({title: editedTitle})
      .expect(200)
      .expect(res => {
        assert(res.body.title === editedTitle, 'Title updated');
        assert(res.body.body === editedBody, 'Body unchanged');
        assert(res.body.finished !== true, 'not finished');
      });
    });

    it('Article not changed before update with finished: true', function() {
      return agent
      .get('/api/articles/' + sampleArticleId)
      .set('Authorization', ownerToken)
      .expect(200)
      .expect(res => {
        assert(res.body.title === sampleArticle.title, 'Title unchanged');
        assert(res.body.body === sampleArticle.body, 'Body unchanged');
        assert(res.body.status === sampleArticle.status, 'Status unchanged');
        assert(!res.body.publicationDate, 'No publication date');
      });
    });

    it('GET edit returns correct edit', function() {
      return agent
      .get('/api/articles/' + sampleArticleId + '/edits/' + editId)
      .set('Authorization', ownerToken)
      .expect(200)
      .expect(res => {
        assert(res.body.body === editedBody, 'Correct body');
      });
    });

    it('If same user starts new edit, gets the previous one back', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .expect(res => {
        assert(res.body.title === editedTitle, 'Title unchanged');
        assert(res.body.body === editedBody, `Body unchanged '${res.body.body}'`);
        assert(res.body.finished !== true, 'not finished');
      });
    });

    it('Unfinished edit is listed under myOpenEdits', function() {
      return agent
      .get('/api/people/myOpenEdits')
      .set('Authorization', ownerToken)
      .expect(200)
      .expect(res => {
        assert(res.body.length === 1, 'one open edit');
        assert(res.body[0].id === editId, 'correct edit open');
      });
    });

    it('Unfinished edit is not listed under myOpenEdits for anonymous', function() {
      return agent
      .get('/api/people/myOpenEdits')
      .expect(200)
      .expect(res => {
        assert(res.body.length === 0, 'no edits');
      });
    });

    it('Other contributor cannot start new edit at the same time', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', common.otherContributor.token)
      .expect(409);
    });

    it('Other contributor cannot update edit', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
      .set('Authorization', common.otherContributor.token)
      .send({body: editedBody2})
      .expect(409);
    });

    it('Another user cannot impersonate current editor through PUT', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
      .set('Authorization', common.otherContributor.token)
      .send({body: editedBody2, personId: common.contributor.id})
      .expect(409);
    });

    it('Updating with finished: true sets endDate', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
      .set('Authorization', ownerToken)
      .send({finished: true})
      .expect(200)
      .expect((res) => {
        assert(res.body.title === editedTitle, 'Title unchanged');
        assert(res.body.finished === true, 'Should be set to finished');
      });
    });

    it('After edit is finished associated article has updated body', function() {
      return agent
      .get('/api/articles/' + sampleArticleId)
      .set('Authorization', ownerToken)
      .expect(200)
      .expect((res) => {
        assert(res.body.title === editedTitle, 'Title unchanged');
        assert(res.body.body === editedBody, 'Body correct');
        assert(res.body.status === 'draft', 'Status correct');
        assert(!res.body.publicationDate, 'no publicationDate');
      });
    });

    it('After edit, there is one edit with status finished', function() {
      return agent
      .get('/api/articles/' + sampleArticleId + '/edits')
      .expect(200)
      .set('Authorization', ownerToken)
      .expect((res) => {
        assert(res.body.length === 1, 'Only one edit');
        assert(res.body[0].body === editedBody, 'Body correct');
        assert(res.body[0].title === editedTitle, 'Title unchanged');
        assert(res.body[0].finished === true, 'Edit finished');
      });
    });

    it('After edit, there are two contributors', function() {
      return agent
      .get('/api/articles/' + sampleArticleId + '/contributors/count')
      .set('Authorization', ownerToken)
      .expect(200)
      .expect({'count': 2});
    });

    it('After edit, the contributors are correct', function() {
      return agent
      .get('/api/articles/' + sampleArticleId + '/contributors')
      .expect(200)
      .set('Authorization', ownerToken)
      .expect(res => {
        assert(res.body.some(c => { return c.name === common.contributor.name; }), 'correct username');
        assert(res.body.some(c => { return c.name === common.otherContributor.name; }), 'correct username');
      });
    });

    it('After edit is finished, the edit cannot be updated', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
      .set('Authorization', ownerToken)
      .expect(409);
    });

    it('After edit is finished it is not listed under myOpenEdits', function() {
      return agent
      .get('/api/people/myOpenEdits')
      .set('Authorization', ownerToken)
      .expect(200)
      .expect(res => {
        assert(res.body.length === 0, 'no open edits');
      });
    });

    it('A non-contributor can not edit same article', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', common.thirdContributor.token)
      .send()
      .expect(401);
    });

    it('A contributor from other project can not edit same article', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', common.otherProjectContributor.token)
      .send()
      .expect(401);
    });
  });

  describe('Deletion', function() {
    function createEdit(cb) {
      agent.post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', common.contributor.token)
      .send()
      .expect(200)
      .end((err, res) => cb(err, res.body && res.body.id));
    }

    it('Contributor can delete own edit', function(done) {
      createEdit((err, id) => {
        if (err) return done(err);

        agent.delete('/api/edits/' + id)
        .set('Authorization', common.contributor.token)
        .expect(200)
        .end(done);
      });
    });

    it('Other project contributor can not delete edit', function(done) {
      createEdit((err, id) => {
        if (err) return done(err);

        agent.delete('/api/edits/' + id)
        .set('Authorization', common.otherContributor.token)
        .expect(401)
        .end((err, res) => {
          if (err) return done(err);
          Edit.deleteById(id, done);
        });
      });
    });

    it('Project editor can delete edit', function(done) {
      createEdit((err, id) => {
        if (err) return done(err);

        agent.delete('/api/edits/' + id)
        .set('Authorization', common.editor.token)
        .expect(200)
        .end(done);
      });
    });
  });

  describe('Publishing', function() {
    it('Contributor can not edit status', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({status: 'published'})
        .expect(401);
      })
      .then(() => {
        return agent
        .delete('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .expect(204);
      });
    });

    it('Editor can edit status (without being contributor to given article)', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', common.editor.token)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', common.editor.token)
        .send({status: 'published', finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId)
        .expect(200)
        .expect(res => {
          assert(res.body.status === 'published', 'is published');
          assert(res.body.publicationDate, 'has publication date');
        });
      });
    });
  });

  describe('Contributors', function() {
    let editId = null;
    let editId2 = null;

    it('Adding contributor through edit successful', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({contributors: [common.contributor.id, common.otherContributor.id, common.thirdContributor.id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/contributors')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 3, 'Correct number of contributors');
          assert(res.body.some(c => { return c.id === common.contributor.id; }), 'Contributor correct');
          assert(res.body.some(c => { return c.id === common.otherContributor.id; }), 'Contributor correct');
          assert(res.body.some(c => { return c.id === common.thirdContributor.id; }), 'Contributor correct');
        });
      })
      .then(() => {
        return agent
        .get('/api/people/' + common.thirdContributor.id + '/articles')
        .expect(200)
        .expect(res => {
          assert(res.body.some(a => { return a.id === sampleArticleId; }), 'Reverse relation created');
        });
      });
    });

    it('Added contributor can edit same article', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', common.thirdContributor.token)
      .expect(200)
      .then(res => {
        let editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', common.thirdContributor.token)
        .send({title: editedTitle2, body: editedBody2})
        .expect(200)
        .expect((res) => {
          assert(res.body.title === editedTitle2, 'Correct title');
          assert(res.body.body === editedBody2, 'Correct body');
          assert(res.body.personId === common.thirdContributor.id, 'Correct person');
          assert(res.body.finished === false, 'Not finished');
          editId2 = res.body.id;
        });
      });
    });

    it('Added contributor can finish edit', function() {
      return agent
      .put('/api/articles/' + sampleArticleId + '/edits/' + editId2)
      .set('Authorization', common.thirdContributor.token)
      .send({finished: true})
      .expect(200)
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId)
        .expect(200)
        .expect(res => {
          assert(res.body.title === editedTitle2, 'Title updated');
          assert(res.body.body === editedBody2, 'Body updated');
        });
      });
    });

    it('Removing contributors successful', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({contributors: [common.contributor.id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/contributors')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 1, 'Correct number of contributors');
          assert(res.body[0].id === common.contributor.id, 'Contributor correct');
        });
      });
    });

    it('Can not remove all contributors', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({contributors: [], finished: true})
        .expect(422);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/contributors')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 1, 'Correct number of contributors');
          assert(res.body[0].id === common.contributor.id, 'Contributor correct');
        });
      });
    });

    it('Adding non-existent contributor through edit not allowed', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({contributors: ['123'], finished: true})
        .expect(422);
      })
      .then(res => {
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({finished: true})
        .expect(200)
        .expect(res => {
          assert(res.body.contributors.length === 1, 'Correct number of contributors');
          assert(res.body.contributors[0] === common.contributor.id, 'Contributor correct');
        });
      });
    });

    it('Adding contributor from other project not allowed', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({contributors: [common.otherProjectContributor.id], finished: true})
        .expect(422);
      })
      .then(res => {
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({finished: true})
        .expect(200)
        .expect(res => {
          assert(res.body.contributors.length === 1, 'Correct number of contributors');
          assert(res.body.contributors[0] === common.contributor.id, 'Contributor correct');
        });
      });
    });
  });

  describe('References', function() {
    it('Adding reference through edit successful', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        let editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({references: [ref1Id, ref3Id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/references')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'Correct number of references');
          assert(res.body.some(r => { return r.id === ref1Id; }), 'Reference correct');
          assert(res.body.some(r => { return r.id === ref3Id; }), 'Reference correct');
        });
      })
      .then(() => {
        return agent
        .get('/api/references/' + ref3Id + '/articles')
        .expect(200)
        .expect(res => {
          assert(res.body.some(a => { return a.id === sampleArticleId; }), 'Reverse relation created');
        });
      });
    });

    it('Removing reference through edit successful', function() {
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        let editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({references: [ref1Id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/references')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 1, 'Correct number of references');
          assert(res.body[0].id === ref1Id, 'Reference correct');
        });
      });
    });

    it('Adding non-existent reference through edit is not allowed', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({references: ['123'], finished: true})
        .expect(422);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .expect(200)
        .expect(res => {
          assert(res.body.references.length === 1, 'Correct number of references');
          assert(res.body.references[0] === ref1Id, 'Reference correct');
        });
      });
    });
  });

  describe('Subjects', function() {
    it('Adding subject through edit successful', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        let editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({subjects: [concept1Id, concept2Id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/subjects')
        .set('Authorization', ownerToken)
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'Correct number of subjects');
          assert(res.body.some(s => { return s.id === concept1Id; }), 'Subject correct');
          assert(res.body.some(s => { return s.id === concept2Id; }), 'Subject correct');
        });
      })
      .then(() => {
        return agent
        .get('/api/concepts/' + concept2Id + '/articles')
        .expect(200)
        .expect(res => {
          assert(res.body.some(a => { return a.id === sampleArticleId; }), 'Reverse relation created');
        });
      });
    });

    it('Removing subject through edit successful', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({subjects: [concept1Id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/subjects')
        .set('Authorization', ownerToken)
        .expect(200)
        .expect(res => {
          assert(res.body.length === 1, 'Correct number of subjects');
          assert(res.body[0].id === concept1Id, 'Subject correct');
        });
      });
    });

    it('Adding non-existent subject through edit gives 404', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({subjects: ['123'], finished: true})
        .expect(422);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .expect(200)
        .expect(res => {
          assert(res.body.subjects.length === 1, 'Correct number of subjects');
          assert(res.body.subjects[0] === concept1Id, 'Subject correct');
        });
      });
    });
  });

  describe('Works', function() {
    it('Adding work through edit successful', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({relatedWorks: [work1Id, work2Id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/relatedWorks')
        .expect(200)
        .expect(res => {
          assert(res.body.length === 2, 'Correct number of works');
          assert(res.body.some(w => { return w.id === work1Id; }), 'Work correct');
          assert(res.body.some(w => { return w.id === work2Id; }), 'Work correct');
        });
      })
      .then(() => {
        return agent
        .get('/api/works/' + work2Id + '/articles')
        .set('Authorization', ownerToken)
        .expect(200)
        .expect(res => {
          assert(res.body.some(a => { return a.id === sampleArticleId; }), 'Reverse relation created');
        });
      });
    });

    it('Removing work through edit successful', function() {
      let editId = null;
      return agent
      .post('/api/articles/' + sampleArticleId + '/edits')
      .set('Authorization', ownerToken)
      .expect(200)
      .then(res => {
        editId = res.body.id;
        return agent
        .put('/api/articles/' + sampleArticleId + '/edits/' + editId)
        .set('Authorization', ownerToken)
        .send({relatedWorks: [work1Id], finished: true})
        .expect(200);
      })
      .then(() => {
        return agent
        .get('/api/articles/' + sampleArticleId + '/relatedWorks')
        .set('Authorization', ownerToken)
        .expect(200)
        .expect(res => {
          assert(res.body.length === 1, 'Correct number of works');
          assert(res.body[0].id === work1Id, 'Work correct');
        });
      });
    });
  });

  describe('Lists', function() {
    it('can not be accessed by anonymous', function() {
      return agent
      .get('/api/projects/' + projectId + '/edits')
      .expect(401);
    });

    it('can not be accessed by project contributor', function() {
      return agent
      .get('/api/projects/' + projectId + '/edits')
      .set('Authorization', common.contributor.token)
      .expect(401);
    });

    it('can be accessed by project editor', function() {
      return agent
      .get('/api/projects/' + projectId + '/edits')
      .set('Authorization', common.editor.token)
      .expect(200);
    });
  });
});

