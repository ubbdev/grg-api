'use strict';

const async = require('async');

const logger = require('loopback-component-logger')('init-db');

const settings = require('../getSettings');

const PROJECTS = settings.projects;

if (process.env.BUILD_SDK) return;

module.exports = function(app, cb) {
  const Project = app.models.Project;
  async.each(PROJECTS, (project, done) => {
    Project.findById(project.id, (err, obj) => {
      if (err) return done(err);
      if (obj) return done();
      console.log('Creating project ' + project.id);
      Project.create(project, done);
    });
  }, err => {
    if (err) logger.fatal(err);
    cb(err);
  });
};
