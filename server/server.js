'use strict';

const loopback = require('loopback');
const boot = require('loopback-boot');
const helmet = require('helmet');
const session = require('express-session');
const flash = require('express-flash');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passportComponent  = require('loopback-component-passport');
const MongoStore = require('connect-mongo')(session);
const bunyan = require('bunyan');
const elasticsearch = require('elasticsearch');

const devmode = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const prodmode = process.env.NODE_ENV === 'production';
const testmode = process.env.NODE_ENV === 'test';

const settings = require('./getSettings');

const cookieSecret = settings.cookieSecret;

let rootLogger;

// we don't want test terminal  output to be clogged by intentionally triggered errors and log messages
if (prodmode || testmode) {
  rootLogger = bunyan.createLogger({
    name: 'GRG-API',
    serializers: {
      err: bunyan.stdSerializers.err,
      req: bunyan.stdSerializers.req,
    },
    streams: [
      {
        level: 'info',
        path: settings.infoLog,
      },
      {
        level: 'error',
        path: settings.errorLog,
      },
    ],
  });
} else { // in development we want errors directly in the terminal
  rootLogger = bunyan.createLogger({
    name: 'GRG-API',
    serializers: {
      err: bunyan.stdSerializers.err,
      req: bunyan.stdSerializers.req,
    },
    streams: [
      {
        level: 'info',
        stream: process.stdout,
      },
      {
        level: 'error',
        stream: process.stderr,
      },
    ],
  });
}

const logger = require('loopback-component-logger')(rootLogger);

const app = module.exports = loopback();

app.use(helmet());

// We need flash messages to see passport errors
app.use(flash());

app.start = function(cb) {
  // start the web server
  return app.listen(err => {
    if (err) return cb(err);
    app.emit('started');
    const baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      const explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
    cb();
  });
};

// we make this a separate function so that we can prepare the database etc before booting, in tests
app.init = function(cb) {
  // elasticsearch, needs to be initialized before boot
  if (settings.elasticsearch) {
    app.elasticClient = new elasticsearch.Client(settings.elasticsearch);
  }

  // Bootstrap the application, configure models, datasources and middleware.
  // Sub-apps like REST API are mounted via boot scripts.
  boot(app, __dirname, function(err) {
    if (err) return cb(err);

    // to support JSON-encoded bodies
    app.middleware('parse', bodyParser.json());
    app.middleware('parse', bodyParser.urlencoded({
      extended: true,
    }));

    // The access token is only available after boot
    app.middleware('auth', loopback.token({
      model: app.models.PersonAccessToken,
    }));

    // sessions
    app.middleware('session:before', cookieParser(cookieSecret));

    if (prodmode) {
      app.dataSources.db.connector.connect((err, db) => {
        if (err) return cb(err);
        app.middleware('session', session({
          secret: cookieSecret,
          saveUninitialized: true,
          resave: true,
          store: new MongoStore({db: db}),
        }));
      });
    } else {
      app.middleware('session', session({
        secret: cookieSecret,
        saveUninitialized: true,
        resave: true,
      }));
    }
    cb();
  });
};

if (!testmode) {
  app.init(err => {
    if (err) throw err;
    console.log('App initialized');
    // start the server if `$ node server.js`
    if (require.main === module) {
      app.start(err => {
        if (err) throw err;
      });
    }
  });
}
