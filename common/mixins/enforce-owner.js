'use strict';

module.exports = function(Model, options) {
  Model.observe('before save', (ctx, next) => {
    if (!ctx.isNewInstance) return next();
    if (!ctx.options.accessToken) return next();
    ctx.instance.personId = ctx.options.accessToken.userId;
    next();
  });
};
