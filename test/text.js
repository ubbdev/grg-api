'use strict';

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

describe('Text', function() {
  let Text = null;
  let Project = null;
  let projectId = common.projects[0].id;
  let sampleText = {body: 'foo', key: 'BAR', lang: 'en', projectId: projectId};

  // for convenience
  before(done => {
    Text = server.models.Text;
    Project = server.models.Project;
    done();
  });

  before(done => {
    Text.create(sampleText, (err, text) => {
      if (err) return done(err);
      sampleText.id = text.id.toString();
      done();
    });
  });

  after(done => Text.destroyAll(done));

  describe('Authorization', function() {
    it('No-one can access hidden api entry points', function() {
      return Promise.all([
        agent.get('/api/texts').expect(404),
        agent.post('/api/texts').expect(404),
        agent.put('/api/texts').expect(404),
        agent.put('/api/texts').expect(404),
        agent.post('/api/texts/update').expect(404),
        agent.post('/api/texts/replaceOrCreate').expect(404),
        agent.post('/api/texts/change-stream').expect(404),
        agent.post('/api/texts/' + sampleText.id + '/replace').expect(404),
      ]);
    });

    it('Unauthorized can read texts', function() {
      return Promise.all([
        agent.get('/api/projects/' + projectId + '/texts').expect(200).expect([sampleText]),
        agent.get('/api/projects/' + projectId + '/texts/' + sampleText.id).expect(200).expect(sampleText),
        agent.get('/api/texts/' + sampleText.id).expect(200).expect(sampleText),
      ]);
    });

    it('Unauthorized can not create or edit texts', function() {
      return Promise.all([
        agent.post('/api/projects/' + projectId + '/texts').expect(401),
        agent.delete('/api/projects/' + projectId + '/texts').expect(401),
        agent.put('/api/projects/' + projectId + '/texts/' + sampleText.id).expect(401),
        agent.delete('/api/projects/' + projectId + '/texts/' + sampleText.id).expect(401),
      ]);
    });

    it('Contributor can not create or edit texts', function() {
      let token = common.contributor.token;
      return Promise.all([
        agent.post('/api/projects/' + projectId + '/texts').set('Authorization', token).expect(401),
        agent.delete('/api/projects/' + projectId + '/texts').set('Authorization', token).expect(401),
        agent.put('/api/projects/' + projectId + '/texts/' + sampleText.id).set('Authorization', token).expect(401),
        agent.delete('/api/projects/' + projectId + '/texts/' + sampleText.id).set('Authorization', token).expect(401),
      ]);
    });

    it('Editor from other project can not create or edit texts', function() {
      let token = common.otherProjectEditor.token;
      return Promise.all([
        agent.post('/api/projects/' + projectId + '/texts').set('Authorization', token).expect(401),
        agent.delete('/api/projects/' + projectId + '/texts').set('Authorization', token).expect(401),
        agent.put('/api/projects/' + projectId + '/texts/' + sampleText.id).set('Authorization', token).expect(401),
        agent.delete('/api/projects/' + projectId + '/texts/' + sampleText.id).set('Authorization', token).expect(401),
      ]);
    });
  });

  describe('CRUD', function() {
    let token = null;

    before(done => {
      token = common.editor.token;
      done();
    });

    it('Project editor can create and delete text', function() {
      return agent
      .post('/api/projects/' + projectId + '/texts')
      .set('Authorization', token)
      .send({body: 'baz', key: 'FOO', lang: 'en', projectId: projectId})
      .expect(200)
      .then(res => {
        return agent
        .delete('/api/projects/' + projectId + '/texts/' + res.body.id)
        .set('Authorization', token)
        .expect(204);
      });
    });

    it('Project editor can update text', function() {
      return agent
      .put('/api/projects/' + projectId + '/texts/' + sampleText.id)
      .set('Authorization', token)
      .send({body: 'foobar'})
      .expect(200)
      .then(res => {
        return agent
        .get('/api/texts/' + sampleText.id)
        .expect(200)
        .expect(res => {
          assert(res.body.body === 'foobar', 'correct body');
          assert(res.body.key === sampleText.key);
        });
      });
    });
  });

  describe('Validation', function() {
    it('can not create two texts with same key, project and language', function(done) {
      let sampleClone = {};
      Object.assign(sampleClone, sampleText);
      delete sampleClone.id;
      Text.create(sampleClone, (err, text) => {
        assert(err, 'should fail');
        done();
      });
    });
  });
});
