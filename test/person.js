'use strict';

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

const projectId = common.projects[0].id;
const projectId2 = common.projects[1].id;

function fullUserObject(user) {
  return {
    name: user.name,
    id: user.id,
    emailAddress: user.email,
  };
}

function limitedUserObject(user) {
  return {
    name: user.name,
    id: user.id,
  };
}

function allLimitedUsers() {
  return common.people.sort((a, b) => a.id > b.id).map(user => { return limitedUserObject(user); });
}

function allFullUsers() {
  return common.people.sort((a, b) => a.id > b.id).map(user => { return fullUserObject(user); });
}

describe('Person', function() {
  let adminToken = null;
  let editorToken = null;

  it('Should get 404 from /api/people/:id/accessTokens', function() {
    return agent.get('/api/people/' + common.admin.id + '/accessTokens')
    .expect(404);
  });

  it('Should get 404 from /api/people/:id/credentials', function() {
    return agent.get('/api/people/' + common.admin.id + '/credentials')
    .expect(404);
  });

  it('Should get 404 from /api/people/:id/identities', function() {
    return agent.get('/api/people/' + common.admin.id + '/identities')
    .expect(404);
  });

  it('Should get articles from /api/people/:id/articles', function() {
    return agent.get('/api/people/' + common.admin.id + '/articles')
    .expect(200); // just checking ACLs here
  });

  describe('Unauthenticated', function() {
    describe('Information hiding', function() {
      it('Should get limited information from /api/people', function() {
        return agent.get('/api/people').expect(200, allLimitedUsers());
      });

      it('Should get limited information from /api/people/:id', function() {
        return agent.get('/api/people/' + common.admin.id)
        .expect(200, limitedUserObject(common.admin));
      });
    });

    describe('Roles', function() {
      it('should get empty result from /api/people/myRoles', function() {
        return agent.get('/api/people/myRoles')
        .set('Content-Type', 'application/json')
        .expect(200, []);
      });

      it('Should get correct info from GET /api/people/:id/roles', function() {
        return agent
        .get('/api/people/' + common.admin.id + '/roles')
        .expect(200, [{projectId: projectId, role: 'admin'}]);
      });
    });
  });

  describe('Admin', function() {
    let token = null;
    before(done => { token = common.admin.token; done(); });
    describe('Information hiding', function() {
      it('Should get full information from /api/people', function() {
        return agent
        .get('/api/people')
        .set('Authorization', token)
        .expect(200, allLimitedUsers());
      });

      it('Should get full information from /api/people/:id for own id', function() {
        return agent
        .get('/api/people/' + common.admin.id)
        .set('Authorization', token)
        .expect(200, fullUserObject(common.admin));
      });

      it('Should get full information from /api/people/:id for other users id', function() {
        return agent
        .get('/api/people/' + common.editor.id)
        .set('Authorization', token)
        .expect(200, fullUserObject(common.editor));
      });
    });

    describe('Roles', function() {
      it('Should get correct information from /api/people/myRoles', function() {
        return agent
        .get('/api/people/myRoles')
        .set('Authorization', token)
        .expect(200)
        .then(res => {
          const roles = res.body;
          assert(roles.length === 1, 'correct number roles');
          assert(roles[0].projectId === projectId, 'correct project');
          assert(roles[0].role === 'admin', 'correct role');
        });
      });

      it('Should get correct information from /api/people/:id/roles for self', function() {
        return agent
        .get('/api/people/' + common.admin.id + '/roles')
        .set('Authorization', token)
        .expect(200, [{projectId: projectId, role: 'admin'}]);
      });

      it('Should get correct information from /api/people/:id/roles for other user', function() {
        return agent
        .get('/api/people/' + common.editor.id + '/roles')
        .set('Authorization', token)
        .expect(200, [{projectId: projectId, role: 'editor'}]);
      });
    });
  });

  describe('Editor', function() {
    let token = null;
    before(done => { token = common.editor.token; done(); });

    describe('Information hiding', function() {
      it('Should get full information from /api/people', function() {
        return agent
        .get('/api/people')
        .set('Authorization', token)
        .expect(200, allLimitedUsers());
      });

      it('Should get full information from /api/people/:id for own id', function() {
        return agent
        .get('/api/people/' + common.editor.id)
        .set('Authorization', token)
        .expect(200, fullUserObject(common.editor));
      });

      it('Should get full information from /api/people/:id for other users id', function() {
        return agent
        .get('/api/people/' + common.admin.id)
        .set('Authorization', token)
        .expect(200, fullUserObject(common.admin));
      });
    });
  });

  describe('Contributor', function() {
    let token = null;
    before(done => { token = common.contributor.token; done(); });

    describe('Information hiding', function() {
      it('Should get limited information from /api/people', function() {
        return agent
        .get('/api/people')
        .set('Authorization', token)
        .expect(200, allLimitedUsers());
      });

      it('Should get full information from /api/people/:id for own id', function() {
        return agent
        .get('/api/people/' + common.contributor.id)
        .set('Authorization', token)
        .expect(200, fullUserObject(common.contributor));
      });

      it('Should get limited information from /api/people/:id for other users id', function() {
        return agent
        .get('/api/people/' + common.admin.id)
        .set('Authorization', token)
        .expect(200, limitedUserObject(common.admin));
      });
    });
  });

  describe('New user', function() {
    let token = null;
    before(done => { token = common.newUser.token; done(); });

    it('Should get limited information from /api/people', function() {
      return agent
      .get('/api/people')
      .expect(200, allLimitedUsers());
    });

    it('Should get full information from /api/people/:id for own id', function() {
      return agent
      .get('/api/people/' + common.newUser.id)
      .set('Authorization', token)
      .expect(200, fullUserObject(common.newUser));
    });

    it('Should get limited information from /api/people/:id for other users id', function() {
      return agent
      .get('/api/people/' + common.admin.id)
      .set('Authorization', token)
      .expect(200, limitedUserObject(common.admin));
    });
  });
});
