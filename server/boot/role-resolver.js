'use strict';

const async = require('async');

module.exports = function(app) {
  const Project = app.models.Project;
  const Article = app.models.Article;
  const Person = app.models.Person;
  const PublicComment = app.models.PublicComment;
  const EditorialComment = app.models.EditorialComment;
  const Concept = app.models.Concept;
  const Edit = app.models.Edit;

  function getProjectFromComment(ctx, Model, cb) {
    let commentId = ctx.remotingContext.instance && ctx.remotingContext.instance.id ||
      (ctx.remotingContext && ctx.remotingContext.args.id);
    if (!commentId) {
      return cb({statusCode: 500, message: 'Role resolver can not determine comment id from request'});
    }
    Model.findById(commentId, (err, comment) => {
      if (err) return cb(err);
      if (!comment) return cb({statusCode: 404, message: 'Comment ' + commentId + ' not found'});
      comment.article((err, article) => cb(err, article.projectId));
    });
  }

  function getProject(ctx, cb) {
    let projectId = null;
    // this is to signalize a case we haven't thought of
    let defaultErr = new Error({statusCode: 500, message: 'Role resolver can not determine project id from request'});

    switch (ctx.modelName) {
      case 'Article':
        projectId = ctx.remotingContext.instance && ctx.remotingContext.instance.projectId;
        if (projectId) return cb(null, projectId);

        let articleId = ctx.remotingContext.args && ctx.remotingContext.args.id;
        if (!articleId) return cb(defaultErr);
        Article.findById(articleId, (err, ass) => {
          if (err) return cb(err);
          if (!ass) return cb({statusCode: 404, message: 'Article ' + articleId + ' not found'});
          cb(null, ass.projectId);
        });
        break;

      case 'Edit':
        projectId = ctx.remotingContext.instance && ctx.remotingContext.instance.projectId;
        if (projectId) return cb(null, projectId);

        let editId = ctx.remotingContext.args && ctx.remotingContext.args.id;
        if (!editId) return cb(defaultErr);
        Edit.findById(editId, (err, ass) => {
          if (err) return cb(err);
          if (!ass) return cb({statusCode: 404, message: 'Edit ' + editId + ' not found'});
          cb(null, ass.projectId);
        });
        break;

      case 'EditorialComment':
        return getProjectFromComment(ctx, EditorialComment, cb);

      case 'PublicComment':
        return getProjectFromComment(ctx, PublicComment, cb);

      case 'Concept':
        projectId = ctx.remotingContext.instance && ctx.remotingContext.instance.projectId;
        if (projectId) return cb(null, projectId);

        let conceptId = ctx.remotingContext.instance && ctx.remotingContext.instance.id ||
          (ctx.remotingContext && ctx.remotingContext.args.id);
        if (!conceptId) return cb(defaultErr);
        Concept.findById(conceptId, (err, concept) => {
          if (err) return cb(err);
          if (!concept) return cb({statusCode: 404, message: 'Concept ' + conceptId + ' not found'});
          cb(null, concept.projectId);
        });
        break;

      case 'Project':
        projectId = ctx.remotingContext.instance && ctx.remotingContext.instance.id;
        if (!projectId) projectId = ctx.remotingContext.args && ctx.remotingContext.args.id;
        return cb(null, projectId);

      default:
        return cb(null, null);
    }
  }

  function hasRole(roleResolver, ctx, cb) {
    const personId = ctx.accessToken.userId;
    if (!personId) return process.nextTick(() => cb(null, false));
    getProject(ctx, (err, projectId) => {
      if (err) return cb(err, false);
      if (!projectId) return cb({statusCode: 422, message: 'Must supply projectId'});
      roleResolver(projectId, personId, cb);
    });
  }

  app.models.Role.registerResolver('projectContributor', function(role, ctx, cb) {
    hasRole(Project.personIsContributor, ctx, cb);
  });

  app.models.Role.registerResolver('projectEditor', function(role, ctx, cb) {
    hasRole(Project.personIsEditor, ctx, cb);
  });

  app.models.Role.registerResolver('projectAdmin', function(role, ctx, cb) {
    hasRole(Project.personIsAdmin, ctx, cb);
  });

  function getArticleId(ctx, cb) {
    switch (ctx.modelName) {
      case 'Article':
        if (ctx.remotingContext.instance)
          return cb(null, ctx.remotingContext.instance.id);
        return cb('Invalid Article');

      case 'EditorialComment':
        if (ctx.remotingContext.instance && ctx.remotingContext.instance.articleId)
          return cb(null, ctx.remotingContext.instance.articleId);

        let id = ctx.remotingContext.args.id;
        if (!id) return cb(null, null);
        if (id === 'findOne') return cb(null, null); // hack to counter bug in Loopback
        return EditorialComment.findById(id, (err, comment) => {
          if (err) return cb(err);
          if (!comment) return cb({statusCode: 404, message: 'Comment ' + id + ' not found'});
          cb(null, comment.articleId);
        });

      default:
        return cb('Can not determine Article Id for model ' + ctx.modelName, null);
    }
  }

  // Is the user registered as a contributor to an article
  app.models.Role.registerResolver('articleContributor', function(role, ctx, cb) {
    const personId = ctx.accessToken.userId;
    getArticleId(ctx, (err, articleId) => {
      if (err) return process.nextTick(() => cb(err, false));
      if (!articleId) return process.nextTick(() => cb(null, false));
      Article.findById(articleId, (err, ass) => {
        if (err) return cb(err);
        if (!ass) return cb({statusCode: 404, message: 'Article ' + articleId + ' not found'});
        ass.contributors(null, (err, contrib) => {
          cb(null, contrib.some(c => { return c.id === personId; }));
        });
      });
    });
  });

  // If an article is a draft, only project contributors can read it
  app.models.Role.registerResolver('articleReader', function(role, ctx, cb) {
    const personId = ctx.accessToken.userId;
    let articleId = null;

    switch (ctx.modelName) {
      case 'Article':
        articleId = ctx.remotingContext.instance && ctx.remotingContext.instance.id;
        if (!articleId) articleId = ctx.remotingContext.args.id;
        break;

      case 'Reference':
        articleId = ctx.remotingContext.args.fk;
        break;

      case 'Project':
        // only for findById
        articleId = ctx.remotingContext.args.fk;
        break;

      default:
        return process.nextTick(() => cb('Role articleReader not defined in this context'));
    }

    Article.findById(articleId, (err, ass) => {
      if (err) return cb(err);
      if (!ass) return cb({statusCode: 404, message: 'Article ' + articleId + ' not found'});
      if (ass.status === Article.STATUS_PUBLISHED ||
        ass.status === Article.STATUS_PUBLIC_DRAFT) {
        return cb(null, true);
      }
      hasRole(Project.personIsContributor, ctx, cb);
    });
  });
};
