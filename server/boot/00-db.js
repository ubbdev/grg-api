'use strict';

const logger = require('loopback-component-logger')('init-db');

module.exports = function(app, cb) {
  const db = app.dataSources.db;
  db.autoupdate(err => {
    if (err) logger.fatal(err);
    cb(err);
  });
};
