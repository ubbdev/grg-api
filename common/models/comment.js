'use strict';

module.exports = function(Comment) {
  Comment.DELETED_MESSAGE = 'This comment was deleted';

  // Comments should always be sorted by date ascending.
  Comment.compareNodes = function(comment1, comment2) {
    const d1 = new Date(comment1.created);
    const d2 = new Date(comment2.created);
    return d1.getTime() - d2.getTime();
  };

  Comment.observe('loaded', (ctx, next) => {
    if (ctx.data.deleted) {
      ctx.data.body = Comment.DELETED_MESSAGE;
      ctx.data.author = null;
    }
    next();
  });

  // make sure only editors can undelete a comment
  Comment.observe('before save', (ctx, next) => {
    if (ctx.isNewInstance) return next();
    if (!ctx.options.accessToken) return next(); // denial will be handled elsewhere for anonymous requests
    if (!ctx.data) return next();
    if (ctx.data.deleted === undefined) return next();
    if (ctx.data.deleted === true) return next(); // if we have write access we can delete
    if (!ctx.data.deleted === false) return next({statusCode: 409, message: 'property \'deleted\' must be a boolean'});

    const personId = ctx.options.accessToken.userId;
    ctx.currentInstance.article((err, article) => {
      if (err) return next(err);
      article.project((err, project) => {
        if (err) return next(err);
        Comment.app.models.Project.personIsEditor(project.id, personId, (err, isEditor) => {
          if (err) return next(err);
          if (isEditor) return next();
          return next({statusCode: 401, message: 'Only editors can undelete comments'});
        });
      });
    });
  });
};
