'use strict';

// tests common for Public and Editorial comments

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

function reply(apiString, token, ownerId, commentId, body) {
  return agent
  .post('/api/' + apiString + '/' + commentId + '/children')
  .set('Authorization', token)
  .send({body: body})
  .expect(200)
  .then(() => {
    return agent
    .get('/api/' + apiString + '/' + commentId + '/tree')
    .set('Authorization', token)
    .expect(200)
    .then(res => {
      let parent = res.body;
      assert(parent.id === commentId, 'correct parent');
      assert(parent.children && parent.children.length === 1, 'has one child');
      assert(parent.children[0].body === body, 'has correct body');
      assert(parent.children[0].parent === commentId, 'has correct parent');
      assert(parent.children[0].personId === ownerId, 'has correct author');
      return parent.children[0].id;
    });
  });
};

function canReply(apiString, token, ownerId, commentId, articleId) {
  const body = 'a reply';
  let replyId = null;

  return reply(apiString, token, ownerId, commentId, body)
  .then(id => {
    replyId = id;
    return agent
    .get('/api/' + apiString + '/' + commentId)
    .set('Authorization', token)
    .then(res => {
      let comment = res.body;
      assert(comment.children && res.body.children.length === 1, 'parent has one child');
      assert(comment.children[0] === replyId, 'correct child');
    });
  })
  .then(() => {
    return agent
    .get('/api/' + apiString + '/' + replyId)
    .set('Authorization', token)
    .then(res => {
      let reply = res.body;
      assert(reply.body === body, 'correct body');
      assert(reply.parent === commentId, 'correct parent');
    });
  })
  .then(() => {
    return agent
    .get('/api/' + apiString + '/' + replyId + '/author')
    .set('Authorization', token)
    .expect(200)
    .then(res => {
      assert(res.body.id === ownerId, 'correct author');
      assert(!res.body.username, 'username hidden');
    });
  })
  .then(() => {
    return agent
    .get('/api/' + apiString + '/' + replyId + '/article')
    .set('Authorization', token)
    .expect(200)
    .then(res => {
      assert(res.body.id === articleId, 'correct article');
    });
  })
  .then(() => {
    return agent
    .delete('/api/' + apiString + '/' + replyId)
    .set('Authorization', common.editor.token) // use editor here since not all users can delete
    .expect(200);
  })
  .then(() => {
    return agent
    .get('/api/' + apiString + '/' + commentId + '/tree')
    .set('Authorization', token)
    .then(res => {
      let parent = res.body;
      assert(parent.id === commentId, 'correct parent');
      assert(parent.children, 'has children array');
      assert(parent.children.length === 0, 'has zero children');
    });
  });
};

function canDeleteTree(apiString, token, ownerId, articleId) {
  let commentId = null;
  let replyId = null;
  return agent
  .post('/api/articles/' + articleId + '/' + apiString)
  .set('Authorization', token)
  .send({body: 'a comment'})
  .expect(200)
  .then(res => {
    commentId = res.body.id;
    return reply(apiString, token, ownerId, commentId, 'a reply');
  })
  .then(id => {
    replyId = id;
    return agent
    .delete('/api/' + apiString + '/' + commentId + '/tree')
    .set('Authorization', token)
    .expect(204);
  })
  .then(() => {
    return agent
    .get('/api/' + apiString + '/' + commentId)
    .set('Authorization', token)
    .expect(404);
  })
  .then(() => {
    return agent
    .get('/api/' + apiString + '/' + replyId)
    .set('Authorization', token)
    .expect(404);
  });
};

describe('Comment', function() {
  let refId = null;
  let article = null;
  let articleId = null;
  let commentId = null;
  const comment = {body: 'comment'};
  const projectId = common.projects[0].id;
  const articleContributors = [common.contributor, common.otherContributor];

  let Article = null;
  let Reference = null;
  let EditorialComment = null;
  let PublicComment = null;

  before(done => {
    Article = server.models.Article;
    Reference = server.models.Reference;
    EditorialComment = server.models.EditorialComment;
    PublicComment = server.models.PublicComment;
    done();
  });

  before(done => {
    Reference.create({title: 'Foo', creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId},
      (err, ref) => {
        refId = ref.id;
        article = {
          title: 'Foo',
          body: 'bar',
          referenceId: [refId],
          projectId: projectId,
          status: 'published',
          contributorId: articleContributors.map(c => c.id),
        };
        done(err);
      }
    );
  });

  before(done => common.createSampleArticle(common.contributor, article, projectId, (err, id) => {
    if (err) return done(err);
    articleId = id;
    done();
  }));

  // Order matters
  after(done => Reference.destroyAll(done));
  after(done => Article.destroyAll(done));

  [{api: 'publicComments', model: 'PublicComment', name: 'Public', unauthorizedRead: true, projectContributorRead: true, projectContributorWrite: true},
   {api: 'editorialComments', model: 'EditorialComment', name: 'Editorial', unauthorizedRead: false, projectContributorRead: false, projectContributorWrite: false}]
  .forEach(config => {
    describe(config.name, function() {
      let commentId = null;
      const comment = {body: 'comment'};

      before(() => {
        return agent
        .post('/api/articles/' + articleId + '/' + config.api)
        .set('Authorization', common.contributor.token)
        .send(comment)
        .expect(200)
        .expect(res => { commentId = res.body.id; });
      });

      after(done => common.server.models[config.model].destroyAll(done));

      describe('Access control', function() {
        it('No-one can access hidden api entry points', function() {
          return Promise.all([
            agent.get('/api/' + config.api).expect(404),
            agent.post('/api/' + config.api).expect(404),
            agent.put('/api/' + config.api).expect(404),
            agent.put('/api/' + config.api).expect(404),
            //agent.get('/api/' + config.api + '/findOne').expect(404), // inconsistencies here, will give 401 when necessary anyway
            agent.post('/api/' + config.api + '/update').expect(404),
            agent.post('/api/' + config.api + '/replaceOrCreate').expect(404),
            agent.post('/api/' + config.api + '/change-stream').expect(404),
            agent.post('/api/' + config.api + '/' + commentId + '/replace').expect(404),
          ]);
        });

        let notString = config.unauthorizedRead ? ' ' : 'not ';

        it('Unauthorized can ' + notString + 'read', function() {
          let status = config.unauthorizedRead ? 200 : 401;
          return Promise.all([
            agent.get('/api/' + config.api + '/' + commentId).expect(status),
            agent.get('/api/' + config.api + '/' + commentId + '/author').expect(status),
            agent.get('/api/' + config.api + '/' + commentId + '/article').expect(status),
            agent.get('/api/' + config.api + '/' + commentId + '/tree').expect(status),
            agent.get('/api/articles/' + articleId + '/' + config.api).expect(status),
            agent.get('/api/articles/' + articleId + '/' + config.api + '/' + commentId).expect(status),
          ]);
        });

        it('Unauthorized can not write', function() {
          return Promise.all([
            agent.patch('/api/' + config.api + '/' + commentId).expect(401),
            agent.delete('/api/' + config.api + '/' + commentId).expect(401),
            agent.delete('/api/' + config.api + '/' + commentId + '/tree').expect(401),
            agent.post('/api/' + config.api + '/' + commentId + '/children').expect(401),
            agent.post('/api/articles/' + articleId + '/' + config.api).expect(401),
          ]);
        });

        [{
          person: common.otherProjectContributor,
          description: 'Non-member contributor',
          canRead: config.unauthorizedRead,
          canWrite: false,
          canReply: false,
          canDeleteTree: false,
        },
        {
          person: common.otherProjectEditor,
          description: 'Non-member editor',
          canRead: config.unauthorizedRead,
          canWrite: false,
          canReply: false,
          canDeleteTree: false,
        },
        {
          person: common.thirdContributor,
          description: 'Project Contributor',
          canRead: config.projectContributorRead,
          canWrite: config.projectContributorWrite,
          canReply: config.projectContributorWrite,
          canDeleteTree: false,
        },
        {
          person: common.contributor,
          description: 'Article Contributor',
          canRead: true,
          canWrite: true,
          canReply: true,
          canDeleteTree: false,
        },
        {
          person: common.editor,
          description: 'Project Editor',
          canRead: true,
          canWrite: true,
          canReply: true,
          canDeleteTree: true,
        }]
        .forEach(personConfig => {
          describe(personConfig.description, function() {
            let token = null;

            before(done => {
              token = personConfig.person.token;
              done();
            });

            if (personConfig.canWrite === false) {
              it('can not write', function() {
                return Promise.all([
                  agent.patch('/api/' + config.api + '/' + commentId).set('Authorization', token).expect(401),
                  agent.delete('/api/' + config.api + '/' + commentId).set('Authorization', token).expect(401),
                  agent.post('/api/' + config.api + '/' + commentId + '/children').set('Authorization', token).expect(401),
                  agent.delete('/api/' + config.api + '/' + commentId + '/tree').set('Authorization', token).expect(401),
                  agent.post('/api/articles/' + articleId + '/' + config.api).set('Authorization', token).expect(401),
                ]);
              });
            }

            if (personConfig.canRead) {
              it('can read', function() {
                return Promise.all([
                  agent.get('/api/' + config.api + '/' + commentId).set('Authorization', token).expect(200),
                  agent.get('/api/' + config.api + '/' + commentId + '/tree').set('Authorization', token).expect(200),
                  agent.get('/api/articles/' + articleId + '/' + config.api).set('Authorization', token).expect(200),
                  agent.get('/api/articles/' + articleId + '/' + config.api + '/' + commentId).set('Authorization', token).expect(200),
                ]);
              });

              it('can read author of comment', function() {
                return agent
                .get('/api/' + config.api + '/' + commentId + '/author')
                .set('Authorization', token)
                .expect(200)
                .expect(res => {
                  assert(res.body.id === common.contributor.id, 'correct author');
                });
              });

              it('can access article for comment', function() {
                return agent
                .get('/api/' + config.api + '/' + commentId + '/article')
                .set('Authorization', token)
                .expect(200)
                .expect(res => {
                  assert(res.body.id === articleId, 'correct article');
                });
              });
            } else {
              it('can not read', function() {
                return Promise.all([
                  agent.get('/api/' + config.api + '/' + commentId).set('Authorization', token).expect(401),
                  agent.get('/api/articles/' + articleId + '/' + config.api).set('Authorization', token).expect(401),
                  agent.get('/api/articles/' + articleId + '/' + config.api).set('Authorization', token).expect(401),
                  agent.get('/api/articles/' + articleId + '/' + config.api + '/' + commentId).set('Authorization', token).expect(401),
                ]);
              });
            }

            if (personConfig.canWrite) {
              it('can add comment to article', function() {
                const comment = {body: 'a second comment'};
                let commentId = null;
                return agent
                .post('/api/articles/' + articleId + '/' + config.api)
                .set('Authorization', token)
                .send(comment)
                .expect(200)
                .then(res => {
                  commentId = res.body.id;
                  return agent
                  .get('/api/' + config.api + '/' + commentId)
                  .set('Authorization', token)
                  .expect(res => {
                    assert(res.body.body === comment.body, 'correct body');
                  });
                })
                .then(() => {
                  return agent
                  .delete('/api/' + config.api + '/' + commentId)
                  .set('Authorization', common.editor.token)
                  .expect(200);
                });
              });

              it('can update comment', function() {
                let comment = {body: 'a second comment'};
                let commentId = null;
                return agent
                .post('/api/articles/' + articleId + '/' + config.api)
                .set('Authorization', token)
                .send(comment)
                .expect(200)
                .then(res => {
                  commentId = res.body.id;
                  comment = {body: 'second edited comment'};
                  return agent
                  .patch('/api/' + config.api + '/' + commentId)
                  .set('Authorization', token)
                  .send(comment)
                  .expect(200);
                })
                .then(res => {
                  return agent
                  .get('/api/' + config.api + '/' + commentId)
                  .set('Authorization', token)
                  .expect(res => {
                    assert(res.body.body === comment.body, 'correct body');
                  });
                })
                .then(() => {
                  return agent
                  .delete('/api/' + config.api + '/' + commentId)
                  .set('Authorization', common.editor.token)
                  .expect(200);
                });
              });
            }

            if (personConfig.canReply) {
              it('can reply', function() {
                return canReply(config.api, token, personConfig.person.id, commentId, articleId);
              });
            }

            if (personConfig.canDeleteTree) {
              it('can delete tree', function() {
                return canDeleteTree(config.api, token, personConfig.person.id, articleId);
              });
            }
          });
        });
      });

      describe('Deletion', function() {
        let contributorCommentId = null;
        let otherContributorCommentId = null;

        before(() => {
          return agent
          .post('/api/articles/' + articleId + '/' + config.api)
          .set('Authorization', common.contributor.token)
          .send({body: 'foo'})
          .expect(200)
          .then(res => {
            contributorCommentId = res.body.id;
          });
        });

        before(() => {
          return agent
          .post('/api/articles/' + articleId + '/' + config.api)
          .set('Authorization', common.otherContributor.token)
          .send({body: 'foo'})
          .expect(200)
          .then(res => {
            otherContributorCommentId = res.body.id;
          });
        });

        it('Non-editor non-owner can not censor comment', function() {
          return agent
          .patch('/api/' + config.api + '/' + otherContributorCommentId)
          .set('Authorization', common.contributor.token)
          .send({deleted: true})
          .expect(401);
        });

        it('Non-editor owner can censor comment', function() {
          return agent
          .patch('/api/' + config.api + '/' + contributorCommentId)
          .set('Authorization', common.contributor.token)
          .send({deleted: true})
          .expect(200)
          .then(res => {
            return agent
            .get('/api/' + config.api + '/' + contributorCommentId)
            .set('Authorization', common.contributor.token)
            .expect(200);
          })
          .then(res => {
            assert(res.body.deleted === true, 'deleted = true');
            assert(!res.body.author, 'author hidden');
            assert(res.body.body === PublicComment.DELETED_MESSAGE, 'body hidden');
            return Promise.resolve();
          });
        });

        it('Non-editor owner can not uncensor comment', function() {
          return agent
          .patch('/api/' + config.api + '/' + contributorCommentId)
          .set('Authorization', common.contributor.token)
          .send({deleted: false})
          .expect(401);
        });

        it('Non-owner editor can uncensor comment', function() {
          return agent
          .patch('/api/' + config.api + '/' + contributorCommentId)
          .set('Authorization', common.editor.token)
          .send({deleted: false})
          .expect(200)
          .then(res => {
            return agent
            .get('/api/' + config.api + '/' + contributorCommentId)
            .set('Authorization', common.contributor.token)
            .expect(200);
          })
          .then(res => {
            assert(res.body.deleted === false, 'deleted = false');
            assert(res.body.author, 'author shown');
            assert(res.body.body === 'foo', 'body shown');
            return Promise.resolve();
          });
        });

        it('Non-owner editor can censor comment', function() {
          return agent
          .patch('/api/' + config.api + '/' + contributorCommentId)
          .set('Authorization', common.editor.token)
          .send({deleted: true})
          .expect(200)
          .then(res => {
            return agent
            .get('/api/' + config.api + '/' + contributorCommentId)
            .set('Authorization', common.contributor.token)
            .expect(200);
          })
          .then(res => {
            assert(res.body.deleted === true, 'deleted = true');
            assert(!res.body.author, 'author hidden');
            assert(res.body.body === PublicComment.DELETED_MESSAGE, 'body hidden');
            return Promise.resolve();
          });
        });

        it('Non-member editor can not censor comment', function() {
          return agent
          .patch('/api/' + config.api + '/' + contributorCommentId)
          .set('Authorization', common.otherProjectEditor.token)
          .send({deleted: true})
          .expect(401);
        });
      });

      describe('Notifications', function() {
        beforeEach(common.maildev.deleteAllEmail);

        const comment = {body: 'a comment'};
        let commentAuthorEmail = null;
        let commentId = null;
        const reply = {body: 'a reply'};

        after(() => {
          return agent
          .delete('/api/' + config.api + '/' + commentId + '/tree')
          .set('Authorization', common.admin.token)
          .expect(204);
        });

        it('Other person commenting on an article sends notification to article contributors', function(done) {
          common.maildev.on('error', err => {
            common.maildev.removeAllListeners();
            done(err);
          });

          common.maildev.on('new', email => {
            assert(email.text.match(comment.body), 'comment body in email');
            assert(email.text.match(article.id), 'has link to article');
            assert(email.to.length === articleContributors.length, 'sent to 2 contributors');

            articleContributors
            .forEach(contrib => assert(email.to.some(recip => recip.address === contrib.email), 'sent to all contributors'));

            common.maildev.removeAllListeners();

            done();
          });

          assert(!articleContributors.some(c => c === common.editor), 'make sure the commenter is not an author');

          agent
          .post('/api/articles/' + articleId + '/' + config.api)
          .set('Authorization', common.editor.token)
          .send(comment)
          .expect(200)
          .end((err, res) => {
            if (err) return done(err);
            commentId = res.body.id;
            commentAuthorEmail = common.editor.email;
          });
        });

        it('Commenting on your own article only sends notifications to other contributors', function(done) {
          common.maildev.on('error', err => {
            common.maildev.removeAllListeners();
            done(err);
          });

          common.maildev.on('new', email => {
            assert(email.text.match(comment.body), 'comment body in email');
            assert(email.text.match(article.id), 'has link to article');
            assert(email.to.length === articleContributors.length - 1, 'one contributor exluded');
            articleContributors.slice(1)
            .forEach(contrib => assert(email.to.some(e => e.address === contrib.email), 'sent to other contributors'));

            common.maildev.removeAllListeners();

            done();
          });

          agent
          .post('/api/articles/' + articleId + '/' + config.api)
          .set('Authorization', articleContributors[0].token)
          .send(comment)
          .expect(200)
          .end(err => {
            if (err) return done(err);
          });
        });

        it('Replying to a comment sends a notification to the author replied to and the article contributors', function(done) {
          let counter = 0;

          common.maildev.on('error', err => {
            common.maildev.removeAllListeners();
            done(err);
          });

          common.maildev.on('new', email => {
            ++counter;

            assert(email.text.match(article.id), 'has link to article');
            assert(email.text.match(reply.body), 'reply body in email');

            if (email.subject.match('replied')) {
              assert(email.to.length === 1, 'only one recipient');
              assert(email.to[0].address === commentAuthorEmail, 'sent to commentAuthor');
            }

            if (email.subject.match('commented')) {
              assert(email.text.match(comment.body), 'comment body in email');
              assert(email.to.length === articleContributors.length - 1, 'one contributor exluded');
              articleContributors.slice(1)
              .forEach(contrib => assert(email.to.some(e => e.address === contrib.email), 'sent to other contributors'));
            }

            if (counter == 2) {
              common.maildev.removeAllListeners();
              done();
            }
          });

          agent
          .post('/api/' + config.api + '/' + commentId + '/children')
          .set('Authorization', articleContributors[0].token)
          .send(reply)
          .expect(200)
          .end((err, res) => {
            if (err) return done(err);
          });
        });
      });
    });
  });
});
