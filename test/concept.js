'use strict';

const common = require('./common.js');
const server = common.server;
const agent = common.agent;
const assert = common.assert;

let Concept = null;
let Project = null;
let Reference = null;

describe('Concept', function() {
  // for convenience
  before(done => {
    Concept = server.models.Concept;
    Project = server.models.Project;
    Reference = server.models.Reference;
    done();
  });

  afterEach(done => Concept.destroyAll(done));

  it('can not create concept without prefLabel', function(done) {
    Concept.create({prefLabel: []}, err => {
      assert(err, 'has error');
      done();
    });
  });

  it('can not create concept with empty string prefLabel', function(done) {
    Concept.create({prefLabel: [{name: '', lang: 'en'}]}, err => {
      assert(err, 'has error');
      done();
    });
  });

  it('can not create concept with the same prefLabel twice', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}, {name: 'bar', lang: 'en'}]}, err => {
      assert(err, 'has error');
      done();
    });
  });

  it('can create concept with two different prefLabels in different languages', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}, {name: 'foo', lang: 'fr'}]}, done);
  });

  it('can create two concepts with same prefLabel.name but different language', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}]},
      (err, concept) => {
        if (err) return done(err);
        let id1 = concept.id;
        Concept.create({prefLabel: [{name: 'foo', lang: 'fr'}]}, (err, concept) => {
          if (err) return done(err);
          let id2 = concept.id;
          assert(id1 != id2);
          done();
        });
      });
  });

  it('can create concept with several altLabels in same language', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}, {name: 'baz', lang: 'en'}]}, done);
  });

  it('can not create concept with identical altLabel repeated', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}, {name: 'bar', lang: 'en'}]}, err => {
      assert(err, 'has error');
      assert(err.statusCode === 422, 'correct error');
      done();
    });
  });

  it('can not create concept where altLabel is same as prefLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'foo', lang: 'en'}]}, err => {
      assert(err, 'has error');
      assert(err.statusCode === 422, 'correct error');
      done();
    });
  });

  it('can not patch concept where altLabel is same as stored prefLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({altLabel: [{name: 'foo', lang: 'en'}]}, err => {
        assert(err, 'has error');
        assert(err.statusCode === 422, 'correct error');
        done();
      });
    });
  });

  it('can not patch concept where prefLabel is same as stored altLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({prefLabel: [{name: 'bar', lang: 'en'}]}, err => {
        assert(err, 'has error');
        assert(err.statusCode === 422, 'correct error');
        done();
      });
    });
  });

  it('can not patch concept where new prefLabel is same as new altLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({prefLabel: [{name: 'baz', lang: 'en'}], altLabel: [{name: 'baz', lang: 'en'}]}, err => {
        assert(err, 'has error');
        assert(err.statusCode === 422, 'correct error');
        done();
      });
    });
  });

  it('can patch concept where new altLabel is same as old altLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({altLabel: [{name: 'bar', lang: 'en'}]}, err => {
        assert(!err, 'no error');
        done();
      });
    });
  });

  it('can patch concept where new prefLabel is same as old prefLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({prefLabel: [{name: 'foo', lang: 'en'}]}, err => {
        assert(!err, 'no error');
        done();
      });
    });
  });

  it('can patch concept with new altLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({altLabel: [{name: 'bar', lang: 'en'}, {name: 'baz', lang: 'en'}]}, err => {
        assert(!err, 'no error');
        done();
      });
    });
  });

  it('can patch concept with new prefLabel', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({prefLabel: [{name: 'baz', lang: 'en'}]}, err => {
        assert(!err, 'no error');
        done();
      });
    });
  });

  it('can patch concept with prefLabels in several languages', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({prefLabel: [{name: 'foo', lang: 'en'}, {name: 'foo', lang: 'fr'}]}, err => {
        assert(!err, 'no error');
        done();
      });
    });
  });

  it('can patch concept with altLabels in several languages', function(done) {
    Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], altLabel: [{name: 'bar', lang: 'en'}]}, (err, concept) => {
      assert(!err, 'no error');
      concept.updateAttributes({altLabel: [{name: 'bar', lang: 'en'}, {name: 'foo', lang: 'fr'}]}, err => {
        assert(!err, 'no error');
        done();
      });
    });
  });

  it('concepts inherit project with addChild()', function() {
    let pid = common.projects[0].id;
    let concept = null;
    let child = null;
    return agent
    .post('/api/projects/' + pid + '/concepts')
    .set('Authorization', common.editor.token)
    .send({prefLabel: [{name: 'foo', lang: 'en'}]})
    .expect(200)
    .expect(res => {
      concept = res.body;
      assert(concept.projectId === pid, 'parent has correct project');
    })
    .then(() => {
      return agent
      .post('/api/concepts/' + concept.id + '/children')
      .set('Authorization', common.editor.token)
      .send({prefLabel: [{name: 'bar', lang: 'en'}]})
      .expect(200)
      .expect(res => {
        child = res.body;
      });
    })
    .then(() => {
      return agent
      .get('/api/projects/' + pid + '/concepts/' + child.id)
      .expect(200);
    })
    .then(() => {
      return Concept.destroyById(child.id); // can't clear up children in our afterEach()
    });
  });

  describe('Reference linking', function() {
    let reference = null;
    let concept = null;
    let projectId = common.projects[0].id;

    beforeEach(done => {
      Concept.create({prefLabel: [{name: 'foo', lang: 'en'}], projectId: projectId}, (err, obj) => {
        if (err) return done(err);
        concept = obj;
        done();
      });
    });

    beforeEach(done => {
      Reference.create({title: 'Foo', creators: ['Bar Baz'], date: '1999', key: '1', projectId: projectId},
        (err, ref) => {
          if (err) return done(err);
          reference = ref;
          done(err);
        }
      );
    });

    afterEach(done => Reference.destroyAll(done));

    it('should index reverse relation when reference link is added', function(done) {
      concept.references.add(reference.id, err => {
        if (err) return done(err);

        concept.save(err => {
          if (err) return done(err);

          Project.searchReferencesES(projectId, null, null, (err, res) => {
            if (err) return done(err);

            assert(res.total === 1, 'one hit');
            assert(res.hits[0].subjects.length === 1, 'one subject');
            assert(res.hits[0].subjects[0].id === concept.id, 'correct subject');
            done();
          });
        });
      });
    });

    it('should remove reverse relation from index when reference link is removed', function(done) {
      concept.references.remove(reference.id, err => {
        if (err) return done(err);

        concept.save(err => {
          if (err) return done(err);

          Project.searchReferencesES(projectId, null, null, (err, res) => {
            if (err) return done(err);

            assert(res.total === 1, 'one hit');
            assert(res.hits[0].subjects.length === 0, 'no subjects');
            done();
          });
        });
      });
    });

    it('should index reverse relation when reference link is added and removed remotely', function(done) {
      agent.put('/api/concepts/' + concept.id + '/references/rel/' + reference.id)
      .set('Authorization', common.admin.token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);

        Project.searchReferencesES(projectId, null, null, (err, res) => {
          if (err) return done(err);

          assert(res.total === 1, 'one hit');
          assert(res.hits[0].subjects.length === 1, 'one subject');
          assert(res.hits[0].subjects[0].id === concept.id, 'correct subject');

          agent.delete('/api/concepts/' + concept.id + '/references/rel/' + reference.id)
          .set('Authorization', common.admin.token)
          .expect(204)
          .end((err, res) => {
            if (err) return done(err);

            Project.searchReferencesES(projectId, null, null, (err, res) => {
              if (err) return done(err);

              assert(res.total === 1, 'one hit');
              assert(res.hits[0].subjects.length === 0, 'no subject');
              done();
            });
          });
        });
      });
    });
  });
});
